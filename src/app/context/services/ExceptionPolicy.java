package app.context.services;

public enum ExceptionPolicy {
	IGNORE, // Just ignore the exception and go round
	THROW, // Mean a critical exception that cannot be skipped and must be re-throwed
	RETURN; // Break and return silently

	private PolicyException error;
	
	public ExceptionPolicy as(Throwable e) {
		this.error =  new PolicyException(e);
		return this;
	} 
	
	public PolicyException wrap(Exception exception) {
		return this.error == null ? new PolicyException(exception) : error;
	} 
}
