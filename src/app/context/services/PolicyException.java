package app.context.services;

@SuppressWarnings("serial")
public class PolicyException extends RuntimeException {
	public PolicyException(String message) {
		super(message);
	}

	public PolicyException(Throwable cause) {
		super(cause);
	}
}
