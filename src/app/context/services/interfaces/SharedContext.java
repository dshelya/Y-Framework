package app.context.services.interfaces;

import java.util.Optional;

import app.context.AppClient;
import app.module.api.GenericModule;

public interface SharedContext {
	Context context = AppClient.getInstance();
	DatabaseService database = context.getDatabase();
	GuiAccessService gui = context.getGui();
	NotificationService notifier = context.getNotifier();
	ConfigService config = context.getConfig();
	//WebViewService web = context.getWebViewService();
	//SchedulerService scheduler = context.getScheduler();
	
	default LoggerService logger(Class<?> tagClass) {
		return context.getLogger().tag(tagClass);
	}
	
	default Optional<GenericModule> externalModule(String name) {
		return context.getModule(name);
	}
}
