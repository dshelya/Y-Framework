package app.context.services.interfaces;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import app.context.services.ConnectionProxy;
import app.context.services.DatabaseWrapper;
import app.context.services.db.ConditionalStatement;

public interface DatabaseService {
	//public void connect();
	//public void reconnect();

	public void shutdown() throws SQLException;
	public void execute(String sql) throws SQLException;

	public ConnectionProxy newConnection() throws SQLException;
	public String getUUID() throws SQLException;

	//public boolean isConnected() throws SQLException;

	public <T> List<T> convert(String sql, UnsafeFunction<ResultSet, T> handler) throws Exception;
	public <T> T convertFirst(String sql, UnsafeFunction<ResultSet, T> handler) throws Exception;
	public void process(String sql, UnsafeConsumer<ResultSet> handler) throws Exception;
	public void processFirst(String sql, UnsafeConsumer<ResultSet> handler) throws Exception;
		
	public <T extends DatabaseWrapper> T wrapBy(Class<T> wrapperClass) throws Exception;
	public <I> void manage(ConditionalStatement<I> enitiryProxy) throws SQLException;
	//public <I> void manage(ReverseStatementProxy<I> enitiryProxy) throws SQLException;

	void bindDBFileName(String dbFileName);
	public void release(ResultSet result);
	public void interrupt();

	@Deprecated
	public Connection getConnection() throws SQLException;	
	@Deprecated
	public ResultSet select(String sql) throws SQLException;	
	@Deprecated
	public ResultSet selectFirst(String sql) throws SQLException;
}
