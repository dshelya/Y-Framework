package app.context.services.interfaces;

import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;

import app.context.services.ExceptionPolicy;

public interface SchedulerService {	
	void sleep(long millis);

	void cancelTasks();
	void purgeTasks();
 	void runSafe(Task task);
	
	void backgroundRun(Task task);
	<T> T backgroundRun(ReturnTask<T> task);
	ScheduledFuture<?> backgroundRunReccurent(Task task, long amount, TimeUnit unit);
	
	void runInMainThread(Task task);
	<T> T runInMainThread(ReturnTask<T> task);

	void runUntilSuccess(Task task, int times, Function<Throwable, ExceptionPolicy> exceptionValidator);
	void runUntilSuccess(Task task, int times);

	<T> T runUntilSuccess(ReturnTask<T> task, int times, Function<Throwable, ExceptionPolicy> exceptionValidator);
	<T> T runUntilSuccess(ReturnTask<T> task, int times);

	void runConstrained(Task task, long timeout, TimeUnit timeoutUnit);	
}