package app.context.services.interfaces;

import static java.time.format.DateTimeFormatter.ofPattern;

import java.time.format.DateTimeFormatter;

public interface NotificationService {
	DateTimeFormatter FORMATTED_DATE = ofPattern("yyyy-MM-dd HH:mm:ss");

	//void sendEmail(String subject, String body);
	//void sendEmail(String subject, String body, String to);
	void sendEmail(String subject, String body, String from, String fromPass, String to);
}
