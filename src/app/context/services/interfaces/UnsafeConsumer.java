package app.context.services.interfaces;

@FunctionalInterface
public interface UnsafeConsumer<A> {
	void accept(A argument) throws Exception;
}