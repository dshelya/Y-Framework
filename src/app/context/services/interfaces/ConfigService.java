package app.context.services.interfaces;

import java.io.File;
import java.util.Map;

import app.module.api.ExternalProperty;

public interface ConfigService {
	String BASE_DIR = System.getenv("AppData");

	File getPathFor(String name);
	void setWorkDirName(String name);
	<T> void serializeToFile(String file, T object) throws Exception;
	<T> T deserializeFromFile(String path) throws Exception;
	void ensureDefault(String resourse);
	<T> void bridgeSettings(ExternalProperty<T> slaveProperty, String module, String masterProperty);
	void setArguments(Map<String, String> arguments);
	Map<String, String> arguments();
}
