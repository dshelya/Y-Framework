package app.context.services.interfaces;

import static java.time.format.DateTimeFormatter.ofPattern;

import java.io.File;
import java.io.IOException;
import java.time.format.DateTimeFormatter;

import app.context.services.impl.SystemLogger.Mode;

public interface DetachedLoggerService {
	DateTimeFormatter FORMATTED_DATE = ofPattern("yyyy-MM-dd HH:mm:ss");

	void dumpAndOpenFile(String data, String filename) throws IOException;
	File dumpToFile(String html, String string);	
	void openFile(String filePath) throws IOException;

	void log(Mode mode, String messagef, Object ... holders);	
	void log(Mode mode, Throwable error);

	void debug(String messagef, Object ... holders);
	void info(String messagef, Object ... holders);
	void warn(String messagef, Object ... holders);
	void critial(String messagef, Object ... holders);
	
	void debug(Throwable e);
	void info(Throwable e);
	void warn(Throwable e);
	void critial(Throwable e);
	
	DetachedLoggerService tag(Class<?> taggedClass);
	DetachedLoggerService tag(String tag);
	
	<T extends Throwable> T getRootCause(Throwable exception);
	<T extends Throwable> T getSpecificCause(Throwable exception, Class<? extends Throwable> relation);

	boolean isRelated(Throwable exception, Class<? extends Throwable> relation);

 	boolean isDebugMode();
	void setMode(Mode mode);
}
