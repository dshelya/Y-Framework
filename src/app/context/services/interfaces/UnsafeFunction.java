package app.context.services.interfaces;

@FunctionalInterface
public interface UnsafeFunction<A,R> {
	R apply(A argument) throws Exception;
}
