package app.context.services.interfaces;

import java.util.Optional;

import app.context.services.impl.DatabasePatchService;
import app.module.api.GenericModule;

public interface Context {
	enum WorkingMode {
		LIVE, SIMULATION;
	}

	DatabaseService getDatabase();
	GuiAccessService getGui();
	SchedulerService getScheduler();
 	LoggerService getLogger();
	NotificationService getNotifier();
	ConfigService getConfig();
	WebViewer getWebViewer();
	DatabasePatchService getPatchService();
	WebViewService getDefaultWebViewer();
	void setVersion(String version);
	void shutdown();
	String applicationName();
	String version();
	String buildNumber();
	void setApplicationName(String applicationName);
	void setWorkingMode(WorkingMode mode);
	WorkingMode workingMode();
	void registerModule(String name, GenericModule module);
	<M extends GenericModule> Optional<M> getModule(String name);

	default String title() {
		return applicationName() + "  (" + version() + "."+buildNumber()+")";
	}
}

