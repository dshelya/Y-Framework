package app.context.services.interfaces;

import java.net.URI;
import java.util.function.Consumer;

import app.module.api.ExternalProperty;
import javafx.geometry.Point2D;
import javafx.scene.Node;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.input.KeyCombination;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

public interface GuiAccessService {	
	double screenX();
	double screenY();
	double getDefaultWidth();
	double getDefaultHeight();
	
	Point2D getAnchoredPoint(Node anchor);
 	
	Consumer<Double> showProgress(String label);
	Consumer<Double> showProgress(String label, Node anchor, int offsetX, int offsetY);

	void copyToClipboard(String data);
	Stage showModal(Pane modal);
	void assignWorkingModeSwitchHotkey(KeyCombination workingModeKotkey);

	void attachTo(Stage mainWindow);
	void showAlert(String string);
	void showModalAlert(String message);			
	void showMessage(String header, String message, AlertType type);
	void addNode(Node node);
	void removeNode(Node node);
	
	ExternalProperty<Stage> mainWindow();
	void browse(URI url);
	void focus();

}
