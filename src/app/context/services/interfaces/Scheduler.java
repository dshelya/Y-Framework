package app.context.services.interfaces;

public interface Scheduler {
	SchedulerService getSchedulerService();
	public void shutdown();
}