package app.context.services.interfaces;

import javafx.scene.web.WebView;

public interface WebViewer {
	WebViewService getWebViewService(SchedulerService scheduler);
	WebViewService getWebViewService(WebView browser, SchedulerService scheduler);
	public void shutdown();
}