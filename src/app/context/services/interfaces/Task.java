package app.context.services.interfaces;

import java.util.concurrent.Future;
import java.util.concurrent.FutureTask;
import java.util.concurrent.ThreadLocalRandom;
import java.util.function.Consumer;

@FunctionalInterface
public interface Task {
	void run() throws Exception;
	
	static Task named(Task unnamed, String name) {
		return () -> {
	    	String oldName = setName(name);

		    try {
		    	unnamed.run();
		    } finally {
		    	purge(oldName);
		    }
		};
	}
	
	default Task andThen(ReturnTask<Task> next) {
		return () -> {
			run();
			next.run();
		};
	}

	default Task andThen(Task next) {	
		return () -> {
			run();
			next.run();
		};
	}
	
	default <T> ReturnTask<T> andThenReturn(ReturnTask<T> next) throws Exception {
		run();
		return next;
	}	
	
	static Task buildChain(Task ... tasks) {
		Task base = () -> {};
		
		for (Task task : tasks) {
			base = base.andThen(task);
		}
		
		return base;
	}
	
	static String setName(String name) {	
		String oldName = Thread.currentThread().getName();
		Thread.currentThread().setName(name+"_"+ThreadLocalRandom.current().nextInt(0, 100));
		
		return oldName;
	}
	
	static void purge(String oldName) {	
		Thread.currentThread().setName(oldName);
		//Thread.currentThread().interrupt(); 
	}	

	default Task onError(Consumer<Exception> errorHandler) {
		return () -> {
			try {
				run();
			} catch (Exception e) {
				errorHandler.accept(e);
			}			
		};	
	}
	
	default Task onFuture(Consumer<Future<?>> consumer) {
		return () -> {			
			FutureTask<Void> futureTask = new FutureTask<>(() -> {
				run();
				return null;				
			});
			
			consumer.accept(futureTask);
			futureTask.run();
		};	
	}

	static Task of(Task task) {
		return task;
	}
}
