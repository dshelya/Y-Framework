package app.context.services.interfaces;

import java.util.function.Consumer;
import java.util.function.Function;

@FunctionalInterface
public interface ReturnTask<T> {
	T run() throws Exception;
	
	public static void main(String[] args) {
		new Thread(() -> {
			try {
				System.out.println("PRESTART");
				
				String r = ReturnTask.named(() -> {
					return "TASK FNISHED";
				}, "X task").run();
				
				System.out.println(r);
				
			} catch (Exception e) {
				e.printStackTrace();
			}
		}).start();		
	}
	
	static <T> ReturnTask<T> named(ReturnTask<T> unnamed, String name) {
		return () -> {
	    	String oldName = Task.setName(name);

		    try {
		    	return unnamed.run();
		    } finally {
		    	Task.purge(oldName);
		    }
		};
	}
	
	default ReturnTask<T> andThen(Function<T, ReturnTask<T>> function) throws Exception {
		return function.apply(run());		
	}
	
	default ReturnTask<T> andThen(Task task) throws Exception {
		return () -> {
			T result = run();
			task.run();
			
			return result;
		};
	}
	
	static <T> ReturnTask<T> of(ReturnTask<T> next) {
		return next;
	}
	
	static <T> ReturnTask<T> of(Task next) {
		return () -> {
			next.run();
			return null;
		};
	}
	
	default void andThen(Consumer<T> terminator) throws Exception {
		terminator.accept(run());		
	}
}
