package app.context.services.interfaces;

import static java.time.format.DateTimeFormatter.ofPattern;

import java.io.File;
import java.io.IOException;
import java.time.format.DateTimeFormatter;

public interface LoggerService {
	DateTimeFormatter FORMATTED_DATE = ofPattern("yyyy-MM-dd HH:mm:ss");

	void dumpAndOpenFile(String data, String filename) throws IOException;
	File dumpToFile(String html, String string);	
	void openFile(String filePath) throws IOException;

	void debug(String messagef, Object ... holders);	
	void info(String messagef, Object ... holders);	
	
	void debug(Throwable e);
	void info(Throwable e);
	
	LoggerService tag(Class<?> taggedClass);
	LoggerService tag(String tag);
	
	<T extends Throwable> T getRootCause(Throwable exception);
	<T extends Throwable> T getSpecificCause(Throwable exception, Class<? extends Throwable> relation);

	boolean isRelated(Throwable exception, Class<? extends Throwable> relation);

	void setDebugMode(boolean isDebug);
	boolean isDebugMode();
	boolean isUnrecoverable(Exception e);
}
