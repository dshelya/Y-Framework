package app.context.services.interfaces;

import java.util.function.Supplier;

import org.w3c.dom.Document;
import org.w3c.dom.html.HTMLBodyElement;

import app.context.AppClient;
import app.crawler.api.WebCrawler;
import javafx.scene.input.KeyCode;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.web.WebView;

public interface WebViewService {	
	void showBrowser(boolean show);

	void simulateInput(String source);
	void simulateInput(String source, Supplier<Integer> minimalDelay);
	
	void simulateInput(KeyCode key, boolean shift, boolean ctrl, boolean alt);
	void simulateInput(KeyCode key, String letter, boolean shift, boolean ctrl, boolean alt);
	void simulatePressButton(KeyCode key, String letter, boolean shift, boolean ctrl, boolean alt);
	void simulateReleaseButton(KeyCode key, String letter, boolean shift, boolean ctrl, boolean alt);

	void simulateClick(String cssQuery, int maxOffsetRight, int maxOffsetDown);

	void pressEnterKey();
	void releaseEnterKey();
	
	MouseDraggable simulateMousePress(String cssQuery, int offsetRight, int offsetDown);

	String runJs(String script);
	void disableJs(boolean disable);
	boolean elementExists(String cssQuery);
	String elementText(String cssQuery);
	String elementAttr(String cssQuery, String attr);
	boolean elementVisible(String cssQuery);

	void onLoad(Task onLoadTask);
	void load(String url);
	void load(String url, Task onLoadTask);
	void waitUntilSuccess();
	void reload();
	//void reset();
	
 	String getActiveHtml();
	String location();
	Document document();
	HTMLBodyElement body();
	
	WebCrawler crawler();
	void bindExternalBrowser(WebView browser);	

	static String getUserAgent() {
		return "Mozilla/5.0 (Windows NT 6.2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.102 Safari/537.36";
	}
	
	class MouseDraggable {
		private ElementBounds bounds;
		private WebView browser;
		private boolean isReleased;

		public MouseDraggable(ElementBounds bounds, WebView browser) {
			this.bounds = bounds;
			this.browser = browser;
		}
		
		public void dragTo(int offsetX, int offsetY) {
			if (isReleased)
				throw new IllegalStateException("The object has been already released");
			
			bounds.offsetTo(offsetX, offsetY);
			MouseEvent drag = new MouseEvent(null, browser, MouseEvent.MOUSE_DRAGGED, bounds.getElementX(), bounds.getElementY(), bounds.getScreenX(), bounds.getScreenY(), MouseButton.PRIMARY, 1,false, false, false, false, false, false, false, false, false, false, null);
		
			AppClient.getInstance().getScheduler().runInMainThread(() -> {
				browser.fireEvent(drag);
				return "OK";
			}).intern();	
		}

		public void release() {
			MouseEvent release = new MouseEvent(null, browser, MouseEvent.MOUSE_RELEASED, bounds.getElementX(), bounds.getElementY(), bounds.getScreenX(), bounds.getScreenY(), MouseButton.PRIMARY, 1,false, false, false, false, false, false, false, false, false, false, null);
		
			AppClient.getInstance().getScheduler().runInMainThread(() -> {
				browser.fireEvent(release);
				return "OK";
			}).intern();	
			
			isReleased = true;
		}
	}

	class ElementBounds {
		private double elementX;
		private double elementY;
		private double screenX;
		private double screenY;
		private double zeroPointY;
		private double zeroPointX;

		public ElementBounds(double elementX, double elementY, double screenX, double screenY) {
			this.elementX = elementX;
			this.elementY = elementY;
			this.screenX = screenX;
			this.screenY = screenY;
			
			this.zeroPointX = screenX - elementX;
			this.zeroPointY = screenY - elementY;
		}
		
		public void offsetTo(int offsetX, int offsetY) {
			this.elementX += offsetX;
			this.elementY += offsetY;
			
			this.screenX = zeroPointX + elementX;
			this.screenY = zeroPointY + elementY;
		}

		public double getElementX() {
			return elementX;
		}

		public double getElementY() {
			return elementY;
		}

		public double getScreenX() {
			return screenX;
		}

		public double getScreenY() {
			return screenY;
		}
	}
}
