package app.context.services;

@SuppressWarnings("serial")
public class SystemException extends Exception {

	public SystemException(String string) {
		super(string);
	}

	public SystemException(Exception exception) {
		super(exception);
	}
}
