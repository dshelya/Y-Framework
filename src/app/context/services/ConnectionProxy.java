package app.context.services;

import java.sql.Connection;
import java.sql.SQLException;
import java.time.LocalDateTime;

public class ConnectionProxy {
	private Connection connection;
	private LocalDateTime lastAccess;

	public ConnectionProxy(Connection connection) {
		this.connection = connection;
		this.lastAccess = LocalDateTime.now();
	}
	
	public Connection get() {
		this.lastAccess = LocalDateTime.now();
		return connection;
	}
	
	public LocalDateTime lastAccessed() {
 		return lastAccess;
	}
	
	public boolean isValid() {
		try {
			return !connection.isClosed();
		} catch (SQLException e) {
			return false;
		}
	} 
	
	public void close() throws SQLException {
		connection.close();
	} 
}
