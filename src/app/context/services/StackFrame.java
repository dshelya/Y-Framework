package app.context.services;


import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public final class StackFrame {
	private Class<?> frameClass;
	private String className;	
	private List<String> implementedIterfaces;
	private String superClass;
	private String calledMethod;
	
	public StackFrame(StackTraceElement frame) {		
		try {
			className = frame.getClassName();
			frameClass = Class.forName(className);
			calledMethod = frame.getMethodName();			
			
			implementedIterfaces = Arrays.asList(frameClass.getInterfaces())
				.stream()
				.map(Class::getName)
				.collect(Collectors.toList());
			
			superClass = frameClass.getSuperclass().getName();
		} catch (Throwable e) {
			//Suppress
		}
	}
	
	public static List<StackFrame> getCallChain() {
		return Arrays.asList(Thread.currentThread().getStackTrace())
			.stream()
			.map(StackFrame::new)
			.collect(Collectors.toList());
	}
	
	public static StackFrame getDirectCallerOf(Object object) {
		List<StackFrame> callChain = getCallChain();
		String callee = object.getClass().getName();
		StackFrame caller = null;
		
		for (int i = 0; i < callChain.size() - 1; i++) {
			caller = callee.equals(callChain.get(i).className) ? callChain.get(i + 1) : caller;
		}

		return caller;
	}

	public Class<?> getFrameClass() {
		return frameClass;
	}

	public String getClassName() {
		return className;
	}

	public List<String> getImplementedIterfaces() {
		return implementedIterfaces;
	}

	public String getSuperClass() {
		return superClass;
	}

	public String getCallerMethod() {
		return calledMethod;
	}
	
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("StackFrame [");
		if (frameClass != null)
			builder.append("frameClass=").append(frameClass).append(", ");
		if (className != null)
			builder.append("className=").append(className).append(", ");
		if (implementedIterfaces != null)
			builder.append("implementedIterfaces=").append(implementedIterfaces).append(", ");
		if (superClass != null)
			builder.append("superClass=").append(superClass).append(", ");
		if (calledMethod != null)
			builder.append("calledMethod=").append(calledMethod);
		builder.append("]");
		return builder.toString();
	}
}