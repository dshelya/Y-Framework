package app.context.services;

@SuppressWarnings("serial")
public class SystemRuntimeException extends RuntimeException {

	public SystemRuntimeException(String string) {
		super(string);
	}

	public SystemRuntimeException(Exception exception) {
		super(exception);
	}

}
