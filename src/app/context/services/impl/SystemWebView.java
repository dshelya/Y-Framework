package app.context.services.impl;

import java.io.StringWriter;
import java.security.SecureRandom;
import java.util.concurrent.CountDownLatch;
import java.util.function.Function;
import java.util.function.Supplier;

import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.html.HTMLBodyElement;

import app.context.services.GenericService;
import app.context.services.interfaces.Context;
import app.context.services.interfaces.SchedulerService;
import app.context.services.interfaces.Task;
import app.context.services.interfaces.WebViewService;
import app.context.services.interfaces.WebViewer;
import app.crawler.api.WebCrawler;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.concurrent.Worker.State;
import javafx.geometry.Bounds;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;

final class SystemWebView extends GenericService implements WebViewer {
	private static final SecureRandom RANDOM = new SecureRandom();
	private static final Function<String,String> VISIBILITY_JS = (CSS) -> "function isVisible(element) {" + 
			"	if (element.tagName == 'BODY')" + 
			"		return true;" + 
			"	if (element.style.display === 'none')" + 
			"		return false;" + 
			"	return isVisible(element.parentNode);    " + 
			"}; isVisible(document.querySelectorAll('"+CSS+"')[0]);";

	protected SystemWebView(Context context) {
		super(context);	
	}

	@Override
	public WebViewService getWebViewService(SchedulerService scheduler) {
		return new SafeWebViwer(scheduler);
	}
	
	@Override
	public WebViewService getWebViewService(WebView browser, SchedulerService scheduler) {
		return new SafeWebViwer(browser, scheduler);
	}
	
	
	@Override
	public void shutdown() {		
	}
	
	public class SafeWebViwer implements WebViewService {

		private WebView browser;
		private WebEngine engine;
		private ChangeListener<State> activeOnLoadListener;
		private SchedulerService scheduler;
		
		public SafeWebViwer(SchedulerService scheduler) {
			this.scheduler = scheduler;
			
			browser = new WebView();
			browser.setId("internal-browser");
			bindExternalBrowser(browser);
 		}
	
		public SafeWebViwer(WebView browser, SchedulerService scheduler) {
			this.scheduler = scheduler;
			bindExternalBrowser(browser);
 		}

		@Override
		public void bindExternalBrowser(WebView browser) {
			this.browser = browser;
			this.engine = browser.getEngine();
		}
		
		@Override
		public void showBrowser(boolean show) {
			browser.setVisible(show);		
			
			scheduler.runInMainThread(() -> {
				if (show) {				
					gui.addNode(browser);
	
					gui.mainWindow().get().setWidth(browser.getPrefWidth());        
					gui.mainWindow().get().setHeight(browser.getPrefHeight());			
				} else {
					gui.removeNode(browser);

					//gui.mainWindow().get().setMinWidth(gui.getDefaultWidth());    
					gui.mainWindow().get().setWidth(gui.getDefaultWidth());        

					//gui.mainWindow().get().setMinHeight(gui.getDefaultHeight());		
					gui.mainWindow().get().setHeight(gui.getDefaultHeight());
				}
			});
		}
				
		@Override
		public WebCrawler crawler() {
			return WebCrawler.getCrawler(scheduler);
		}
	 
		@Override
		public void onLoad(Task onLoadTask) {
			Platform.runLater(() -> {
				if (activeOnLoadListener != null) {
					engine.getLoadWorker().stateProperty().removeListener(activeOnLoadListener);
				}
		
				this.activeOnLoadListener = (V, W, N) -> {
					if (N == State.SUCCEEDED) {
						scheduler.runSafe(() -> onLoadTask.run());
						engine.getLoadWorker().stateProperty().removeListener(activeOnLoadListener);
						activeOnLoadListener = null;
					}
				};
		
				engine.getLoadWorker().stateProperty().addListener(activeOnLoadListener);
			});
		}
		
		@Override
		public boolean elementExists(String cssQuery) {
			return "true".equals(runJs("document.querySelectorAll('"+cssQuery+"').length > 0"));
		}
		
		@Override
		public String elementText(String cssQuery) {
			return elementAttr(cssQuery, "textContent");
		}
		
		@Override
		public boolean elementVisible(String cssQuery) {
			if (elementExists(cssQuery)) {
				return "true".equals(runJs(VISIBILITY_JS.apply(cssQuery)));
			}
			
			return false;
		}	
	
		@Override
		public String elementAttr(String cssQuery, String attr) {
			if (elementExists(cssQuery))
				return runJs("document.querySelectorAll('"+cssQuery+"')[0]."+attr);
			
			return "";
		}
		
		@Override
		public String runJs(String script) {
			if (Platform.isFxApplicationThread())
				return engine.executeScript(script).toString();
			
			return scheduler.runInMainThread(() -> engine.executeScript(script).toString());
		}
		
		private KeyCode parseSpecial(String sign) {
			switch (sign) {
				case "@": return KeyCode.AT;
				case ".": return KeyCode.PERIOD;
				case "#": return KeyCode.NUMBER_SIGN;
				case "$": return KeyCode.DOLLAR;
				//case "%": return KeyCode.pe
				case "^": return KeyCode.CIRCUMFLEX;
				case "&": return KeyCode.AMPERSAND;
				case "*": return KeyCode.ASTERISK;
				case "(": return KeyCode.LEFT_PARENTHESIS;
				case ")": return KeyCode.RIGHT_PARENTHESIS;
				case "_": return KeyCode.UNDERSCORE;
				case "+": return KeyCode.PLUS;
				case "\"": return KeyCode.QUOTEDBL;
				case ":": return KeyCode.COLON;
				case ";": return KeyCode.SEMICOLON;
				case "\\": return KeyCode.BACK_SLASH;
				//case "?": return KeyCode
				case "!": return KeyCode.EXCLAMATION_MARK;
				case "-": return KeyCode.MINUS;
				case "=": return KeyCode.EQUALS;
				case "/": return KeyCode.SLASH;
				case "{": return KeyCode.BRACELEFT;
				case "}": return KeyCode.BRACERIGHT;
				case "'": return KeyCode.QUOTE;					
				case "[": return KeyCode.OPEN_BRACKET;
				case "]": return KeyCode.CLOSE_BRACKET;
				case "<": return KeyCode.LESS;
				case ">": return KeyCode.GREATER;
				case " ": return KeyCode.SPACE;		
			}
			
			return null;
		}	
		
		@Override
		public void pressEnterKey() {
			scheduler.runInMainThread(() -> {
				KeyEvent press = new KeyEvent(browser, browser, KeyEvent.KEY_PRESSED, "\r", "", KeyCode.ENTER, false, false, false, false);
				browser.fireEvent(press);
				
				//return true;
			});
		}
		
		@Override
		public void releaseEnterKey() {	
			scheduler.runInMainThread(() -> {
				KeyEvent typed = new KeyEvent(browser, browser, KeyEvent.KEY_TYPED, "\r", "", KeyCode.ENTER, false, false, false, false);
				KeyEvent release = new KeyEvent(browser, browser, KeyEvent.KEY_RELEASED, "\r", "", KeyCode.ENTER, false, false, false, false);
				
				browser.fireEvent(release);		
				browser.fireEvent(typed);
				
				//return true;
			});
		}
		
		@Override
		public void simulateInput(KeyCode key, String letter, boolean shift, boolean ctrl, boolean alt) {
			scheduler.runInMainThread(() -> {
				KeyEvent press = new KeyEvent(null, browser, KeyEvent.KEY_PRESSED, letter, letter, key, shift, ctrl, alt, false);
				KeyEvent typed = new KeyEvent(null, browser, KeyEvent.KEY_TYPED, letter , letter, key, shift, ctrl, alt, false);
				KeyEvent release = new KeyEvent(null, browser, KeyEvent.KEY_RELEASED, letter, letter, key, shift, ctrl, alt, false);
		
				browser.fireEvent(press);
				browser.fireEvent(typed);		
				browser.fireEvent(release);
				
				return true;
			});
		}
		
		@Override
		public void simulatePressButton(KeyCode key, String letter, boolean shift, boolean ctrl, boolean alt) {
			scheduler.runInMainThread(() -> {
				KeyEvent press = new KeyEvent(null, browser, KeyEvent.KEY_PRESSED, letter, letter, key, shift, ctrl, alt, false);
				browser.fireEvent(press);
				
				//return true;
			});
		}
		
		@Override
		public void simulateReleaseButton(KeyCode key, String letter, boolean shift, boolean ctrl, boolean alt) {
			scheduler.runInMainThread(() -> {
				KeyEvent typed = new KeyEvent(null, browser, KeyEvent.KEY_TYPED, letter , letter, key, shift, ctrl, alt, false);
				KeyEvent release = new KeyEvent(null, browser, KeyEvent.KEY_RELEASED, letter, letter, key, shift, ctrl, alt, false);
		
				browser.fireEvent(typed);		
				browser.fireEvent(release);
				
				//return true;
			});
		}
	
		@Override
		public void simulateInput(KeyCode key, boolean shift, boolean ctrl, boolean alt) {
			simulateInput(key, null, shift, ctrl, alt);
		}
		
		@Override
		public void simulateInput(String source, Supplier<Integer> minimalDelay) {
			source.chars().forEach(X -> {
				char key = (char) X;
				String letter = key + "";
	
				boolean isShiftDown = Character.isUpperCase(key);
				boolean isAlphabetic = Character.isLetterOrDigit(key);
	
				KeyCode keyCode = isAlphabetic ? KeyCode.getKeyCode(letter.toUpperCase()) : parseSpecial(letter);
	
				if (keyCode == null) {
					throw new RuntimeException("Unable to find keycode for: " + letter.toUpperCase());
				}
	
				scheduler.runInMainThread(() -> {
					simulatePressButton(keyCode, letter, isShiftDown, false, false);
					scheduler.sleep(RANDOM.nextInt(60) + 5);
					
					simulateReleaseButton(keyCode, letter, isShiftDown, false, false);
					scheduler.sleep(RANDOM.nextInt(40) + 5 + minimalDelay.get());
					
					return true;
				});	
			});
		}
		
		@Override
		public void simulateInput(String source) {
			simulateInput(source, () -> RANDOM.nextInt(100) + 60);
		}
	
		private Bounds getBrowserBounds() {
			return browser.localToScene(browser.getBoundsInLocal());
		}
		
		private ElementBounds getBoundsForElement(String cssQuery, int offsetRight, int offsetDown) {
			Bounds browserBounds = getBrowserBounds();
			
			double offsetX = offsetRight + browserBounds.getMinX();
			double offsetY = offsetDown + browserBounds.getMinY();	
			
			Double elementX = Double.valueOf(
				(String) scheduler.runInMainThread(
					() -> engine.executeScript("var elements = document.querySelectorAll('"+cssQuery+"');"
						+ "if (elements.length > 0) {"
						+ "		elements[0].scrollIntoView();"
						+ "		elements[0].focus();"
						+ "		elements[0].getBoundingClientRect().left + "+offsetX+" + '';"
						+ "}"
					)
				)
			);
			
			Double elementY = Double.valueOf(
				(String) scheduler.runInMainThread(
					() -> engine.executeScript("var elements = document.querySelectorAll('"+cssQuery+"');"
						+ "if (elements.length > 0) {"
						+ "		elements[0].getBoundingClientRect().top + "+offsetY+" + '';"
						+ "}"
					)
				)
			);	
			
			double screenX = gui.screenX() + elementX;
			double screenY = gui.screenY() + elementY;	
			
			return new ElementBounds(elementX, elementY, screenX, screenY);
		}
		
		@Override
		public void simulateClick(String cssQuery, int maxElementOffsetRight, int maxElementOffsetDown) {
			int offsetRight =  RANDOM.nextInt(maxElementOffsetRight) + 3;
			int offsetDown = RANDOM.nextInt(maxElementOffsetDown);
			
			if (!elementExists(cssQuery)) {
				getLogger().debug("Cannot simulate click. Element [%s] is not found", cssQuery);
				return;
			}
			
			ElementBounds bounds = getBoundsForElement(cssQuery, offsetRight, offsetDown);
			
			MouseEvent enter = new MouseEvent(null, browser, MouseEvent.MOUSE_ENTERED, bounds.getElementX(), bounds.getElementY(), bounds.getScreenX(), bounds.getScreenY(), MouseButton.PRIMARY, 1,false, false, false, false, false, false, false, false, false, false, null);
			MouseEvent click = new MouseEvent(null, browser, MouseEvent.MOUSE_CLICKED, bounds.getElementX(), bounds.getElementY(), bounds.getScreenX(), bounds.getScreenY(), MouseButton.PRIMARY, 1,false, false, false, false, false, false, false, false, false, false, null);
			MouseEvent press = new MouseEvent(null, browser, MouseEvent.MOUSE_PRESSED, bounds.getElementX(), bounds.getElementY(), bounds.getScreenX(), bounds.getScreenY(), MouseButton.PRIMARY, 1,false, false, false, false, false, false, false, false, false, false, null);
			MouseEvent release = new MouseEvent(null, browser, MouseEvent.MOUSE_RELEASED, bounds.getElementX(), bounds.getElementY(), bounds.getScreenX(), bounds.getScreenY(), MouseButton.PRIMARY, 1,false, false, false, false, false, false, false, false, false, false, null);
			
			scheduler.runInMainThread(() -> {
				browser.fireEvent(enter);
				browser.fireEvent(press);
				browser.fireEvent(release);
				browser.fireEvent(click);
	
				return null;
			});		
		}	
		
		@Override
		public MouseDraggable simulateMousePress(String cssQuery, int offsetRight, int offsetDown) {
			if (!elementExists(cssQuery)) {
				getLogger().debug("Cannot simulate mouse press. Element [%] is not found", cssQuery);
				//return null;
			}
			
			ElementBounds bounds = getBoundsForElement(cssQuery, offsetRight, offsetDown);		
			MouseEvent press = new MouseEvent(null, browser, MouseEvent.MOUSE_PRESSED, bounds.getElementX(), bounds.getElementY(), bounds.getScreenX(), bounds.getScreenY(), MouseButton.PRIMARY, 1,false, false, false, false, false, false, false, false, false, false, null);
	
			scheduler.runInMainThread(() -> {
				browser.fireEvent(press);
				//return "OK";
			});
			
			return new MouseDraggable(bounds, browser);
		}	
		
		@Override
		public void load(String url) {			
			if (Platform.isFxApplicationThread())
				 engine.load(url);
			else
				scheduler.runInMainThread(() -> engine.load(url));
		}
		
		@Override
		public void load(String url, Task onLoadTask) {	
			CountDownLatch latch = new CountDownLatch(1);
			
			Task synronousTask = () -> {
				scheduler.backgroundRun(() -> {
					onLoadTask.run();
					latch.countDown();
				});
			};
			
			onLoad(synronousTask);
			scheduler.runInMainThread(() -> engine.load(url));			
			scheduler.runSafe(latch::await);
		}
		
		@Override
		public void reload() {
			if (Platform.isFxApplicationThread())
				 engine.reload();
			else
				scheduler.runInMainThread(() -> engine.reload());
		}	
	
		//@Override
		private State getState() {
			if (Platform.isFxApplicationThread())
				return engine.getLoadWorker().getState();
			
			return scheduler.runInMainThread(() -> engine.getLoadWorker().getState());		
		}
		
		@Override
		public void waitUntilSuccess() {
			while (getState() != State.SUCCEEDED) {
				scheduler.sleep(50);
			}	
		}	
	
		@Override
		public void disableJs(boolean disable) {
			if (Platform.isFxApplicationThread())
				 engine.setJavaScriptEnabled(!disable);
			else
				scheduler.runInMainThread(() -> engine.setJavaScriptEnabled(!disable));
		}
	
		@Override
		public String getActiveHtml() {		    
			Document document = Platform.isFxApplicationThread() 
				?  engine.getDocument()
				:  scheduler.runInMainThread(() -> engine.getDocument());
				
			return scheduler.backgroundRun(() -> {
				Transformer transformer = TransformerFactory.newInstance().newTransformer();
				StreamResult result = new StreamResult(new StringWriter());
				transformer.transform(new DOMSource(document), result);
	
				return result.getWriter().toString();
			});
	
		}
	
		@Override
		public String location() {
			return Platform.isFxApplicationThread() 
				? engine.getLocation()
				: scheduler.runInMainThread(() -> engine.getLocation());
		}
	
		@Override
		public Document document() {
			return Platform.isFxApplicationThread() 
					? engine.getDocument()
					: scheduler.runInMainThread(() -> engine.getDocument());
		}
	
		@Override
		public HTMLBodyElement body() {
			return (HTMLBodyElement) document().getElementsByTagName("body").item(0);
		}
	}
}
