package app.context.services.impl;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.nio.channels.WritableByteChannel;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.UUID;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;

import org.apache.poi.ss.formula.eval.NotImplementedException;

import app.context.services.ConnectionProxy;
import app.context.services.DatabaseWrapper;
import app.context.services.GenericService;
import app.context.services.db.ConditionalStatement;
import app.context.services.interfaces.Context;
import app.context.services.interfaces.DatabaseService;
import app.context.services.interfaces.LoggerService;
import app.context.services.interfaces.SchedulerService;
import app.context.services.interfaces.Task;
import app.context.services.interfaces.UnsafeConsumer;
import app.context.services.interfaces.UnsafeFunction;

final class SystemDatabaseService extends GenericService implements DatabaseService {
	private static final String DB_EXTENSION = ".mv.db";

	private Connection defaultConnection;

	private String dbFileName;
	private LoggerService log;
	private List<ConnectionProxy> pool;
	private String database;
	private SchedulerService scheduler;
	private AtomicBoolean initialized = new AtomicBoolean();

	protected SystemDatabaseService(Context context) {
		super(context);
		
		this.log = getLogger();		
		this.pool = new CopyOnWriteArrayList<>();		
		this.scheduler = context.getScheduler();		
		
		// Setup a pool cleaner process
		scheduler.backgroundRunReccurent(Task.named(() -> {			
			try {
				ArrayList<ConnectionProxy> aged = new ArrayList<>();
				
				for (ConnectionProxy connection : pool) {
					LocalDateTime accessTime = connection.lastAccessed();
					
					if (ChronoUnit.MINUTES.between(accessTime, LocalDateTime.now()) > 10 || !connection.isValid()) 
						aged.add(connection);		
				}
				
				if (!aged.isEmpty()) {
					log.debug("Found aged connections: %s", aged.size());
					
					for (ConnectionProxy connection : aged) {
						closeConnection(connection.get());
						pool.remove(connection);
					}
				}				
			} catch (Exception e) {
				log.debug("DB pool clear error %s", e.getMessage());
				log.debug(e);
			}
			
			scheduler.purgeTasks();			
		}, "Database pool cleaner"), 3, TimeUnit.MINUTES);		
	}
 
	@Override
	public void bindDBFileName(String dbFileName) {
		this.dbFileName = dbFileName;
		this.database = String.format("jdbc:h2:%s;AUTO_SERVER=TRUE;AUTO_RECONNECT=TRUE;FILE_LOCK=SOCKET", config.getPathFor(dbFileName));		
		
		context.getPatchService().patchIfAvailable();
	}	

	public void initDatabases() {
		if (dbFileName == null) {
			log.info("Init Database error: Please assign the default database to the controller.");
			return;
		}

		try {
			Class.forName("org.h2.Driver");
			File dbFilePath = config.getPathFor(dbFileName + DB_EXTENSION);

			if (!dbFilePath.exists())
				extractDefaultDatabse();
			 
			defaultConnection = DriverManager.getConnection(database);
		} catch (Exception cnfe) {
			log.info("Error: " + cnfe.getMessage());
			throw new RuntimeException(cnfe);
		}
		
		initialized.set(true);
	}

	private void extractDefaultDatabse() {
		File path = config.getPathFor(dbFileName + DB_EXTENSION);
		Objects.requireNonNull(getClass().getClassLoader().getResource(dbFileName + DB_EXTENSION), 
				"Default database with name ["+dbFileName+".mv.db] is not found. Cannot continue");

		try (
			final InputStream in = getClass().getClassLoader().getResourceAsStream(dbFileName + DB_EXTENSION);
			final OutputStream out = new FileOutputStream(path);				

			final ReadableByteChannel inChannel = Channels.newChannel(in);
			final WritableByteChannel outChannel = Channels.newChannel(out);
		) {
			final ByteBuffer buffer = ByteBuffer.allocateDirect(16 * 1024);
			while (inChannel.read(buffer) != -1) {
				buffer.flip();
				outChannel.write(buffer);
				buffer.compact();
			}

			buffer.flip();
			while (buffer.hasRemaining())
				outChannel.write(buffer);

			in.close();
			out.close();
			inChannel.close();
			outChannel.close();

		} catch (Exception e) {
			path.delete();
			throw new RuntimeException(e);
		}
	}

	@Override
	public void shutdown() {
		if (initialized.get()) {
			closeConnection(defaultConnection);
		}
		
		for (ConnectionProxy connection : pool) {
			closeConnection(connection.get());				
		}
		
		pool.clear();			
		scheduler.cancelTasks();
	}
	
	private void closeConnection(Connection connection) {
		try {
			if (connection != null && !connection.isClosed()) {
				if (!connection.getAutoCommit())
					connection.commit();
				
				connection.close();	
			}
		} catch (SQLException e) {
			log.info("Unable to close database connection");
		}
	}
 
	@Override
	public <T> List<T> convert(String sql, UnsafeFunction<ResultSet, T> handler) throws Exception {
		return select(sql, false, handler);		 
	}
	
	@Override
	public <T> T convertFirst(String sql, UnsafeFunction<ResultSet, T> handler) throws Exception {
		return select(sql, true, handler).stream().findFirst().orElse(null);
	}
	
	@Override
	public void process(String sql, UnsafeConsumer<ResultSet> handler) throws Exception {
		select(sql, false, handler);		 
	}
	
	@Override
	public void processFirst(String sql, UnsafeConsumer<ResultSet> handler) throws Exception {
		select(sql, true, handler);
	}
	
	private <T> List<T> select(String sql, boolean first, UnsafeFunction<ResultSet, T> handler) throws Exception {
		List<T> processed = new ArrayList<>();
		
		try (
			Connection db = newConnection().get();
			Statement statement = db.createStatement();
			ResultSet result = statement.executeQuery(sql)
		) {			 
			 if (first && result.next()) {
				 processed.add(handler.apply(result));
				 return processed;			 
			 } 		

			 while(result.next()) {
			 	processed.add(handler.apply(result));
			 }
			 
			 return processed;
		} 		 
	}
	
	private void select(String sql, boolean first, UnsafeConsumer<ResultSet> handler) throws Exception {
		try (
			Connection db = newConnection().get();
			Statement statement = db.createStatement();
			ResultSet result = statement.executeQuery(sql)
		) {		 
			if (first && result.next()) {
				handler.accept(result);			
				return;
			} 
		
			while(result.next()) {
				handler.accept(result);
			}			
		}	 		 
	}

	@Override
	public void execute(String sql) throws SQLException {
		try (Connection connection = newConnection().get(); Statement statement = connection.createStatement()) {
			statement.executeUpdate(sql);
		}
	}

	@Override
	public ConnectionProxy newConnection() throws SQLException {
		if (!initialized.get())
			initDatabases();
		
		//org.h2.tools.Server.createTcpServer("-tcpAllowOthers").start();
		ConnectionProxy connection = new ConnectionProxy(DriverManager.getConnection(database));
		pool.add(connection);
		
		return connection;
	}

	@Override
	public String getUUID() throws SQLException {
		return UUID.randomUUID().toString().replace("-", "").toUpperCase();
	}
  
	@Override
	public <T extends DatabaseWrapper> T wrapBy(Class<T> wrapperClass) throws Exception {
		return wrapperClass.newInstance().<T>bindService(this);
	}

	@Override
	public <I> void manage(ConditionalStatement<I> enitityProxy) throws SQLException {
		enitityProxy.execute(newConnection());				
	}

	@Override
	public void release(ResultSet result) {
		try {
			if (result != null)
				result.close();
		} catch (Exception e) {
			getLogger().debug(e);
		}
	}
	
	@Override
	public void interrupt() {
		synchronized (pool) {		
			for (ConnectionProxy connection : pool) {
				closeConnection(connection.get());
			}
			
			pool.clear();
		}
	}

	@Override
	@Deprecated
	public Connection getConnection() throws SQLException {
		throw new NotImplementedException("Not Implemented");
	}

	@Override
	@Deprecated	
	public ResultSet select(String sql) throws SQLException {
		throw new NotImplementedException("Not Implemented");
	}

	@Override
	@Deprecated	
	public ResultSet selectFirst(String sql) throws SQLException {
		throw new NotImplementedException("Not Implemented");
	}
}
