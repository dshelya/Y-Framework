package app.context.services.impl;

import java.awt.Desktop;
import java.io.IOException;
import java.net.URI;
import java.util.Objects;
import java.util.function.Consumer;

import app.context.services.GenericService;
import app.context.services.interfaces.Context;
import app.context.services.interfaces.Context.WorkingMode;
import app.context.services.interfaces.GuiAccessService;
import app.context.services.interfaces.SchedulerService;
import app.context.services.interfaces.Task;
import app.module.api.ExternalProperty;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Bounds;
import javafx.geometry.Insets;
import javafx.geometry.Point2D;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.input.Clipboard;
import javafx.scene.input.ClipboardContent;
import javafx.scene.input.KeyCombination;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Priority;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.stage.Window;

final class SystemGuiService extends GenericService implements GuiAccessService {	
	private ExternalProperty<Stage> mainWindow = new ExternalProperty<>();
	private SchedulerService scheduler;
	private double defaultWidth;
	private double defaultHeight;
	
	protected SystemGuiService(Context apiContext) {
		super(apiContext);	
		scheduler = context.getScheduler();
	}	

	@Override
	public void attachTo(Stage window) {
		mainWindow.setValue(window);
		
		defaultWidth = window.getWidth();
		defaultHeight = window.getHeight();
	}
	
	@Override
	public double getDefaultWidth() {
		return defaultWidth;
	}

	@Override
	public double getDefaultHeight() {
		return defaultHeight;
	}

	@Override
	public void assignWorkingModeSwitchHotkey(KeyCombination workingModeKotkey) {
		if (mainWindow.get() == null)
			return;
		
		mainWindow.get().addEventHandler(KeyEvent.KEY_RELEASED, (E) -> {
			if (workingModeKotkey.match(E)) {
				buildSwitchWorkingModeModal();				
			}
		});
	}

	private void buildSwitchWorkingModeModal() {
		BorderPane switcher = new BorderPane();
		switcher.getStyleClass().add("mode-modal");
		switcher.setPrefWidth(200);
		switcher.setPrefHeight(100);
		switcher.getStylesheets().addAll(mainWindow.get().getScene().getStylesheets());
		
		Label text = new Label("Select working mode:");
		text.setPrefWidth(200);
		text.getStyleClass().add("mode-modal-label");
		
		ChoiceBox<WorkingMode> modes = new ChoiceBox<>();
		modes.setItems(FXCollections.observableArrayList(WorkingMode.values()));
		switcher.setTop(text);
		switcher.setCenter(modes);
		
		Stage modal = context.getGui().showModal(switcher);
		
		modes.getSelectionModel().selectedItemProperty().addListener((I,W,MODE) -> {
			if (MODE != null) {
				context.setWorkingMode(MODE);
				modal.close();						
				context.getGui().showAlert("Working mode has been set to:\n"+context.workingMode());
			}
		});
	}

	@Override
	public Stage showModal(Pane root) {
		Objects.requireNonNull(mainWindow.get(), "Please bind the main window first");
		
		Stage modal = new Stage();

		modal.initModality(Modality.NONE);
		modal.initOwner(null);
		modal.initStyle(StageStyle.TRANSPARENT);
		
		Rectangle rect = new Rectangle(root.getPrefWidth(), root.getPrefHeight());
		rect.setArcHeight(20.0);
		rect.setArcWidth(20.0);
		rect.setId("clip");
		root.setClip(rect);
		
		Scene scene = new Scene(root);
		scene.setFill(Color.TRANSPARENT);
       
		modal.setScene(scene);
		modal.centerOnScreen();
        modal.show();	
        
        return modal;
	}

	@Override
	public void showModalAlert(String message) {
		scheduler.runInMainThread(() -> {
			Stage popup = new Stage();
			popup.initOwner(mainWindow.get());
			popup.initStyle(StageStyle.UNIFIED);
			popup.initModality(Modality.WINDOW_MODAL);

			StackPane content = new StackPane();
			content.getChildren().setAll(new Label(message));
			content.paddingProperty().setValue(new Insets(20, 20, 20, 20));
			popup.setScene(new Scene(content));
			popup.showAndWait();
		});
	}
	
	@Override
	public void showAlert(String message) {
		showMessage(context.applicationName(), message, AlertType.INFORMATION);
	}
	
	@Override
	public Consumer<Double> showProgress(String label, Node anchor, int offsetX, int offsetY) {
		final HBox pane = new HBox(); 
		final Label info = new Label("\t"+label);
		final ProgressIndicator progress = new ProgressIndicator();
		
		progress.setPrefSize(25, 25);
		HBox.setHgrow(progress, Priority.ALWAYS);  

		info.setPrefHeight(40);
		info.setCenterShape(true);
		
		pane.setPrefWidth(-1);
		pane.setMinHeight(40);
		pane.setPrefSize(200, 45);

		pane.setFillHeight(true);
		pane.setSpacing(10);
		pane.getStyleClass().add("load-progress");
		
		pane.getChildren().addAll(info, progress);		
		Stage popup = showModal(pane);		

		popup.setAlwaysOnTop(true);
 		
		if (anchor != null) {
			Window window = popup.getScene().getWindow();
 			window.setX(mainWindow.get().getX() + anchor.getBoundsInParent().getMaxX() + offsetX);
			window.setY(mainWindow.get().getY() + anchor.getBoundsInParent().getMaxY() + offsetY);
		} else {
			popup.setX(mainWindow.get().getX() + mainWindow.get().getWidth() / 2 - popup.getWidth() / 2);
			popup.setY(mainWindow.get().getY() + mainWindow.get().getHeight() / 2 - popup.getHeight() / 2);
		}
		
		SchedulerService scheduler = context.getScheduler();
  	 		
	 	return (PROGRESS) -> {	 	
	 		if (PROGRESS == null) {
				scheduler.runInMainThread(popup::close);
				return;
	 		}
	 		
 			if (PROGRESS < 1) {
 				scheduler.runInMainThread(() -> progress.setProgress(PROGRESS));
 				return;
 			}	 		 
			
			scheduler.runInMainThread(() -> progress.setProgress(1));
			scheduler.sleep(1000);
			scheduler.runInMainThread(popup::close);
	 	};
	}
	
	@Override
	public Consumer<Double> showProgress(String label) {
		return showProgress(label, null, 0, 0);
	}

	@Override
	public double screenX() {
		Objects.requireNonNull(mainWindow, "Please bind the main window first");
		return mainWindow.get().getX() + 8;
	}

	@Override
	public double screenY() {
		Objects.requireNonNull(mainWindow, "Please bind the main window first");
		return mainWindow.get().getY() + 30;
	}

	@Override
	public void addNode(Node node) {
		Objects.requireNonNull(mainWindow, "Please bind the main window first");
		
		Task addNode = () -> {
			if (node.getId().isEmpty())		
				throw new IllegalArgumentException("Please set node ID");
		
			ObservableList<Node> nodes = ((Pane) mainWindow.get().getScene().getRoot()).getChildren();
	
			if (nodes.stream().map(Node::getId).noneMatch(node.getId()::equals)) { 
				nodes.add(node);
			}
		};	
		
		if (Platform.isFxApplicationThread())
			scheduler.runSafe(addNode::run);
		else
			scheduler.runInMainThread(addNode);
	}
	
	@Override
	public void removeNode(Node node) {
		Objects.requireNonNull(mainWindow, "Please bind the main window first");
		scheduler.runInMainThread(() -> {
			if (node.getId().isEmpty())
				throw new IllegalArgumentException("Please set node ID");
			
			ObservableList<Node> nodes = ((Pane) mainWindow.get().getScene().getRoot()).getChildren();			
			nodes.removeIf(node.getId()::equals);
		});
	}

	@Override
	public ExternalProperty<Stage> mainWindow() {
		return mainWindow;
	}

	@Override
	public void shutdown() {		
		scheduler.cancelTasks();
	}

	@Override
	public void browse(URI url) {
		try {
			Desktop.getDesktop().browse(url);
		} catch (IOException e) {
			getLogger().debug(e);
		}
	}

	@Override
	public void focus() {		
		scheduler.runInMainThread(() -> {
			mainWindow().get().setAlwaysOnTop(true);

			mainWindow().get().requestFocus();
			mainWindow().get().centerOnScreen();
			mainWindow().get().toFront();
			
			mainWindow().get().setAlwaysOnTop(false);

		});
	}
	
	@Override
	public void copyToClipboard(String data) {
		final Clipboard clipboard = Clipboard.getSystemClipboard();
		final ClipboardContent content = new ClipboardContent();			
		
		content.putString(data);
		clipboard.setContent(content);
	}

	@Override
	public void showMessage(String header, String message, AlertType type) {
		scheduler.runInMainThread(() -> {
		Alert alert = new Alert(type);
			alert.setTitle(header);
			alert.setHeaderText("");
			alert.setContentText(message);
			alert.showAndWait();
		});
	}

	@Override
	public Point2D getAnchoredPoint(Node anchor) {
		Bounds bounds = anchor.localToScene(anchor.getBoundsInLocal());

		double x = mainWindow.get().getX() + bounds.getMinX();
		double y = mainWindow.get().getY() + bounds.getMaxY();
		
		return new Point2D(x, y);
	}
}
