package app.context.services.impl;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.nio.channels.WritableByteChannel;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

import app.context.services.GenericService;
import app.context.services.interfaces.ConfigService;
import app.context.services.interfaces.Context;
import app.module.api.ExternalProperty;
import app.module.api.GenericModule;

final class SystemConfigService extends GenericService implements ConfigService {
	private Map<String, String> arguments;
	private String workDir;

	protected SystemConfigService(Context context, String name) {
		super(context);
		setWorkDirName(name);
		
		arguments = new HashMap<>();
	}
	
	@Override
	public <T> void bridgeSettings(ExternalProperty<T> slaveProperty, String moduleName, String masterProperty) {
		Optional<GenericModule> module = context.getModule(moduleName);

		if (module.isPresent()) {
			Optional<ExternalProperty<T>> property = module.get().getProperty(masterProperty);

			property.ifPresent(P -> {
				slaveProperty.bind(P);
				slaveProperty.setBridged(true);
			});
		}
	}
	
	@Override
	public void setArguments(Map<String, String> arguments) {
		this.arguments.putAll(arguments);
	}
	
	@Override
	public Map<String, String> arguments() {
		return arguments;
	}

	@Override
	public void setWorkDirName(String name) {
		this.workDir = BASE_DIR + "/" + name;
		new File(workDir).mkdirs();
	}	
	
	@Override
	public void ensureDefault(String resourse) {
		File path = getPathFor(resourse);
		
		if (path.exists())
			return;		
		
		try (
			final InputStream in = getClass().getClassLoader().getResourceAsStream(resourse);
			final OutputStream out = new FileOutputStream(path);

			final ReadableByteChannel inChannel = Channels.newChannel(in);
			final WritableByteChannel outChannel = Channels.newChannel(out);
		) {
			path.createNewFile();

			final ByteBuffer buffer = ByteBuffer.allocateDirect(16 * 1024);
			while (inChannel.read(buffer) != -1) {
				buffer.flip();
				outChannel.write(buffer);
				buffer.compact();
			}

			buffer.flip();
			while (buffer.hasRemaining())
				outChannel.write(buffer);

			in.close();
			out.close();
			inChannel.close();
			outChannel.close();

		} catch (Throwable e) {
			path.delete();
			throw new RuntimeException(e);
		}
	}

	@Override
	public File getPathFor(String fileName) {
		Objects.requireNonNull(workDir, "Please set path for application working directory");
		
		File path = new File (workDir + "/" + fileName);
		path.getParentFile().mkdirs();
		
		return path;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public <T> T deserializeFromFile(String fileName) throws Exception {
		File path = getPathFor(fileName);

		ObjectInputStream stream = new ObjectInputStream(new FileInputStream(path));
		T object = (T) stream.readObject();
		
		stream.close();
		return object;
	}
	
	@Override
	public <T> void serializeToFile(String file, T object) throws Exception {
		File path = getPathFor(file);

		ObjectOutputStream stream = new ObjectOutputStream(new FileOutputStream(path));
		stream.writeObject(object);
		stream.close();
	}

	@Override
	public void shutdown() {
		// TODO Auto-generated method stub
		
	}
}
