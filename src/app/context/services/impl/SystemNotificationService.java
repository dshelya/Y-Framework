package app.context.services.impl;

import java.util.Arrays;
import java.util.Objects;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import app.context.services.GenericService;
import app.context.services.interfaces.Context;
import app.context.services.interfaces.NotificationService;

public class SystemNotificationService extends GenericService implements NotificationService {
	private final static Properties PROPERTIES;

	static {
		PROPERTIES = new Properties();
		PROPERTIES.put("mail.smtp.auth", "true");
		PROPERTIES.put("mail.smtp.starttls.enable", "true");
		PROPERTIES.put("mail.smtp.host", "smtp.gmail.com");
		PROPERTIES.put("mail.smtp.port", "587");		
	}
	
	protected SystemNotificationService(Context apiContext) {
		super(apiContext);	
	}	
	
	@Override
	public void sendEmail(String subject, String body, String from, String fromPass, String to) {
		Session session = Session.getInstance(PROPERTIES, new javax.mail.Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(from, fromPass);
			}
		});

		try {
			Message message = new MimeMessage(session);
			
			InternetAddress[] recepients = Arrays.asList(to.split("[;,]")).stream().map(A -> {
				try {
					return new InternetAddress(A);
				} catch (AddressException e) {
					return null;
				}
			}).filter(Objects::nonNull).toArray(InternetAddress[]::new);

			message.setFrom(new InternetAddress(from));
			message.setRecipients(Message.RecipientType.TO, recepients);
			message.setSubject(subject);
			message.setContent(body, "text/html; charset=utf-8");

			Transport.send(message);
		} catch (MessagingException e) {
			getLogger().info("Failed to send email [%s] due to: %s", subject, getLogger().getRootCause(e).getMessage());
		}
	}

	
	@Override
	public void shutdown() {		
	}
}
