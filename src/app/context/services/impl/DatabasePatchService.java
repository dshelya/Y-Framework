package app.context.services.impl;

import java.io.File;
import java.nio.file.Files;
import java.sql.Connection;
import java.sql.Statement;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

import app.context.services.GenericService;
import app.context.services.interfaces.Context;
import app.context.services.interfaces.LoggerService;
import javafx.scene.control.Alert.AlertType;

public class DatabasePatchService extends GenericService {
	private LoggerService logger = getLogger().tag(getClass());
	private Map<String, File> availablePatches;

	protected DatabasePatchService(Context context) {
		super(context);

		initAvailbalePatches();
 	}

	private void initAvailbalePatches() {
		availablePatches = new HashMap<>();
		
		try {			
			File patchFile = new File("patches");
			
			if (patchFile.isDirectory()) {
				Arrays.stream(
					patchFile.listFiles((F) -> F.getName().toLowerCase().endsWith(".sql"))
				).forEach(F -> availablePatches.put(F.getName().replaceAll("\\..*", ""), F));
			} 			
		} catch (Exception e) {
			e.printStackTrace();
			return;
		}
	}

	public void patchIfAvailable() {
		String feature = config.arguments().get("patch");

		if (feature != null) {
			logger.info("Trying to patch database. Looking for feature: %s", feature);

			File patchFile = availablePatches.get(feature);

			if (patchFile != null) {
				logger.info("Patch found. Altering database... Feature [%s]", feature);

				try {
					String statements = Files.lines(patchFile.toPath()).collect(Collectors.joining("\n"));

					try (
						Connection db = database.newConnection().get(); 
						Statement query = db.createStatement();
					) {
						for (String statement : statements.split(";")) {
							query.addBatch(statement);
						}

						query.executeBatch();
						gui.showMessage("Success", "Database has been patched.\nFeature added: " + feature,	AlertType.CONFIRMATION);
					}
				} catch (Exception e) {
					String error = e.getMessage().toLowerCase();
					
					if (error.contains("duplicate") || error.contains("already")) {
						logger.info("The feature already applied. Skipping...");
						return;
					}
					
					logger.info(e);
				}
			} else {
				logger.info("No patch found for feature = %s", feature);
			}
		}
	}

	@Override
	public void shutdown() {

	}

}
