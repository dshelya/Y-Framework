package app.context.services.impl;

import java.awt.Desktop;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.UncheckedIOException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.Objects;
import java.util.function.Supplier;

import app.context.services.StackFrame;
import app.context.services.interfaces.DetachedLoggerService;

public class SystemLogger implements DetachedLoggerService {
	public static enum Mode {
		INFO, DEBUG, WARN, CRITICAL;
	}
	
	private static final SystemLogger SELF = new SystemLogger();
	private static final String LOG_ARCHIVE_DIR = "logs/";
	
	private Mode mode;
	
	private FileOutputStream systemLog;
	private String logdir;
	private TaggedLogger internalLogger;

	private SystemLogger() {
		this.logdir = "";
		this.mode = Mode.INFO;
		this.systemLog = initStreamFor("system.log");
 		this.internalLogger = new TaggedLogger(this::stackFrameTag);
	}
	
	public static DetachedLoggerService log() {
 		return SELF;
	}

	@Override
	public void setMode(Mode mode) {
		this.mode = mode;
	}

	@Override
	public boolean isDebugMode() {
		return mode != Mode.INFO;
	}

	@Override
	public File dumpToFile(String data, String filename) {
		return internalLogger.dumpToFile(data, filename);
	}

	@Override
	public void dumpAndOpenFile(String data, String filename) throws IOException {
		internalLogger.dumpAndOpenFile(data, filename);
	}

	@Override
	public void openFile(String filePath) throws IOException {
		internalLogger.openFile(filePath);
	}
	
	public static void main(String[] args) {
		SystemLogger.log().setMode(Mode.DEBUG);
		SystemLogger.log().debug("LOL");
	}
	
	private String stackFrameTag() {		
		return StackFrame.getCallChain().stream()
			.skip(6)
			.filter(F -> !"debug".equals(F.getCallerMethod()) && !F.getClass().equals(this.getClass()))
			.findFirst()
			.map(S -> S.getFrameClass().getName().replace("$", "."))
			.orElse("");
	}

	private void backupLog(String name) {
		File file = getPathFor(name);

		if (file.exists()) {
			String date = new SimpleDateFormat("yyyy-MM-dd'_'HH'_'").format(new Date(file.lastModified()));
			File archiveLog = getPathFor(LOG_ARCHIVE_DIR + date + name);

			file.renameTo(archiveLog);
		}
	}
	
	public void setLogDir(String logdir) {
		this.logdir = logdir;
	}

	private File getPathFor(String filename) {
		return new File(logdir + filename);
	}

	private FileOutputStream initStreamFor(String name) {
		File file = getPathFor(name);

		try {
			backupLog(name);

			if (!file.exists())
				file.createNewFile();

			return new FileOutputStream(file);
		} catch (IOException e) {
			throw new UncheckedIOException(e);
		}
	}
	
	@Override
	public void log(Mode mode, Throwable error) {
		internalLogger.log(mode, error);
	}

	@Override
	public void log(Mode mode, String messagef, Object ... holders) {
		internalLogger.log(mode, messagef, holders);
	}

	@Override
	public void warn(String messagef, Object ... holders) {
		internalLogger.warn(messagef, holders);
	}

	@Override
	public void critial(String messagef, Object ... holders) {
		internalLogger.critial(messagef, holders);
	}

	@Override
	public void warn(Throwable e) {
		internalLogger.warn(e);		
	}

	@Override
	public void critial(Throwable e) {
		internalLogger.critial(e);		
	}

	@Override
	public void debug(String messagef, Object... holders) {
		internalLogger.debug(messagef, holders);
	}

	@Override
	public void info(String messagef, Object... holders) {
		internalLogger.info(messagef, holders);
	}

	@Override
	public void info(Throwable e) {
		internalLogger.info(e);
	}

	@Override
	public void debug(Throwable e) {
		internalLogger.debug(e);
	}

	@Override
 	public <T extends Throwable> T getRootCause(Throwable exception) {
		return internalLogger.getRootCause(exception);
	}

	@Override
 	public <T extends Throwable> T getSpecificCause(Throwable exception, Class<? extends Throwable> relation) {
		return internalLogger.getSpecificCause(exception, relation);
	}

	@Override
	public boolean isRelated(Throwable exception, Class<? extends Throwable> relation) {
		return internalLogger.isRelated(exception, relation);
	}

	@Override
	public DetachedLoggerService tag(Class<?> taggedClass) {
		return new TaggedLogger(() -> taggedClass.getSimpleName());
	}

	@Override
	public DetachedLoggerService tag(String tag) {
		return new TaggedLogger(() -> tag);
	}

	private class TaggedLogger implements DetachedLoggerService {
		private Supplier<String> tag;

		public TaggedLogger(Supplier<String> tag) {
			this.tag = tag;
		}

		@Override
		public void dumpAndOpenFile(String data, String filename) throws IOException {
			Desktop.getDesktop().browse(dumpToFile(data, filename).toURI());
		}

		@Override
		public File dumpToFile(String data, String filename) {
			File dump = getPathFor(filename);
			
			try (FileOutputStream stream = new FileOutputStream(dump)) { 
				stream.write(Objects.toString(data, "").getBytes("UTF-8"));
	 		} catch (IOException e) {
				throw new RuntimeException(e);
			}

			return dump;
		}

		@Override
		public void openFile(String filePath) throws IOException {
			Desktop.getDesktop().browse(new File(filePath).toURI());
		}

		@Override
		public synchronized void log(Mode mode, String messagef, Object... holders) {			
			String time = FORMATTED_DATE.format(LocalDateTime.now());
			messagef = holders.length == 0 ? messagef.replace("%", "%%") : messagef;

			messagef = "[" + mode + "] " + tag.get() + ": " + messagef;
			String message = String.format(time + ": " + messagef+"\n", holders);
			
			try {
				systemLog.write(message.getBytes());
			} catch (IOException e) {
				throw new UncheckedIOException(e);
			}	
			
			System.out.print(message);
		}

		@Override
		public void log(Mode mode, Throwable error) {
			StringWriter errors = new StringWriter();
			error.printStackTrace(new PrintWriter(errors));

			log(mode, errors.toString());
		}

		@Override
		public DetachedLoggerService tag(Class<?> taggedClass) {
			this.tag = () -> taggedClass.getSimpleName();
			return this;
		}

		@Override
		public DetachedLoggerService tag(String tag) {
			this.tag = () -> tag;
			return this;
		}

		@Override
		@SuppressWarnings("unchecked")
		public <T extends Throwable> T getRootCause(Throwable exception) {
			return (exception.getCause() == null) ? (T) exception : getRootCause(exception.getCause());
		}

		@Override
		@SuppressWarnings("unchecked")	
		public <T extends Throwable> T getSpecificCause(Throwable exception, Class<? extends Throwable> relation) {			
			if (relation.isInstance(exception))
				return (T) exception;

			return (exception.getCause() == null) ? null : getSpecificCause(exception.getCause(), relation);
		}

		@Override
 		public boolean isRelated(Throwable exception, Class<? extends Throwable> relation) {
			if (relation.isInstance(exception))
				return true;

			return (exception.getCause() == null) ? false : isRelated(exception.getCause(), relation);
		}

		@Override
		public void setMode(Mode mode) {
			SystemLogger.this.setMode(mode);
		}

		@Override
		public boolean isDebugMode() {
			return SystemLogger.this.isDebugMode();
		}

		@Override
		public void debug(String messagef, Object ... holders) {
			log(Mode.DEBUG, messagef, holders);
		}

		@Override
		public void info(String messagef, Object ... holders) {
			log(Mode.INFO, messagef, holders);
		}

		@Override
		public void warn(String messagef, Object ... holders) {
			log(Mode.WARN, messagef, holders);			
		}

		@Override
		public void critial(String messagef, Object ... holders) {
			log(Mode.CRITICAL, messagef, holders);
		}

		@Override
		public void debug(Throwable e) {
			log(Mode.DEBUG, e);
		}

		@Override
		public void warn(Throwable e) {
			log(Mode.WARN, e);
		}

		@Override
		public void critial(Throwable e) {
			log(Mode.CRITICAL, e);
		}

		@Override
		public void info(Throwable e) {
			log(Mode.INFO, e);
		}
	} 
}
