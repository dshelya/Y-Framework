package app.context.services.impl;

import java.awt.Desktop;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.UncheckedIOException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.Objects;
import java.util.concurrent.CancellationException;
import java.util.logging.Level;
import java.util.logging.Logger;

import app.context.services.GenericService;
import app.context.services.SystemRuntimeException;
import app.context.services.interfaces.Context;
import app.context.services.interfaces.LoggerService;

final class SystemLoggerService extends GenericService implements LoggerService {	
	static {
		Logger.getLogger("com.gargoylesoftware").setLevel(Level.OFF);
	}

	private static final String LOG_ARCHIVE_DIR = "logs/";
	
	private boolean debug;
	private FileOutputStream systemLog;

	protected SystemLoggerService(Context context) {
		super(context);
		this.systemLog = initStreamFor("system.log");
	}

	@Override
	public void setDebugMode(boolean debug) {
		this.debug = debug;
	}
	
	@Override
	public boolean isDebugMode() {
		return debug;
	}
	
	@Override	
	public File dumpToFile(String data, String filename) {		
		File dump = config.getPathFor(filename);
		try {
			dump.createNewFile();

			FileOutputStream stream = new FileOutputStream(dump);
			stream.write(Objects.toString(data, "").getBytes("UTF-8"));
			stream.close();
		} catch (IOException e) {
			throw new SystemRuntimeException(e);
		}
		
		return dump;
	}
	
	@Override	
	public void dumpAndOpenFile(String data, String filename) throws IOException {
		Desktop.getDesktop().browse(dumpToFile(data, filename).toURI());
	}
	
	@Override
	public void openFile(String filePath) throws IOException {
		Desktop.getDesktop().browse(new File(filePath).toURI());		
	}
	
	private void backupLog(String name) {
		File file = config.getPathFor(name);

		if (file.exists()) {
			String date = new SimpleDateFormat("yyyy-MM-dd'_'HH'_'").format(new Date(file.lastModified()));
 			File archiveLog = config.getPathFor(LOG_ARCHIVE_DIR + date + name);
 			
			file.renameTo(archiveLog);
		}
	}
	
	private FileOutputStream initStreamFor(String name) {
		File file = config.getPathFor(name);

		try {
			backupLog(name);
			
			if (!file.exists())
				file.createNewFile();		
			
			return new FileOutputStream(file);
		} catch (IOException e) {
			throw new UncheckedIOException(e);
		}
	}
	
	@Override
	public synchronized void debug(String messagef, Object ... holders) {
		if (debug)
			info("[DEBUG] "+messagef, holders);
	}
	
	@Override
	public synchronized void info(String messagef, Object ... holders) {
		String time = FORMATTED_DATE.format(LocalDateTime.now());
		messagef = holders.length == 0 ? messagef.replace("%", "%%") : messagef;

		String message = String.format(time + ": " + messagef+"\n", holders);
		
		try {
			this.systemLog.write(message.getBytes());
		} catch (IOException e) {
			throw new UncheckedIOException(e);
		}	
		
		System.out.print(message);
	}
	
	@Override
	public synchronized void info(Throwable e) {
		StringWriter errors = new StringWriter();
		e.printStackTrace(new PrintWriter(errors));
		
		info(errors.toString());
	}
	
	@Override
	public void debug(Throwable e) {
		if (debug)
			info(e);		
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public <T extends Throwable> T getRootCause(Throwable exception) {
		return (exception.getCause() == null) ? (T) exception : getRootCause(exception.getCause());
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public <T extends Throwable> T getSpecificCause(Throwable exception, Class<? extends Throwable> relation) {
		if (relation.isInstance(exception))
			return (T) exception;
		
		return (exception.getCause() == null) ? null : getSpecificCause(exception.getCause(), relation);
	}
	
	@Override
	public boolean isRelated(Throwable exception, Class<? extends Throwable> relation) {
		if (relation.isInstance(exception))
			return true;
		
		return (exception.getCause() == null) ? false : isRelated(exception.getCause(), relation);
	}	

	@Override
	public void shutdown() {		
	}

	@Override
	public LoggerService tag(Class<?> taggedClass) {
		return new TaggedLogger(taggedClass);
	}
	
	@Override
	public LoggerService tag(String tag) {
		return new TaggedLogger(tag);
	}	
	
	private class TaggedLogger  implements LoggerService {
		private String tag;

		public TaggedLogger(Class<?> taggedClass) {
			this.tag = taggedClass.getSimpleName();
		}
		
		public TaggedLogger(String tag) {
			this.tag = tag;
		}

		@Override
		public void dumpAndOpenFile(String data, String filename) throws IOException {
			SystemLoggerService.this.dumpAndOpenFile(data, filename);			
		}
		
		@Override
		public File dumpToFile(String data, String filename) {
			return SystemLoggerService.this.dumpToFile(data, filename);
		}

		@Override
		public void openFile(String filePath) throws IOException {
			SystemLoggerService.this.openFile(filePath);			
		}

		@Override
		public void debug(String messagef, Object... holders) {
			if (debug)
				SystemLoggerService.this.info("[DEBUG] " + tag + " -> " + messagef, holders);
		}

		@Override
		public void info(String messagef, Object... holders) {
			SystemLoggerService.this.info(tag + " -> " + messagef, holders);						
		}

		@Override
		public void debug(Throwable e) {
			if (debug)
				info(e);	
		}

		@Override
		public void info(Throwable e) {
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			
			info(errors.toString());				
		}

		@Override
		public LoggerService tag(Class<?> taggedClass) {
			return this;
		}
		
		@Override
		public LoggerService tag(String tag) {
			return this;
		}	

		@Override
		public <T extends Throwable> T getRootCause(Throwable exception) {
			return SystemLoggerService.this.getRootCause(exception);
		}			

		@Override
		public <T extends Throwable> T getSpecificCause(Throwable exception, Class<? extends Throwable> relation) {
			return SystemLoggerService.this.getSpecificCause(exception, relation);
		}

		@Override
		public boolean isRelated(Throwable exception, Class<? extends Throwable> relation) {
			return SystemLoggerService.this.isRelated(exception, relation);
		}	

		@Override
		public void setDebugMode(boolean isDebug) {
			SystemLoggerService.this.setDebugMode(isDebug);
		}

		@Override
		public boolean isDebugMode() {
			return SystemLoggerService.this.isDebugMode();
		}

		@Override
		public boolean isUnrecoverable(Exception e) {
 			return SystemLoggerService.this.isUnrecoverable(e);
		}	
	}

	@Override
	public boolean isUnrecoverable(Exception e) {
		return isRelated(e, InterruptedException.class) || isRelated(e, CancellationException.class);
	}
}
