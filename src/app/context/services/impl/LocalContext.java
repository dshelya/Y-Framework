package app.context.services.impl;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

import app.context.services.interfaces.ConfigService;
import app.context.services.interfaces.Context;
import app.context.services.interfaces.DatabaseService;
import app.context.services.interfaces.GuiAccessService;
import app.context.services.interfaces.LoggerService;
import app.context.services.interfaces.NotificationService;
import app.context.services.interfaces.SchedulerService;
import app.context.services.interfaces.WebViewService;
import app.context.services.interfaces.WebViewer;
import app.module.api.GenericModule;

public class LocalContext implements Context {
	private static LocalContext instance;

	private SystemDatabaseService database;
	private SystemLoggerService logger;
	private SystemGuiService guiAccessor;
	private SystemConfigService config;
	private WebViewer webView;
	private WebViewService defaultViewer;
	private DatabasePatchService patcher;

	private SystemNotificationService notificator;
	private SystemScheduler scheduler;

	private String applicationName;
	private String version;

	private Map<String, GenericModule> activeModules;
	private WorkingMode workingMode;

	
	private LocalContext(String applicationName, boolean console, Map<String, String> arguments, int threadPoolSize) {	
		this.applicationName = applicationName;
		
		this.config = new SystemConfigService(this, applicationName);		
		config.setArguments(arguments);
		
		this.logger = new SystemLoggerService(this);
		this.scheduler = new SystemScheduler(this, threadPoolSize);
		this.database = new SystemDatabaseService(this);

		this.guiAccessor = new SystemGuiService(this);
		this.notificator = new SystemNotificationService(this);
		
		this.patcher = new DatabasePatchService(this);

		if (!console) {
			this.webView = new SystemWebView(this);	
			this.defaultViewer = webView.getWebViewService(scheduler.getSchedulerService());			
		} 
		
		this.activeModules = new HashMap<>();	
		setWorkingMode(WorkingMode.LIVE);

		logger.debug("Application context initialized!");
	}
	
	public static LocalContext getInstance(String appName, Map<String, String> arguments) {
		return getInstance(appName, false, arguments, SystemScheduler.DEFAULT_THREAD_POOL_SIZE);
	}
	
	public static LocalContext getInstance(String appName) {
		return getInstance(appName, false, Collections.emptyMap(), SystemScheduler.DEFAULT_THREAD_POOL_SIZE);
	}
	
	public static LocalContext getInstance(boolean console) {
		return getInstance("", console, Collections.emptyMap(), SystemScheduler.DEFAULT_THREAD_POOL_SIZE);
	}
	
	public static LocalContext getInstance(String appName, boolean console) {
		return getInstance(appName, console, Collections.emptyMap(), SystemScheduler.DEFAULT_THREAD_POOL_SIZE);
	}
	
	public static LocalContext getInstance(String appName, boolean console, int threadPoolSize) {
		return getInstance(appName, console, Collections.emptyMap(), threadPoolSize);
	}
	
	public static LocalContext getInstance(String appName, boolean console, Map<String, String> arguments, int threadPoolSize) {
		if (instance == null)
			return instance = new LocalContext(appName, console, arguments, threadPoolSize);
		
		return instance;
	}
	

	public static LocalContext getInstance() {	
		Objects.requireNonNull(instance, "Context was not properly initialized");
		return instance;
	}	
	
	@Override
	public String buildNumber() {
 		return String.valueOf(activeModules.values().stream().mapToInt(M -> M.version()).max().orElse(0));
	}
		
	@Override
	public DatabaseService getDatabase() {
		return database;
	}
	
	@Override
	public DatabasePatchService getPatchService() {
		return patcher;
	}

	@Override
	public ConfigService getConfig() {
		return config;
	}

	@Override
	public GuiAccessService getGui() {
		return guiAccessor;
	}

	@Override
	public LoggerService getLogger() {
		return logger;
	}

	@Override
	public WebViewer getWebViewer() {
		return webView;
	}
	
	@Override
	public NotificationService getNotifier() {
		return notificator;
	}	

	@Override
	public SchedulerService getScheduler() {
		return scheduler.getSchedulerService();
	}

	@Override
	public void shutdown() {
		database.shutdown();
		logger.shutdown();
		guiAccessor.shutdown();
		config.shutdown();
		notificator.shutdown();
		scheduler.shutdown();
		
	//	if (webView !=null)
	//		webView.shutdown();		
	}

	@Override
	public void setApplicationName(String applicationName) {
		this.applicationName = applicationName;
	}

	@Override
	public String applicationName() {
		return applicationName;
	}

	@Override
	public void setVersion(String version) {
		this.version = version;
	}

	@Override
	public String version() {
		return version;
	}

	@Override
	public void registerModule(String name, GenericModule module) {
		if (activeModules.containsKey(name)) {
			//throw new ModuleRuntimeException("A module with name ["+name+"] is already registered");
			return;
		}
		
		this.activeModules.put(name, module);
	}

	@Override
	@SuppressWarnings("unchecked")
	public <M extends GenericModule> Optional<M> getModule(String name) {
		return Optional.<M>ofNullable((M) activeModules.get(name));
	}

	@Override
	public void setWorkingMode(WorkingMode mode) {
		this.workingMode = mode;
		logger.info("Working mode is [%s]", mode);
	}

	@Override
	public WorkingMode workingMode() {
		return workingMode;
	}

	@Override
	public WebViewService getDefaultWebViewer() {
 		return defaultViewer;
	}
}
