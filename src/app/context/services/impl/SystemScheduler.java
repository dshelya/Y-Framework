package app.context.services.impl;

import java.math.BigInteger;
import java.nio.channels.ClosedByInterruptException;
import java.security.SecureRandom;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.FutureTask;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.function.Function;

import app.context.services.ExceptionPolicy;
import app.context.services.GenericService;
import app.context.services.SystemRuntimeException;
import app.context.services.interfaces.Context;
import app.context.services.interfaces.ReturnTask;
import app.context.services.interfaces.Scheduler;
import app.context.services.interfaces.SchedulerService;
import app.context.services.interfaces.Task;
import javafx.application.Platform;

public class SystemScheduler extends GenericService implements Scheduler { 	
	public static final int DEFAULT_THREAD_POOL_SIZE = 16;

	private final ExecutorService executor;
	private final ScheduledExecutorService scheduler;

	public SystemScheduler(Context context, int threadPoolSize) {
		super(context);
		
		executor = Executors.newFixedThreadPool(threadPoolSize);
		scheduler = Executors.newScheduledThreadPool(threadPoolSize);
	}

	@Override
	public SchedulerService getSchedulerService() {
		return new GroupScheduler();
	}	

	@Override
	public void shutdown() {
		executor.shutdownNow();
		scheduler.shutdownNow();

		try {
			executor.awaitTermination(0, TimeUnit.MICROSECONDS);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		try {
			scheduler.awaitTermination(0, TimeUnit.MICROSECONDS);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
		
	private class GroupScheduler implements SchedulerService {
		SecureRandom random = new SecureRandom();
 		Map<String, Future<?>> futures = Collections.synchronizedMap(new HashMap<>());

 		String getTaskID() {
			return new BigInteger(100, random).toString(32);
		}
		
		void saveFuture(String id, Future<?> future) {
			purgeTasks();			
			futures.put(id, future);
 		}

		@Override
		public void cancelTasks() {
			futures.forEach((M,F) -> {
			    F.cancel(true);
			});
			
			futures.clear();
			purgeTasks();
		}
		
		@Override
		public void purgeTasks() {			
			futures.entrySet().removeIf(E -> E.getValue().isDone() || E.getValue().isCancelled());		
 		}

		@Override
		public void sleep(long millis) {
			int priority = Thread.currentThread().getPriority();

			try {
				Thread.currentThread().setPriority(Thread.MIN_PRIORITY);
 				Thread.sleep(millis);
 				Thread.currentThread().setPriority(priority);
			} catch (Throwable e) { 
 				Thread.currentThread().setPriority(priority);
				getLogger().info("Process was killed by the system");	
				
				handleError(new InterruptedException("Thread "+Thread.currentThread().getName()+" was killed by the system"));
			}
		}

		@Override
		public void runSafe(Task task) {
			wrapUnsafe(task).run();
		}

		@Override
		public void backgroundRun(Task task) {
			saveFuture(getTaskID(), executor.submit(wrapUnsafe(task)));			
		}

		@Override
		public <T> T backgroundRun(ReturnTask<T> task) {
			FutureTask<T> future = wrapUnsafe(task);
			
			saveFuture(getTaskID(), future);
			saveFuture(getTaskID(), executor.submit(future));
			
			try {
				return future.get();
			} catch (Exception e) {
				throw new RuntimeException(e);
			}
		}
		
		@Override
		public void runConstrained(Task task, long timeout, TimeUnit timeoutUnit) {
 			Future<?> future = executor.submit(wrapUnsafe(task)); 			
			saveFuture(getTaskID(), future);	
			
 			try {
				future.get(timeout, timeoutUnit);
			} catch (TimeoutException e) {
				future.cancel(true);
				throw new SystemRuntimeException("Constrained task is timed out"); 
			} catch (Exception e) {
				future.cancel(true);
				throw new RuntimeException(e);
			}
		}


		@Override
		public ScheduledFuture<?> backgroundRunReccurent(Task task, long amount, TimeUnit unit) {
			if (amount == 0)
				return null;
			
			ScheduledFuture<?> future = scheduler.scheduleWithFixedDelay(wrapUnsafe(task), 0, amount, unit);
			saveFuture(getTaskID(), future);
			
			return future;	
		}

		@Override
		public void runInMainThread(Task task) {
			Platform.runLater(wrapUnsafe(task));
		}

		@Override
		public <T> T runInMainThread(ReturnTask<T> task) {
			FutureTask<T> future = wrapUnsafe(task);	
			saveFuture(getTaskID(), future);
					
			Platform.runLater(future);
			
			try {
				return future.get();
			} catch (Exception e) {
				throw new RuntimeException(e);
			}
		}
		
		@Override
		public void runUntilSuccess(Task task, int times) {
			runUntilSuccess(() -> {
				task.run();
				return null;
			}, times);
		}
		
		@Override
		public <T> T runUntilSuccess(ReturnTask<T> task, int times) {
			return runUntilSuccess(task, times, (E) -> ExceptionPolicy.IGNORE);
		}
		
		@Override
		public void runUntilSuccess(Task task, int times, Function<Throwable, ExceptionPolicy> exceptionValidator) {
			ReturnTask<?> fakeTask = () -> {
				task.run();
				return null;
			};
			
			runUntilSuccess(fakeTask, times, exceptionValidator);
		}

		@Override
		public <T> T runUntilSuccess(ReturnTask<T> task, int times,	Function<Throwable, ExceptionPolicy> exceptionValidator) {			
			return backgroundRun(() -> {
				int count = 0;

				while (true) {
					try {					
						return task.run(); 	
					} catch (Exception e) {	
						Throwable cause = getLogger().getRootCause(e);
						
						if (cause instanceof InterruptedException || cause instanceof CancellationException) {
							context.getDatabase().interrupt();
							throw new SystemRuntimeException(cause.getMessage());
						}
	 					
						ExceptionPolicy answer = exceptionValidator.apply(cause);
						
						if (answer == ExceptionPolicy.IGNORE)
							continue;
						
						if (answer == ExceptionPolicy.RETURN)
							return null;
						
						if (answer == ExceptionPolicy.THROW && times == 0) {
							throw answer.wrap(e); 
						}
						
						if (times > 0 && count++ >= times - 1) {
 							throw new SystemRuntimeException("Cannot complete task in "+times+" tries. Last execption caught ["+cause.getMessage()+"]");
						}
					}
				}
			});
		}
		
		private <T> FutureTask<T> wrapUnsafe(ReturnTask<T> task) {
			return new FutureTask<>(() -> {
				try {
					return task.run();
				} catch (Throwable e) {
					handleError(e);
					throw new RuntimeException(e);
				}
			});
		}
		
		private Runnable wrapUnsafe(Task task) {
			return () -> {
				try {
					task.run();
				} catch (Throwable e) {					
					handleError(e);
				}
			};
		}
		
		private void handleError(Throwable error) {
			Throwable cause = getLogger().getRootCause(error);
			
			if (cause instanceof InterruptedException || cause instanceof CancellationException || cause instanceof ClosedByInterruptException) {
				context.getDatabase().interrupt();
				throw new SystemRuntimeException(new InterruptedException(cause.getMessage()));
			}
			
			getLogger().info("Task error: %s", cause.getMessage());
			getLogger().info(cause);
		}
	}
}
