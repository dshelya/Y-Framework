package app.context.services.db;

@SuppressWarnings("serial")
public class DatabaseRuntimeException extends RuntimeException {

	public DatabaseRuntimeException(String string) {
		super(string);
	}

	public DatabaseRuntimeException(Exception e) {
		super(e);
	}

}
