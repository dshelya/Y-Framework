package app.context.services.db;

import java.util.Collection;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

public class DatabaseMerge<I> extends StatementProxy<I> {
	private static final String TEMPLATE = "MERGE INTO #TABLE#(#COLUMNS#) VALUES(#VALUES#)";

	public DatabaseMerge(Collection<I> collection) {
		super(collection);
	}
	
	public DatabaseMerge(I object) {
		super(object);
	}
	
	public DatabaseMerge<I> nothing() {
		return this;
	}
	
	protected String assembleStatement() {
		Optional<I> possibleEntity = batchMode ? objects.stream().findFirst() : Optional.ofNullable(object);		
		I entity = possibleEntity.orElseThrow(() -> new DatabaseRuntimeException("No entity is bound. Transport aborted"));
		
		Map<String, Object> shards = disassemble(entity);
		
		String columns = shards.keySet()
			.stream()					
			.collect(Collectors.joining(","));
			
		String values = shards.keySet()
			.stream()
			.map(C -> columnValueCoverters.getOrDefault(C, X -> "?").apply(C))
			.collect(Collectors.joining(","));
			
		return TEMPLATE.replace("#TABLE#", table).replace("#COLUMNS#", columns).replace("#VALUES#", values);		
	}	
}