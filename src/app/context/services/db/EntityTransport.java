package app.context.services.db;

import java.util.Collection;

public final class EntityTransport {
	private EntityTransport() {
	};

	public static <I> DatabaseUpdate<I> update(Collection<I> entities) {
		return new DatabaseUpdate<I>(entities);
	}

	public static <I> DatabaseUpdate<I> update(I object) {
		return new DatabaseUpdate<I>(object);
	}

	public static <I extends Entity> DatabaseSelect<I> selectInto(I object) {		
		return new DatabaseSelect<I>(object);
	}
	
	public static <I extends Entity> DatabaseSelect<I> selectInto(Collection<I> entities) {		
		return new DatabaseSelect<I>(entities);
	}
		
	public static <I> DatabaseInsert<I> insert(Collection<I> entities) {
		return new DatabaseInsert<I>(entities);
	}

	public static <I> DatabaseInsert<I> insert(I object) {
		return new DatabaseInsert<I>(object);
	}

	public static <I> DatabaseMerge<I> merge(Collection<I> entities) {
		return new DatabaseMerge<I>(entities);
	}

	public static <I> DatabaseMerge<I> merge(I object) {
		return new DatabaseMerge<I>(object);
	}

	public static <I> DatabaseDelete<I> delete(I object) {
		return new DatabaseDelete<I>(object);
	}
	
	public static <I> DatabaseDelete<I> delete(Collection<I> entities) {
		return new DatabaseDelete<I>(entities);
	}
}
