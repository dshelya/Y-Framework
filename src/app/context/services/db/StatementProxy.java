package app.context.services.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Objects;
import java.util.function.Consumer;
import java.util.function.Function;

import app.context.AppClient;
import app.context.services.ConnectionProxy;
import app.module.api.ExternalProperty;

public abstract class StatementProxy<I> extends ConditionalStatement<I> {
	protected boolean batchMode;
	protected Collection<I> objects;
	protected I object;
	protected Map<String, Function<I,?>> boundColumns = new LinkedHashMap<>();
	protected Map<String, Function<String, String>> columnValueCoverters = new LinkedHashMap<>();

	protected String table;
	private Consumer<Object> saver;
	
	public StatementProxy(I object) {
		this.object = object;
	}

	public StatementProxy(Collection<I> objects) {
		this.objects = objects;
		batchMode = true;
	}
	
	@SuppressWarnings("unchecked")
	public <R extends StatementProxy<I>> R table(String table) {
		this.table = table;
		return (R) this;
	}
			
	public StatementProxy<I> link(String column, Function<I,?> extractor) {
		this.boundColumns.put(column, extractor);		
		return this;
	}	

	public StatementProxy<I> linkConverted(String column, Function<I,?> extractor, Function<String, String> converter) {
		this.boundColumns.put(column, extractor);		
		this.columnValueCoverters.put(column, converter);
		
		return this;
	}
	
	@SuppressWarnings("unchecked")
	public <T> StatementProxy<I> onCommit(Consumer<T> saver) {
		this.saver = (Consumer<Object>) saver;
 		return this;
	}
	
	protected Map<String, Object> disassemble(I object) {		
		return boundColumns.entrySet()
			.stream()
			.collect(
				LinkedHashMap::new, 
				(M,E)->M.put(E.getKey(), E.getValue().apply(object)), 
				LinkedHashMap::putAll
			);
	}
	
	protected void addEntityToBatch(PreparedStatement statement, I entity) throws SQLException {
		Map<String, Object> shards = disassemble(entity);
		Integer counter = 1;
		
		for (Object value : shards.values()) {
			if (value == null)
				statement.setString(counter++, null);
			else if (value instanceof ExternalProperty) {
				statement.setObject(counter++, ((ExternalProperty<?>) value).get());
			} else {
				statement.setObject(counter++, value);
			}
		};	
		
		Map<String, Object> conditions = mapConditions(entity);
		
		if (statement.getParameterMetaData().getParameterCount() != 0) {
			for (Object value : conditions.values()) {
				if (!Objects.equals(value, "#NONE#")) {
					statement.setObject(counter++, value);
				}
			};	
		}
		
		statement.addBatch();
	}
		
	protected PreparedStatement generateStatement(Connection connection) throws SQLException {
		Objects.requireNonNull(table, "Unable to identify database table. Please bind it first");
		
		PreparedStatement statement = connection.prepareStatement(assembleStatement(), Statement.RETURN_GENERATED_KEYS);
		
		if (batchMode) {
			for (I entity : objects) {
				addEntityToBatch(statement, entity);
			}
		} else {
			addEntityToBatch(statement, object);
		}
		
		return statement;
	}
	
	protected abstract String assembleStatement();

	@Override
	public void execute(ConnectionProxy connection) throws SQLException {
		if (batchMode && objects.isEmpty()) {
			AppClient.getInstance().getLogger().tag(this.getClass()).debug("Empty collection found for the operation [%s]. Skipping..", this.getClass().getSimpleName());
			return;
		}
			
		try (Connection db = connection.get()) {
			PreparedStatement statement = generateStatement(db);		
			statement.executeBatch();
			
			ResultSet keys = statement.getGeneratedKeys();
 			
			if (batchMode && saver != null) {
				throw new UnsupportedOperationException("OnCommit callback is unsupported for collections");
			} 

			if (saver != null)
				while (keys.next()) {
					saver.accept(keys.getObject(1));
				}
			
			statement.close();
		}  
	}
}
