package app.context.services.db;

import java.util.Collection;
import java.util.Objects;

public class DatabaseDelete<I> extends StatementProxy<I> {
	private static final String TEMPLATE = "DELETE FROM #TABLE# #CONDITIONS#";

	public DatabaseDelete(Collection<I> collection) {
		super(collection);
	}
	
	public DatabaseDelete(I object) {
		super(object);
	}
	
	public DatabaseDelete<I> bind(String column, String value) {
		return this;
	}
	
	@Override
	protected String assembleStatement() {
		if (batchMode)
			Objects.requireNonNull(objects, "No entity list is bound. Transport aborted");
		else
			Objects.requireNonNull(object, "No entity is bound. Transport aborted");
		
		String conditions = assembleConditions();					
		return TEMPLATE.replace("#TABLE#", table).replace("#CONDITIONS#", conditions.isEmpty() ? "" : "WHERE " + conditions);		
	}
}