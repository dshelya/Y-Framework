package app.context.services.db;

import java.util.Collection;

public class DatabaseInsert<I> extends StatementProxy<I> {
	public DatabaseInsert(I object) {
		super(object);
	}

	public DatabaseInsert(Collection<I> collection) {
		super(collection);
	}

	public DatabaseInsert<I> bind(String column, String value) {
		return this;
	}

	@Override
	protected String assembleStatement() {
		throw new UnsupportedOperationException("Not Implemented");
	}
}