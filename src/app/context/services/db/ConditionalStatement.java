package app.context.services.db;

import java.sql.SQLException;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

import app.context.services.ConnectionProxy;

public abstract class ConditionalStatement<I> {
	protected Map<String, Function<I,String>> conditions = new LinkedHashMap<>();

	@SuppressWarnings("unchecked")
	public <R extends ConditionalStatement<I>> R condition(String column, Function<I,String> conditionMapper) {
		conditions.put(column, conditionMapper);		
		return (R) this;
	}
	
	@SuppressWarnings("unchecked")
	public <R extends ConditionalStatement<I>> R condition(String column) {
		conditions.put(column, V -> "#NONE#");		
		return (R) this;
	}
	
	protected Map<String, Object> mapConditions(I object) {
		return conditions.entrySet()
			.stream()
			.collect(
				LinkedHashMap::new, 
				(M,E) -> M.put(E.getKey(), E.getValue() == null ? null : E.getValue().apply(object)), 
				LinkedHashMap::putAll
			);
	}
	
 	protected String assembleConditions() {
		return conditions.entrySet()
			.stream()
			.map(C -> C.getKey() + (C.getValue() == null || "#NONE#".equals(C.getValue().apply(null)) ? "" : "= ?"))
			.collect(Collectors.joining(" AND "));	
	}

	public abstract void execute(ConnectionProxy connectionProxy) throws SQLException;
}
