package app.context.services.db;

import java.util.function.Consumer;
import java.util.function.Function;

 
public interface Entity {
	
	@SuppressWarnings("unchecked")
	default <T> Consumer<Object> link(Consumer<T> setter) {
		return O -> setter.accept((T) O);
	}

 	static <A, R> Function<Object, Object> cast(Function <String, R> mapper) {
 		return F -> (R) mapper.apply((String) F);
	}
}
