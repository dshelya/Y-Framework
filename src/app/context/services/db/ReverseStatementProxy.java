package app.context.services.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.Set;
import java.util.function.Consumer;
import java.util.function.Function;

import app.context.services.ConnectionProxy;
import app.module.api.ExternalProperty;

public abstract class ReverseStatementProxy<I extends Entity> extends ConditionalStatement<I> {
	private Class<I> type;

	protected boolean batchMode;
	protected Collection<I> objects;
	protected I object;
	protected String table;
	
	protected Map<String, Function<I, ExternalProperty<?>>> columnMappers; 	
	protected Map<String, Function<I, Consumer<Object>>> columnConsumers; 	
	protected Map<String, Function<Object, Object>> valueConverters;

	protected List<DatabaseJoin<I>> joins;
	protected String orderBy;

	{
		joins = new ArrayList<>();
		columnMappers = new LinkedHashMap<>();
		valueConverters = new HashMap<>();
		columnConsumers = new LinkedHashMap<>();
	}

	public ReverseStatementProxy(I object) {
		this.object = object;
	}

	public ReverseStatementProxy(Collection<I> objects) {
		this.objects = objects;
		batchMode = true;
	}
	
	public ReverseStatementProxy<I> fromTable(String table) {
		this.table = table;
		return this;
	}	
	
	public ReverseStatementProxy<I> map(String column, Function<I, ExternalProperty<?>> mapper) {
		this.columnMappers.put(column, mapper);		
		return this;
	}
	
	public ReverseStatementProxy<I> flatMap(String column, Function<I, Consumer<Object>> mapper) {
		this.columnConsumers.put(column, mapper);		
		return this;
	}
	
	public ReverseStatementProxy<I> map(String column, Function<I, ExternalProperty<?>> mapper, Function<Object,Object> converter) {
		this.columnMappers.put(column, mapper);		
		this.valueConverters.put(column, converter);
		
		return this;
	}
	
	public ReverseStatementProxy<I> flatMap(String column, Function<I, Consumer<Object>> mapper, Function<Object,Object> converter) {
		this.columnConsumers.put(column, mapper);		
		this.valueConverters.put(column, converter);
		
		return this;
	}
	
	public ReverseStatementProxy<I> orderBy(String orderBy) {
		this.orderBy = orderBy;
 		return this;
	}
		
	protected Set<String> getColumns() {
		HashSet<String> columns = new HashSet<>(columnMappers.keySet());		
		columns.addAll(columnConsumers.keySet());
 		return columns;
	}	
	
	protected void addEntityToBatch(PreparedStatement statement, I entity) throws SQLException {
		Map<String, Object> shards = mapConditions(entity);
		Integer counter = 1;

		if (statement.getParameterMetaData().getParameterCount() != 0) {
			for (Object value : shards.values()) {
				if (!Objects.equals(value, "#NONE#")) {
					statement.setObject(counter++, value);
				}
			}
		}

		statement.addBatch();
	}
		
	protected PreparedStatement generateStatement(Connection db) throws SQLException {
		Objects.requireNonNull(table, "Unable to identify database table. Please bind it first");
		
		PreparedStatement statement = db.prepareStatement(assembleStatement());
		
		if (batchMode) {
			addEntityToBatch(statement, null);
		} else {
			addEntityToBatch(statement, object);
		}
		
		return statement;
	}
	
	protected abstract String assembleStatement();

	public ReverseStatementProxy<I> asCollectionOf(Class<I> type) {
		this.type = type;
		return this;
	}
	
	@Override
	public void execute(ConnectionProxy connection) throws SQLException {
		try(Connection db = connection.get()) {		
			PreparedStatement statement = generateStatement(db);		
			ResultSet results = statement.executeQuery();
			
			if (batchMode) {
				Objects.requireNonNull(objects, String.format("Null collection found for the operation [%s]", this.getClass().getSimpleName()));
				Objects.requireNonNull(type, "Type of returning collection is unfefined. Please use [asCollectionOf] menthod to set a type");
				
				while (results.next()) {
					try {
											
						try {
							object = type.newInstance();
						} catch (Exception e) {
							throw new RuntimeException("No default constructor is available for the type "+type.getSimpleName());
						}
						
						for (Entry<String, Function<I, ExternalProperty<?>>> mapper : columnMappers.entrySet()) {
							Function<I, ExternalProperty<?>> extractor = mapper.getValue();
							Function<Object, Object> converter = valueConverters.getOrDefault(mapper.getKey(), Function.identity());
							
							String columnName = mapper.getKey()
									.replaceAll(".*\\.", "")
									.replaceAll(".*\\s+AS\\s+(.*)", "$1");
							
							Object value = converter.apply(results.getObject(columnName));
							extractor.apply(object).set(value);						
						}
						
						for (Entry<String, Function<I, Consumer<Object>>> entry : columnConsumers.entrySet()) {
							Function<I, Consumer<Object>> extractor = entry.getValue();
							Function<Object, Object> converter = valueConverters.getOrDefault(entry.getKey(), Function.identity());
							
							String columnName = entry.getKey()
									.replaceAll(".*\\.", "")
									.replaceAll(".*\\s+AS\\s+(.*)", "$1");
							
							Object value = converter.apply(results.getObject(columnName));
							extractor.apply(object).accept(value);						
						}	
						
						objects.add(object);
					} catch (Exception e) {
						throw new DatabaseRuntimeException(e);
					}
				}
			} else {
				if (results.next()) {
					for (Entry<String, Function<I, ExternalProperty<?>>> mapper : columnMappers.entrySet()) {
						Function<I, ExternalProperty<?>> extractor = mapper.getValue();
						Function<Object, Object> converter = valueConverters.getOrDefault(mapper.getKey(), Function.identity());
						String columnName = mapper.getKey()
								.replaceAll(".*\\.", "")
								.replaceAll(".*\\s+AS\\s+(.*)", "$1");
						
						Object value = converter.apply(results.getObject(columnName));
						extractor.apply(object).set(value);
					}
					
					for (Entry<String, Function<I, Consumer<Object>>> mapper : columnConsumers.entrySet()) {
						Function<I, Consumer<Object>> extractor = mapper.getValue();
						Function<Object, Object> converter = valueConverters.getOrDefault(mapper.getKey(), Function.identity());
						String columnName = mapper.getKey()
								.replaceAll(".*\\.", "")
								.replaceAll(".*\\s+AS\\s+(.*)", "$1");
						
						Object value = converter.apply(results.getObject(columnName));
						extractor.apply(object).accept(value);
					}
				}
			}
				
			statement.close();
		}
	}

	public DatabaseJoin<I> leftJoin(String table) {
		return new DatabaseJoin<>(this, table, true);
	}
	
	public DatabaseJoin<I> join(String table) {
		return new DatabaseJoin<>(this, table, false);
	}

	void addJoin(DatabaseJoin<I> databaseJoin) {
		joins.add(databaseJoin);
	}
}
