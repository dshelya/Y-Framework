package app.context.services.db;

public class DatabaseJoin<I extends Entity> {

	private ReverseStatementProxy<I> reverseStatementProxy;
	private String table;
	private String join;
	private boolean isLeft;

	public DatabaseJoin(ReverseStatementProxy<I> reverseStatementProxy, String table, boolean isLeft) {
		this.reverseStatementProxy = reverseStatementProxy;
		this.table = table;
		this.isLeft = isLeft;
	}
	
	public ReverseStatementProxy<I> on(String baseColumn, String joinedColumn) {
		String base = reverseStatementProxy.table;
		this.join = String.format("%s JOIN %s ON %s.%s = %s.%s", isLeft ? "LEFT" : "", table, base, baseColumn, table, joinedColumn);
		
		this.reverseStatementProxy.addJoin(this);
		return reverseStatementProxy;
	}
	
	public ReverseStatementProxy<I> on(String sameColumn) {
		return on(sameColumn, sameColumn);
	}
	
	String getJoinStatement() {
		return join;
	}
}
