package app.context.services.db;

import java.util.Collection;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

public class DatabaseUpdate<I> extends StatementProxy<I> {
	private static final String TEMPLATE = "UPDATE #TABLE# SET #COLUMNS# #CONDITIONS#";
	
	public DatabaseUpdate(Collection<I> collection) {
		super(collection);
	}
	
	public DatabaseUpdate(I object) {
		super(object);
	}
	
	public DatabaseUpdate<I> bind(String column, String value) {
		return this;
	}

	@Override
	protected String assembleStatement() {
		Optional<I> possibleEntity = batchMode ? objects.stream().findFirst() : Optional.ofNullable(object);		
		I entity = possibleEntity.orElseThrow(() -> new DatabaseRuntimeException("No entity is bound. Transport aborted"));
		
		Map<String, Object> shards = disassemble(entity);
		
		String columns = shards.keySet()
			.stream()
			.map(C -> C + "= " + columnValueCoverters.getOrDefault(C, X -> "?").apply(C))
			.collect(Collectors.joining(","));
			
		String conditions = assembleConditions();
			
		return TEMPLATE.replace("#TABLE#", table)
			.replace("#COLUMNS#", columns)
			.replace("#CONDITIONS#", conditions.isEmpty() ? "" : "WHERE " + conditions);	
	}
}