package app.context.services.db;

import java.util.Collection;
import java.util.Objects;
import java.util.stream.Collectors;

public final class DatabaseSelect<I extends Entity> extends ReverseStatementProxy<I> {
	private static final String TEMPLATE = "SELECT #COLUMNS# FROM #TABLE# #JOINS# #CONDITIONS# #ORDERBY#";

	public DatabaseSelect(Collection<I> collection) {
		super(collection);
	}
	
	public DatabaseSelect(I object) {
		super(object);
	}	
			
	protected String assembleStatement() {		
		String columns = getColumns().stream().collect(Collectors.joining(","));			
		String conditions = assembleConditions();
		
		String joinStatements = joins.stream()
			.map(DatabaseJoin::getJoinStatement)
			.collect(Collectors.joining(" "));
		
		String statement = TEMPLATE.replace("#TABLE#", table)
			.replace("#COLUMNS#", columns)
			.replace("#JOINS#", joinStatements)
			.replace("#CONDITIONS#", conditions.isEmpty() ? "" : "WHERE " + conditions)
			.replace("#ORDERBY#", Objects.toString(orderBy, "").isEmpty() ? "" : "ORDER BY " + orderBy);	
		
		return statement;
	}	
}