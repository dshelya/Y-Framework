package app.context.services;

import java.util.Objects;

import app.context.services.interfaces.ConfigService;
import app.context.services.interfaces.Context;
import app.context.services.interfaces.DatabaseService;
import app.context.services.interfaces.GuiAccessService;
import app.context.services.interfaces.LoggerService;
import app.context.services.interfaces.NotificationService;

public abstract class GenericService {
	protected Context context;
	protected DatabaseService database;
	protected GuiAccessService gui;
	protected NotificationService notifier;
	protected ConfigService config;

	protected GenericService() {
		throw new IllegalStateException("You made a great mistake!");
	}
	
	public GenericService(Context appContext) {
		Objects.requireNonNull(appContext, "Cannot initialize application context. Please check your build path libs order");	
		
		context = appContext;
		database = context.getDatabase();
		gui = context.getGui();
		notifier = context.getNotifier();
		config = context.getConfig();
	}
	
	protected LoggerService getLogger() {
		return context.getLogger().tag(this.getClass());
	}

	public abstract void shutdown();
}
