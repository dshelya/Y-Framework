package app.context.services;

import java.sql.SQLException;

import app.context.AppClient;
import app.context.services.db.ConditionalStatement;
import app.context.services.interfaces.DatabaseService;
import app.context.services.interfaces.LoggerService;

public abstract class DatabaseWrapper {
	private DatabaseService dbService;
	protected LoggerService log;

	public DatabaseWrapper() {
		this.log = AppClient.getInstance().getLogger().tag(this.getClass());
	}
	
	public DatabaseService getService() {
		return dbService;
	}
	
 	public <I> void manage(ConditionalStatement<I> enitityProxy, ConnectionProxy connection) throws SQLException {
		enitityProxy.execute(connection);			
	}
 	
 	public <I> void manage(ConditionalStatement<I> enitityProxy) {
		try {
			getService().manage(enitityProxy);
		} catch (SQLException e) {
 			throw new SystemRuntimeException(e);
		}
	}
	
	//public <I> void manage(ReverseStatementProxy<I> enitiryProxy) throws SQLException {
	//	getService().manage(enitiryProxy);		
	//}
	
	/*
	 * Added to be compatible with extensible module api
	 */
	@SuppressWarnings("unchecked")
	public <R extends DatabaseWrapper> R bindService(DatabaseService dbService) {
		this.dbService = dbService;
		return (R) this;
	}
	
	public ConnectionProxy getConnection() {
        try {
            return dbService.newConnection();  
        } catch (SQLException e) {
            log.debug("Cannot access database connection");
            throw new RuntimeException("Invalid Database Connection");
        }
    }
}
