package app.context;

import java.util.Map;

import app.context.services.impl.LocalContext;
import app.context.services.interfaces.Context;

public class AppClient {	
	public static Context getInstance(String appName) {
		return LocalContext.getInstance(appName);
	}
	
	public static Context getInstance() {
		return LocalContext.getInstance();
	}

	public static Context getInstance(String appName, Map<String, String> arguments) {
		return LocalContext.getInstance(appName, arguments);
	}
}
