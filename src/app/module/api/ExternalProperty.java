package app.module.api;

import java.math.BigDecimal;
import java.text.FieldPosition;
import java.text.Format;
import java.text.ParsePosition;
import java.time.LocalDate;
import java.util.Objects;
import java.util.function.Function;

import app.util.converters.DateStringConverter;
import javafx.beans.property.SimpleObjectProperty;

@Configurable
public class ExternalProperty<T> extends SimpleObjectProperty<T> {
	public static final Format DOUBLE_STRING_FORMAT = doubleStringFormatter();
	public static final Format BIGDECIMAL_STRING_FORMAT = bigDecimalStringFormatter();
	public static final Format INTEGER_STRING_FORMAT = integerStringFormatter();
	public static final Format BOOLEAN_STRING_FORMAT = booleanStringFormatter();
	public static final Format DATE_STRING_FORMAT = localDateStringFormatter();

	private boolean isBridged;

	public static void main(String[] args) {
		ExternalProperty<String> data = new ExternalProperty<>();
		System.err.println(data);
	}

	public ExternalProperty() {
	}

	public ExternalProperty(T value) {
		super(value);
		set(value);
	}

	@SuppressWarnings("unchecked")
	public void set(Object value) {
		if (!isBridged)
			super.set((T) value);
	}

	@Override
	public T get() {
		return super.get();
	}

	public void setBridged(boolean isBridged) {
		this.isBridged = isBridged;
	}
	
	private static <S> Object safeParse(Function<String, S> parser, String source) {		
		try {
			return source == null ? null : parser.apply(source);
		} catch (Exception e) {
 			return null;
		}
	}

	@SuppressWarnings("serial")
	private static Format localDateStringFormatter() {
		return new Format() {
			@Override
			public StringBuffer format(Object obj, StringBuffer toAppendTo, FieldPosition pos) {
				return toAppendTo.append(obj == null ? null : DateStringConverter.get().toString((LocalDate) obj));
			}

			@Override
			public Object parseObject(String source, ParsePosition pos) {
				pos.setIndex(1);
				return safeParse(DateStringConverter.get()::fromString, source);
			}
		};
	}

	@SuppressWarnings("serial")
	private static Format booleanStringFormatter() {
		return new Format() {
			@Override
			public StringBuffer format(Object obj, StringBuffer toAppendTo, FieldPosition pos) {
				return toAppendTo.append(obj == null ? null : obj.toString());
			}

			@Override
			public Object parseObject(String source, ParsePosition pos) {
				pos.setIndex(1);
				return safeParse(Boolean::valueOf, source);
			}
		};
	}
	
	@SuppressWarnings("serial")
	private static Format doubleStringFormatter() {
		return new Format() {
			@Override
			public StringBuffer format(Object obj, StringBuffer toAppendTo, FieldPosition pos) {
				return toAppendTo.append(obj == null ? null : obj.toString());
			}

			@Override
			public Object parseObject(String source, ParsePosition pos) {
				pos.setIndex(1);
				return safeParse(Double::valueOf, source);
			}
		};
	}

	@SuppressWarnings("serial")
	private static Format bigDecimalStringFormatter() {
		return new Format() {
			@Override
			public StringBuffer format(Object obj, StringBuffer toAppendTo, FieldPosition pos) {
				return toAppendTo.append(Objects.toString(obj, ""));
			}

			@Override
			public Object parseObject(String source, ParsePosition pos) {
				pos.setIndex(1);
				return safeParse(BigDecimal::new, source);
			}
		};
	}

	@SuppressWarnings("serial")
	private static Format integerStringFormatter() {
		return new Format() {
			@Override
			public StringBuffer format(Object obj, StringBuffer toAppendTo, FieldPosition pos) {
				return toAppendTo.append(obj == null ? null : obj.toString());
			}

			@Override
			public Object parseObject(String source, ParsePosition pos) {
				pos.setIndex(1);
				return safeParse(Integer::valueOf, source);
			}
		};
	}
}
