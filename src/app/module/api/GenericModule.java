package app.module.api;

import java.io.File;
import java.lang.reflect.Field;
import java.net.URISyntaxException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.Future;
import java.util.concurrent.atomic.AtomicBoolean;

import app.context.AppClient;
import app.context.services.interfaces.ConfigService;
import app.context.services.interfaces.DatabaseService;
import app.context.services.interfaces.GuiAccessService;
import app.context.services.interfaces.LoggerService;
import app.context.services.interfaces.NotificationService;
import app.context.services.interfaces.SchedulerService;
import app.context.services.interfaces.SharedContext;
import app.context.services.interfaces.Task;
import app.context.services.interfaces.WebViewService;
import app.context.services.interfaces.WebViewer;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.scene.control.ProgressIndicator;

public abstract class GenericModule implements SharedContext {
 	protected ProgressIndicator monitorProgress;
	protected StringProperty errorLine;
	protected LoggerService log;
	
	protected SchedulerService scheduler;
	protected WebViewService web;
	
	private Optional<Task> startCallback;
	private Optional<Task> stopCallback;

	private AtomicBoolean isRunning = new AtomicBoolean(false);
	private AtomicBoolean isPaused = new AtomicBoolean(false);

	private List<Future<?>> activeTasks = Collections.synchronizedList(new ArrayList<>());	
	private GenericModule parentModule;

	public GenericModule() {
		Objects.requireNonNull(context, "Cannot initialize application context. Please check your build path libs order");

		WebViewer webViewer = context.getWebViewer();

		log = context.getLogger().tag(this.getClass());	
		activeTasks = Collections.synchronizedList(new ArrayList<>());
		errorLine = new SimpleStringProperty();
		scheduler = context.getScheduler();		
		web = webViewer != null ? webViewer.getWebViewService(scheduler) : context.getDefaultWebViewer();
		
		startCallback = Optional.empty();
		stopCallback = Optional.empty();
		
		AppClient.getInstance().registerModule(getClass().getName(), this);
		setParent(null);
	}

	public abstract String getModuleName();

	public void showProgress(boolean show) {
		monitorProgress.setVisible(show);
	}	
	
	public void onStart(Task startCallback) {
		this.startCallback = Optional.of(startCallback);
	}
	
	public void onStop(Task stopCallback) {
		this.stopCallback = Optional.of(stopCallback);
	}
	
	public void start() {
		log.info("Starting %s...", getModuleName());

		startCallback.ifPresent(scheduler::runSafe);
		isRunning.set(true);
	}
	
	//@SuppressWarnings("unchecked")
	public void setParent(GenericModule parentModule) {
		this.parentModule = parentModule;		
	}
	
	public GenericModule parent() {
		return parentModule;
	}
	
	public GenericModule getRoot() {
		GenericModule root = this;		
		
		while (true) {
			if (root.parent() == null)
				return root;
			
			root = root.parent();			
		}	
	}
	
	public void stop() {
		if (isPaused.get() || isRunning.get()) {
			log.info("Stopping %s...", getModuleName());
	
			stopCallback.ifPresent(scheduler::runSafe);
			
			for (Future<?> activeTask : activeTasks) {
				activeTask.cancel(true);
			}
			
			scheduler.cancelTasks();
			activeTasks.clear();
			
			isRunning.set(false);
		}
	}
	
	public void addActiveTask(Future<?> task) {
		if (isRunning.get())
			this.activeTasks.add(task);
		else {
			task.cancel(true);	
		}
	}
	
	public boolean isRunning() {
		return isRunning.get();
	}
	
	public void resume() {
		isPaused.set(false);
	}

	public void pause() {
		isPaused.set(true);
	}
	
	public boolean isPaused() {
		return !isRunning.get() ? false : isPaused.get();
	}

	public void bindProgressIndicator(ProgressIndicator monitorProgress) {
		this.monitorProgress = monitorProgress;
	}
	
	public void bindErrorLine(StringProperty errorLine) {
		errorLine.bind(this.errorLine);
	}
	
	public void showError(Throwable message) {
		Throwable cause = log.getRootCause(message);
		
		log.info("[%s] Error: %s", getModuleName(), cause.getMessage());
		log.debug(message);
		
		scheduler.runInMainThread(() -> this.errorLine.set(cause.getMessage()));
	}
	
	public void clearError() {
		scheduler.runInMainThread(() -> this.errorLine.set(null));
	}

	public Set<Field> getConfigurableFields() {
		Set<Field> set = new HashSet<>();

		for (Field field : this.getClass().getDeclaredFields()) {
			if (field.isAnnotationPresent(Configurable.class)) {
				set.add(field);
			}
		}
		
		return set;
	}
	
	public DatabaseService getSystemDatabaseService() {
		return database;
	}

	public GuiAccessService getSystemGuiControlService() {
		return gui;		
	}

	public LoggerService getSystemLoggerService() {
		return context.getLogger();		
	}

	public ConfigService getSystemSettingsService() {
		return config;		
	}

	public WebViewService getSystemWebViewService() {
		return web;		
	}
	
	public NotificationService getSystemNotificationService() {
		return notifier;		
	}
	
	public SchedulerService getSystemSchedulerService() {
		return scheduler;
	}	

	public int version() {
		try {
			String path = getClass().getCanonicalName().replace('.', '/') + ".class";
			URL resource = getClass().getClassLoader().getResource(path);
			Date time = null;

			if (resource != null) {
				if (resource.getProtocol().equals("file")) {
					time = new Date(new File(resource.toURI()).lastModified());
				} else if (resource.getProtocol().equals("jar")) {
					String filepath = resource.getPath();
					time = new Date(new File(filepath.substring(5, filepath.indexOf("!"))).lastModified());
				} 
			}

			return Integer.valueOf(new SimpleDateFormat("ddMMyyHH").format(time));
		} catch (URISyntaxException e) {
			return 0;
		}
	}

	@SuppressWarnings("unchecked")
	public <T> Optional<ExternalProperty<T>> getProperty(String name) {	
		return getConfigurableFields().stream()
			.filter(F -> F.getName().equals(name))
			.map(F -> {
				try {
					F.setAccessible(true);
					return (ExternalProperty<T>) F.get(this);
				} catch (Exception e) {
					return null;
				}
			})
			.filter(Objects::nonNull)
			.findFirst();
	}
}

