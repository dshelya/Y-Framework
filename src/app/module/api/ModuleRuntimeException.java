package app.module.api;

@SuppressWarnings("serial")
public class ModuleRuntimeException extends RuntimeException {

	public ModuleRuntimeException(String string) {
		super(string);
	}

	public ModuleRuntimeException(Exception e) {
		super(e);
	}
}
