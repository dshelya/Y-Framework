package app.module.api;

@SuppressWarnings("serial")
public class ModuleException extends Exception {

	public ModuleException(String string) {
		super(string);
	}

}
