package app.module.api;

import java.util.function.Consumer;
import java.util.function.Supplier;

import javafx.beans.property.SimpleObjectProperty;

public class ViewProperty<V> extends SimpleObjectProperty<V> {

	private Consumer<V> setter;
	private Supplier<V> getter;

	private ViewProperty(Consumer<V> setter, Supplier<V> getter) {
		this.setter = setter;
		this.getter = getter;
	}

	public static final <V> ViewProperty<V> of(Supplier<V> getter, Consumer<V> setter) {
		return new ViewProperty<>(setter, getter);
	}

	public static final <V> ViewProperty<V> of(Supplier<V> getter) {
		return new ViewProperty<>(null, getter);
	}

	@Override
	public V get() {
		return getter.get();
	}

	@Override
	public void set(V value) {
		super.set(value);
		
		if (setter != null)
			setter.accept(value);
 	}

	@Override
	public void setValue(V value) {
		super.setValue(value);

		if (setter != null)
			setter.accept(value);		
	}

	@Override
	public V getValue() {
		return getter.get();
	}

	public void update() {
 		set(get());
	}
}
