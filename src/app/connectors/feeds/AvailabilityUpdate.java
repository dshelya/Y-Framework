package app.connectors.feeds;

import app.connectors.amazon.FeedMessage;

public class AvailabilityUpdate implements FeedMessage {		
	private static final String MESSAGE =
		"<OperationType>Update</OperationType>\n"+
		"<Inventory>\n"+
		"    <SKU>%s</SKU>\n"+
		"    <Quantity>%s</Quantity>\n"+
		"    <FulfillmentLatency>%s</FulfillmentLatency>\n"+
		"</Inventory>";
	
	private static final String FEED_TYPE = "_POST_INVENTORY_AVAILABILITY_DATA_";
	private static final String OPERATION_TYPE = "Inventory";
	
 	private String sku = "";
	private String fulfillmentLatency = ""; 	
	private String quantity = "";

	private boolean updated;
	private String updateError;
	
	public AvailabilityUpdate() {
		this.updated = true;
	}
	
	public boolean isUpdated() {
		return updated;
	}

	public String updateError() {
		return updateError;
	}

	public void setUpdateError(String updateError) {
		this.updateError = updateError;
		this.updated = false;
	}

	public AvailabilityUpdate sku(String sku) {
		this.sku = sku;
		return this;
	}
	
	public String entityID() {
		return sku;
	}

	
	public AvailabilityUpdate fulfillmentLatency(String fulfillmentLatency) {
		this.fulfillmentLatency = fulfillmentLatency;
		return this;
	}
	
	public AvailabilityUpdate quantity(String quantity) {
		this.quantity = quantity;
		return this;
	}
	
	@Override
	public String toString() {
  		return String.format(MESSAGE, xmlEscape(sku), quantity, fulfillmentLatency);
	}
 
	@Override
	public String feedType() {
		return FEED_TYPE;
	}

	@Override
	public String operationType() {
		return OPERATION_TYPE;
	}

	@Override
	public boolean isValid() {
 		return isSKUValid(sku);
	}
}
