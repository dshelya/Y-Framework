package app.connectors.feeds;

import app.connectors.amazon.FeedMessage;

public class PriceUpdate implements FeedMessage {	
	private static final String MESSAGE = 
			"<Price>\n" + 
			"     <SKU>%s</SKU>\n" + 
			"     <StandardPrice currency=\"USD\">%s</StandardPrice>\n" + 
			"     %s"+
			"</Price>\n";
	
	private static final String SALE = 
			"<Sale>\n" + 
			"    <StartDate>%s</StartDate>\n" + 
			"    <EndDate>%s</EndDate>\n" + 
			"    <SalePrice currency=\"USD\">%s</SalePrice>\n" + 
			"</Sale>\n"; 
	
	private static final String FEED_TYPE = "_POST_PRODUCT_PRICING_DATA_";
	private static final String MESSAGE_TYPE = "Price";
	
 	private String sku = "";
	private String standardPrice = ""; 	
	private String startDate = "";
	private String endDate = "";
	private String salePrice = "";

	private boolean updated;
	private String updateError;
	
	public PriceUpdate() {
		this.updated = true;
	}
	
	public boolean isUpdated() {
		return updated;
	}

	public String updateError() {
		return updateError;
	}

	public void setUpdateError(String updateError) {
		this.updateError = updateError;
		this.updated = false;
	}

	public PriceUpdate sku(String sku) {
		this.sku = sku;
		return this;
	}
	
	public String entityID() {
		return sku;
	}

	public PriceUpdate startDate(String startDate) {
		this.startDate = startDate;
		return this;
	}

	public PriceUpdate endDate(String endDate) {
		this.endDate = endDate;
		return this;
	}
	
	public PriceUpdate salePrice(double salePrice) {
		this.salePrice = String.format("%.2f", salePrice);
		return this;
	}
	
	public PriceUpdate salePrice(String salePrice) {
		this.salePrice = salePrice;
		return this;
	}
	 
	public PriceUpdate standardPrice(double standardPrice) {
		this.standardPrice = String.format("%.2f", standardPrice);
		return this;
	}
	
	public PriceUpdate standardPrice(String standardPrice) {
		this.standardPrice = standardPrice;
		return this;
	}
	
	@Override
	public String toString() {
		String sale = salePrice.isEmpty() ? "" : String.format(SALE, startDate, endDate, salePrice);
 		return String.format(MESSAGE, xmlEscape(sku), standardPrice, sale);
	}
 
	@Override
	public String feedType() {
		return FEED_TYPE;
	}

	@Override
	public String operationType() {
		return MESSAGE_TYPE;
	}

	@Override
	public boolean isValid() {
 		return isSKUValid(sku);
	}
}
