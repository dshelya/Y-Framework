package app.connectors.feeds;

import app.connectors.amazon.FeedMessage;

public class OrderFulfillment implements FeedMessage {	
	private static final String MESSAGE = 
			"  <OrderFulfillment>\n" + 
			"	  <AmazonOrderID>%s</AmazonOrderID>\n" + 
			"	  <FulfillmentDate>%s</FulfillmentDate>\n" + 
			"	  <FulfillmentData>\n" + 
			"		  <CarrierName>%s</CarrierName>\n" + 
			"		  <ShippingMethod>AIR</ShippingMethod>\n" + 
			"		  <ShipperTrackingNumber>%s</ShipperTrackingNumber>\n" + 
			"	  </FulfillmentData>\n" + 
			"  </OrderFulfillment>\n";
	
	private static final String FEED_TYPE = "_POST_ORDER_FULFILLMENT_DATA_";
	private static final String MESSAGE_TYPE = "OrderFulfillment";

 	private String orderID;
	private String shipDate;
	private String carrierName;
	private String trackNum;
	private String updateError;
	private boolean ignoreTrackingInfo;
	private boolean updated;

	public OrderFulfillment() {
		this.updated = true;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((orderID == null) ? 0 : orderID.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		OrderFulfillment other = (OrderFulfillment) obj;
		if (orderID == null) {
			if (other.orderID != null)
				return false;
		} else if (!orderID.equals(other.orderID))
			return false;
		return true;
	}

	public boolean isUpdated() {
		return updated;
	}

	public String updateError() {
		return updateError;
	}
	
	public OrderFulfillment ignoreTrackingInfo(boolean ignore) {
		this.ignoreTrackingInfo = ignore;
		return this;
	}
	
	public boolean ignoreTrackingInfo() {
		return ignoreTrackingInfo;
	}

	public void setUpdateError(String updateError) {
		this.updateError = updateError;
		this.updated = false;
	}

	public OrderFulfillment amazonOrderID(String amazonOrderID) {
		this.orderID = amazonOrderID;
		return this;
	}
	
	public String entityID() {
		return orderID;
	}

	public OrderFulfillment fulfillmentDate(String fulfillmentDate) {
		this.shipDate = fulfillmentDate;
		return this;
	}

	public OrderFulfillment carrierName(String carrierName) {
		this.carrierName = carrierName;
		return this;
	}

	public OrderFulfillment trackingNumber(String shipperTrackingNumber) {
		this.trackNum = shipperTrackingNumber;
		return this;
	}
	
	@Override
	public String toString() {
		if (ignoreTrackingInfo)
			return String.format(MESSAGE, xmlEscape(orderID), shipDate, "", "");
		
 		return String.format(MESSAGE, xmlEscape(orderID), shipDate, xmlEscape(carrierName), xmlEscape(trackNum));
	}

	@Override
	public String feedType() {
		return FEED_TYPE;
	}

	@Override
	public String operationType() {
		return MESSAGE_TYPE;
	}

	@Override
	public boolean isValid() {
 		return true;
	}
}
