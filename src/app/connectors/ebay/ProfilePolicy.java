package app.connectors.ebay;

import java.util.Objects;

import com.ebay.soap.eBLBaseComponents.SupportedSellerProfileType;

public class ProfilePolicy {
	private Long profileId;
	private String profileName;

	public ProfilePolicy(SupportedSellerProfileType type) {
		profileId = type.getProfileID();
		profileName = type.getProfileName();
	}

	public Long getProfileId() {
		return profileId;
	}

	public String getProfileName() {
		return profileName;
	}

	@Override
	public String toString() {
		return Objects.toString(profileName, "N/A");
	}
}