package app.connectors.ebay;

import static java.util.Arrays.asList;
import static java.util.Arrays.stream;
import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.mapping;
import static java.util.stream.Collectors.toList;

import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Function;

import org.jsoup.Jsoup;

import com.ebay.sdk.ApiContext;
import com.ebay.sdk.ApiLogging;
import com.ebay.sdk.call.CompleteSaleCall;
import com.ebay.sdk.call.EndItemCall;
import com.ebay.sdk.call.GetOrdersCall;
import com.ebay.sdk.call.GetUserPreferencesCall;
import com.ebay.sdk.call.RelistItemCall;
import com.ebay.sdk.call.ReviseFixedPriceItemCall;
import com.ebay.sdk.call.ReviseInventoryStatusCall;
import com.ebay.soap.eBLBaseComponents.AmountType;
import com.ebay.soap.eBLBaseComponents.CurrencyCodeType;
import com.ebay.soap.eBLBaseComponents.EndReasonCodeType;
import com.ebay.soap.eBLBaseComponents.InventoryStatusType;
import com.ebay.soap.eBLBaseComponents.ItemType;
import com.ebay.soap.eBLBaseComponents.OrderIDArrayType;
import com.ebay.soap.eBLBaseComponents.OrderStatusCodeType;
import com.ebay.soap.eBLBaseComponents.OrderType;
import com.ebay.soap.eBLBaseComponents.ShipmentStatusCodeType;
import com.ebay.soap.eBLBaseComponents.ShipmentType;
import com.ebay.soap.eBLBaseComponents.SupportedSellerProfileType;
import com.ebay.soap.eBLBaseComponents.TradingRoleCodeType;
import com.ebay.soap.eBLBaseComponents.TransactionType;
import com.ebay.soap.eBLBaseComponents.VariationType;
import com.ebay.soap.eBLBaseComponents.VariationsType;
import com.ebay.soap.eBLBaseComponents.WarningLevelCodeType;

import app.connectors.ebay.GenericEbayOrder.Item;
import app.context.AppClient;
import app.context.services.impl.SystemLogger;
import app.context.services.interfaces.Context.WorkingMode;
import app.context.services.interfaces.DetachedLoggerService;

public class EbayConnector {	
	public enum PolicyType {
		PAYMENT, RETURN_POLICY, SHIPPING;		
  		
 		private static PolicyType valueOf(SupportedSellerProfileType type) {
 			return valueOf(type.getProfileType());
 		}
	}	
	
 	private static final  DetachedLoggerService LOGGER = SystemLogger.log();

	private final ApiContext apiContext;
 	private final Set<InventoryStatusType> updateQueue;

	public EbayConnector(String serviceUrl, String ebayToken) {
		apiContext = new ApiContext();
		
		ApiLogging apiLogging = new ApiLogging();
		apiLogging.setLogExceptions(false);
		apiLogging.setLogHTTPHeaders(false);
		apiLogging.setLogSOAPMessages(false);

		apiContext.setApiLogging(apiLogging);
		apiContext.setApiServerUrl(serviceUrl);
		apiContext.getApiCredential().seteBayToken(ebayToken);
		
		updateQueue = new HashSet<>();
	}

	public boolean updateShippingInfo(String orderId, Date shipTime, String carrier, String trackingNumber) {
		Calendar shipDate = Calendar.getInstance();
		shipDate.setTime(shipTime);

		CompleteSaleCall update = new CompleteSaleCall(apiContext);
		ShipmentType shipment = new ShipmentType();
		shipment.setShippingCarrierUsed(carrier);
		shipment.setShipmentTrackingNumber(trackingNumber);
		shipment.setShippedTime(shipDate);
		shipment.setStatus(ShipmentStatusCodeType.ACTIVE);
		
		update.setOrderID(orderId);
		update.setShipped(true);
		update.setPaid(true);
		update.setShipment(shipment);
		
		try {
			if (AppClient.getInstance().workingMode() == WorkingMode.SIMULATION) {
				LOGGER.info("Simulation mode enabled. Skipping any updates");
				return false;
			}
			
			update.completeSale();
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	/**
	 * When we set quantity to 0, item becomes hidden from search. We can relist
	 * the variation by setting quantity to 1+
	 */
	public void reviseItem(String ebayId, String sku, double price, int quantity) throws Exception {		
		ItemType item = new ItemType();		
		item.setItemID(ebayId);	

		AmountType amount = new AmountType();
		amount.setCurrencyID(CurrencyCodeType.USD);
		amount.setValue(price);
		
		if (sku == null) {
			item.setQuantity(quantity);
			item.setStartPrice(amount);
		} else {			
			item.setVariations(createVariationChange(sku, quantity, amount));	
		}		

		ReviseFixedPriceItemCall call = new ReviseFixedPriceItemCall(apiContext);
		call.setItemToBeRevised(item);			
		call.setEnableCompression(true);
		call.setWarningLevel(WarningLevelCodeType.LOW);
		
		if (AppClient.getInstance().workingMode() == WorkingMode.SIMULATION) {
			LOGGER.info("Simulation mode enabled. Skipping the update");
			return;
		}
		
		call.reviseFixedPriceItem();
	}
	
	public void reviseItem(String itemId, double price, int quantity) throws Exception {
		reviseItem(itemId, null, price, quantity);
	}

	private VariationsType createVariationChange(String sku, int quantity, AmountType amount) {
		VariationsType variations = new VariationsType();
		VariationType variation = new VariationType();	
		
		variation.setSKU(sku);
		variation.setQuantity(quantity);
		variation.setStartPrice(amount);
		variations.setVariation(new VariationType[] {variation});
		
		return variations;
	}
	
	/**
	 * Delegates the call to ebay-sdk's native GetUserPreferencesCall where the original response is being represented as a map
	 * with one of PolicyType values as a key and ProfilePolicy object as its value. 
	 * The value defines [profileId] and [profileName] accessible by getters
	 * </br></br><b> Example usage</b></br></br>
 	 * {@code
	 * Map<PolicyType, List<ProfilePolicy>> policies = connector.getProfilePolicies();
	 * List<ProfilePolicy> paymentPolicies = policies.get(PolicyType.PAYMENT);
	 * }</pre>
	 * @return mapped policies
	 * @throws Exception
	 */
	public Map<PolicyType, List<ProfilePolicy>> getProfilePolicies() throws Exception {
		GetUserPreferencesCall call = new GetUserPreferencesCall(apiContext);
		call.setShowSellerProfilePreferences(true);
		call.getUserPreferences();
		
		SupportedSellerProfileType[] response = call.getReturnedSellerProfilePreferences()
			.getSupportedSellerProfiles()
			.getSupportedSellerProfile();
		
 		return stream(response).collect(
			groupingBy(PolicyType::valueOf, mapping(ProfilePolicy::new, toList()))
		);	 
	}
	
	/**
	 * Delegates the call to ebay-sdk's native GetUserPreferencesCall where the original response is being represented as a map
	 * with one of PolicyType values as a key and ProfilePolicy object as its value. 
	 * The value defines [profileId] and [profileName] accessible by getters
	 * </br></br><b> Example usage</b></br></br>
 	 * {@code
	 * Map<PolicyType, List<ProfilePolicy>> policies = connector.getProfilePolicies();
	 * List<ProfilePolicy> paymentPolicies = policies.get(PolicyType.PAYMENT);
	 * }</pre>
	 * @return mapped policies
	 * @throws Exception
	 */
	public Double getShippingFeeByPolicyId(long policyId) throws Exception {
		String service = "https://svcs.ebay.com/services/selling/v1/SellerProfilesManagementService" +
			 "?X-EBAY-SOA-OPERATION-NAME=getSellerProfiles" +
			 "&X-EBAY-SOA-SERVICE-NAME=SellerProfilesManagementService" + 
			 "&X-EBAY-SOA-GLOBAL-ID=EBAY-US" +
			 "&X-EBAY-SOA-SERVICE-VERSION=1.0.0" +
			 "&X-EBAY-SOA-SECURITY-TOKEN=" + URLEncoder.encode(apiContext.getApiCredential().geteBayToken(), "UTF-8") +
			 "&X-EBAY-SOA-RESPONSE-DATA-FORMAT=XML" +
			 "&REST-PAYLOAD&includeDetails=true" +
			 "&profileId=" + policyId;
		
		String shippingFee = Jsoup.connect(service)
			.ignoreContentType(true)
			.get()
			.select("shippingPolicyInfo domesticShippingPolicyInfoService shippingServiceCost")
			.text();
			
		return Double.parseDouble(0+shippingFee);
	}
	
	public List<OrderType> readSpecificOrder(String orderId) throws Exception {
		GetOrdersCall orderCall = new GetOrdersCall(apiContext);

		OrderIDArrayType orderIds = new OrderIDArrayType();
		orderIds.setOrderID(new String[] { orderId });
		orderCall.setOrderIDArray(orderIds);
		
		return asList(orderCall.getOrders());
	}
	
	public List<GenericEbayOrder> readOrdersFor(int days, String defaultPhone) throws Exception {
		List<OrderType> fetchedOrders = new ArrayList<>();
		
		GetOrdersCall ordersCall = new GetOrdersCall(apiContext);			
		ordersCall.setNumberOfDays(days);
		ordersCall.setOrderRole(TradingRoleCodeType.SELLER);
		ordersCall.setOrderStatus(OrderStatusCodeType.COMPLETED);
		
		fetchedOrders.addAll(asList(ordersCall.getOrders()));
		
		Function<TransactionType, String> skuExtractor = T -> Optional.of(T)
			.map(TransactionType::getVariation)
			.map(VariationType::getSKU)
			.orElse(T.getItem().getItemID());

		return fetchedOrders.stream()
			.map(O -> new GenericEbayOrder()
				.setAddressLine1(O.getShippingAddress().getStreet1())
				.setAddressLine2(O.getShippingAddress().getStreet2())
				.setCity(O.getShippingAddress().getCityName())
				.setCountryCode(O.getShippingAddress().getCountry().value())
				.setName(O.getShippingAddress().getName())
				.setState(O.getShippingAddress().getStateOrProvince())
				.setZip(O.getShippingAddress().getPostalCode())
				.setPhone(O.getShippingAddress().getPhone(), defaultPhone)
				.setOrderID(O.getOrderID())
				.setItems(
					asList(O.getTransactionArray().getTransaction())
						.stream()
						.map(I -> new Item(skuExtractor.apply(I), I.getQuantityPurchased(), I.getTransactionPrice().getValue()))
						.collect(toList())
				)				
			).collect(toList());
	}
		
	public String updateInventoryAvailability(String itemId, boolean available) throws Exception {
		if (AppClient.getInstance().workingMode() == WorkingMode.SIMULATION) {
			LOGGER.info("Simulation mode enabled. Skipping any updates");
			return itemId;
		}
		
		if (available) {
			RelistItemCall enable = new RelistItemCall(apiContext);
			
			ItemType item = new ItemType();
			item.setItemID(itemId);
			enable.setItemToBeRelisted(item);
			enable.setWarningLevel(WarningLevelCodeType.LOW);
			enable.relistItem();
			
			return enable.getReturnedItemID();
		} else {
			EndItemCall disableCall = new EndItemCall(apiContext);
			
			disableCall.setItemID(itemId);
			disableCall.setEndingReason(EndReasonCodeType.NOT_AVAILABLE);
			disableCall.setWarningLevel(WarningLevelCodeType.LOW);

			disableCall.endItem();
			return itemId;
		}
	}

	public Map<String, String> updateInventory() {	
		if (updateQueue.isEmpty())
			return Collections.emptyMap(); 
		
		if (AppClient.getInstance().workingMode() == WorkingMode.SIMULATION) {
			LOGGER.info("Simulation mode enabled. Skipping any updates");
			return Collections.emptyMap();
		}
		
		Map<String, String> errors = new HashMap<>();		
		AtomicInteger idx = new AtomicInteger();
		
		Collection<List<InventoryStatusType>> groups = updateQueue.stream()
			.collect(groupingBy(E -> idx.getAndIncrement() / 4))
			.values();
		
		for (List<InventoryStatusType> group : groups) {
			try {
				ReviseInventoryStatusCall updateInventory = new ReviseInventoryStatusCall(apiContext);			
				InventoryStatusType[] newStatuses = group.stream().toArray(InventoryStatusType[]::new);
				
				updateInventory.setInventoryStatus(newStatuses);
				updateInventory.setWarningLevel(WarningLevelCodeType.LOW);

				InventoryStatusType[] statuses = updateInventory.reviseInventoryStatus();
				
				for (InventoryStatusType status : statuses) {
					LOGGER.debug("UPDATE RESULT: ID:%s, quantity:%s, price:%.2f", status.getItemID(), status.getQuantity(), status.getStartPrice().getValue());
				}
			} catch (Exception e) {
				LOGGER.critial(e.getMessage());				
				group.forEach(I -> errors.put(I.getItemID(), e.getMessage()));
			}	
		}
		
		updateQueue.clear();
		return errors;
	}
	
	public void addInventoryItemToUpdateQueue(String itemId, int quantity, double price) {		
		InventoryStatusType inventoryStatus = new InventoryStatusType();
		inventoryStatus.setItemID(itemId);
		inventoryStatus.setQuantity(quantity);
		
		AmountType amount = new AmountType();
		amount.setCurrencyID(CurrencyCodeType.USD);
		amount.setValue(price);
		inventoryStatus.setStartPrice(amount);
		
		updateQueue.add(inventoryStatus);		
	}	
}
