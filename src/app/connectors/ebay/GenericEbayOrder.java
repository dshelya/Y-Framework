package app.connectors.ebay;

import java.util.List;
import java.util.Objects;
import java.util.stream.Stream;

public class GenericEbayOrder {
	private int quantityOrdered;
	private String sku;
	private String name;
	private String addressLine1;
	private String addressLine2;
	private String city;
	private String unifiedState;
	private String countryCode;
	private String zip;
	private String phone;
	private String orderId;
	private String price;
	private String priceThreshold;
	private List<Item> items;

	public GenericEbayOrder() {
	}

	public GenericEbayOrder(GenericEbayOrder order) {
		this.name = order.name;
		this.addressLine1 = order.addressLine1;
		this.addressLine2 = order.addressLine2;
		this.city = order.city;
		this.unifiedState = order.unifiedState;
		this.countryCode = order.countryCode;
		this.zip = order.zip;
		this.phone = order.phone;
		this.orderId = order.orderId;
		this.price = order.price;
		this.priceThreshold = order.priceThreshold;
	}
	
	public static Stream<GenericEbayOrder> unfoldItems(GenericEbayOrder order) {
		return order.items.stream()
			.map(I -> new GenericEbayOrder(order)
				.setSku(I.getSku())
				.setQuantityOrdered(I.getQuantityOrdered())
				.setPrice(I.getPrice())
			);
	}
	
	public String getAddressLine1() {
		return addressLine1;
	}

	public String getAddressLine2() {
		return addressLine2;
	}

	public String getOrderId() {
		return orderId;
	}

	public String getCity() {
		return city;
	}

	public String getCountryCode() {
		return countryCode;
	}

	public List<Item> getItems() {
		return items;
	}

	public String getName() {
		return name;
	}

	public String getPhone() {
		return phone;
	}

	public String getPrice() {
		return price;
	}

	public String getPriceThreshold() {
		return priceThreshold;
	}

	public int getQuantityOrdered() {
		return quantityOrdered;
	}

	public String getSku() {
		return sku;
	}

	public String getUnifiedState() {
		return unifiedState;
	}

	public String getZip() {
		return zip;
	}

	public GenericEbayOrder setAddressLine1(String addressLine1) {
		this.addressLine1 = addressLine1;
		return this;
	}

	public GenericEbayOrder setAddressLine2(String addressLine2) {
		this.addressLine2 = addressLine2;
		return this;
	}

	public GenericEbayOrder setOrderID(String orderId) {
		this.orderId = orderId;
		return this;
	}

	public GenericEbayOrder setCity(String city) {
		this.city = city;
		return this;
	}

	public GenericEbayOrder setCountryCode(String countryCode) {
		this.countryCode = countryCode;
		return this;
	}

	public GenericEbayOrder setItems(List<Item> items) {
		this.items = items;
		return this;
	}

	public GenericEbayOrder setName(String name) {
		this.name = name;
		return this;
	}

	public GenericEbayOrder setPhone(String phone, String defaultPhone) {
		// Avoiding NULLs
		String notNullPhone = Objects.toString(phone);

		// Leaving only digits and taking only first 10 digits starting from the
		// end
		notNullPhone = notNullPhone.replaceAll("[^\\d]", "").replaceAll(".*(\\d{10}$)", "$1");

		// In case of empty number set the default 10-zeros otherwise leave it
		// as is
		notNullPhone = notNullPhone.isEmpty() ? defaultPhone : notNullPhone;

		this.phone = notNullPhone;
		return this;
	}

	public GenericEbayOrder setPrice(String price) {
		this.price = price;
		this.priceThreshold = Double.toString(Double.parseDouble(price) * 0.8);
		return this;
	}

	public GenericEbayOrder setQuantityOrdered(int quantityOrdered) {
		this.quantityOrdered = quantityOrdered;
		return this;
	}

	public GenericEbayOrder setSku(String sku) {
		this.sku = sku;
		return this;
	}

	public GenericEbayOrder setState(String unifiedState) {
		this.unifiedState = unifiedState.toUpperCase();
		return this;
	}

	public GenericEbayOrder setZip(String zip) {
		this.zip = zip;
		return this;
	}

	@Override
	public String toString() {
		return "ExternalOrder [name=" + name + ", orderId=" + orderId + ", sku=" + sku + "]";
	}
	
	public static class Item {
		private int quantityOrdered;
		private String price;
		private String sku;

		public Item(String sku, int quantityOrdered, double price) {
			this.sku = sku;
			this.quantityOrdered = quantityOrdered;
			this.price = Double.toString(price);
		}

		public int getQuantityOrdered() {
			return quantityOrdered;
		}

		public String getSku() {
			return sku;
		}
		
		public String getPrice() {
			return price;
		}
	}
}
