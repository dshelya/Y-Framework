package app.connectors.amazon;

public enum OfferType {
	PRIME, REGULAR, COMPETITIVE;
}