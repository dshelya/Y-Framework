package app.connectors.amazon;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Stream;

import com.amazonservices.mws.orders._2013_09_01.model.Order;
import com.amazonservices.mws.orders._2013_09_01.model.OrderItem;

public class ExternalAmazonOrder {
	public enum Filter {
		SHIPPED(O -> "Shipped".equals(O.getOrderStatus())), 
		EXCEPT_SHIPPED(SHIPPED.asFilter().negate()), 
		ANY(O -> true);

		private final Predicate<Order> predicate;

		private Filter(Predicate<Order> predicate) {
			this.predicate = predicate;
		}

		public Predicate<Order> asFilter() {
			return predicate;
		}
	}

	public static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("dd/MM/yyyy HH:mm");

	private String name;
	private String addressLine1;
	private String addressLine2;
	private String city;
	private String unifiedState;
	private String countryCode;
	private String zip;
	private String phone;
	private String amazonId;
	private String price;
	private String priceThreshold;
	private List<OrderItem> items;
	private OrderItem item;
	private String purchaseTime;
	private String orderStatus;
	
	public ExternalAmazonOrder(ExternalAmazonOrder order) {
		this.name = order.name;
		this.addressLine1 = order.addressLine1;
		this.addressLine2 = order.addressLine2;
		this.city = order.city;
		this.unifiedState = order.unifiedState;
		this.countryCode = order.countryCode;
		this.zip = order.zip;
		this.phone = order.phone;
		this.amazonId = order.amazonId;
		this.price = order.price;
		this.priceThreshold = order.priceThreshold;
		this.item = order.item;
		this.purchaseTime = order.purchaseTime;
		this.orderStatus = order.orderStatus;
	}
	
	public ExternalAmazonOrder() {
	}

	public static Stream<ExternalAmazonOrder> unfoldItems(ExternalAmazonOrder order) {
		return order.items.stream().map(I -> new ExternalAmazonOrder(order).setItem(I));
	}

	public String getName() {
		return name;
	}

	public String getAddressLine1() {
		return addressLine1;
	}

	public String getAddressLine2() {
		return addressLine2;
	}

	public String getCity() {
		return city;
	}

	public String getUnifiedState() {
		return unifiedState;
	}

	public String getCountryCode() {
		return countryCode;
	}

	public String getZip() {
		return zip;
	}

	public String getPhone() {
		return phone;
	}

	public String getAmazonId() {
		return amazonId;
	}

	public String getPrice() {
		return price;
	}

	public String getPriceThreshold() {
		return priceThreshold;
	}

	public List<OrderItem> getOrderItems() {
		return items;
	}

	public OrderItem getItem() {
		return item;
	}

	public String getPurchaseTime() {
		return purchaseTime;
	}
	
	public String getOrderStatus() {
		return orderStatus;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setAddressLine1(String addressLine1) {
		if (addressLine1.matches(".*[a-z]+.*")) {
			addressLine1 = addressLine1.replaceAll("^(\\d+)([a-zA-Z]+.*)", "$1 $2");
		}

		this.addressLine1 = addressLine1;
	}

	public void setAddressLine2(String addressLine2) {
		this.addressLine2 = addressLine2;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public void setUnifiedState(String unifiedState) {
		this.unifiedState = unifiedState.toUpperCase();
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	public void setZip(String zip) {
		this.zip = zip;
	}

	public void setPhone(String phone, String defaultPhone) {
		// Avoiding NULLs
		phone = phone + "";

		// Leaving only digits and taking only first 10 digits starting from the
		// end
		phone = phone.replaceAll("[^\\d]", "").replaceAll(".*(\\d{10}$)", "$1");

		// In case of empty number set the default 10-zeros otherwise leave it
		// as is
		phone = phone.isEmpty() ? defaultPhone : phone;

		this.phone = phone;
	}

	public void setAmazonId(String amazonId) {
		this.amazonId = amazonId;
	}

	public void setPrice(String price) {
		this.price = price;
		this.priceThreshold = Double.parseDouble(price) * 0.8 + "";
	}

	public void setOrderItems(List<OrderItem> orderItems) {
		this.items = orderItems;
	}

	public ExternalAmazonOrder setItem(OrderItem item) {
		this.item = item;
		return this;
	}

	public void setPurchaseTime(Date purchaseTime) {
		this.purchaseTime = DATE_FORMAT.format(purchaseTime);
	}

	@Override
	public String toString() {
		return "ExternalOrder [name=" + name + ", amazonId=" + amazonId + ", item=" + item.getASIN() + "]";
	}

	public void setOrderStatus(String orderStatus) {
 		this.orderStatus = orderStatus;
	}
}
