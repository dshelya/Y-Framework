package app.connectors.amazon;

import java.util.Objects;

public interface FeedMessage {
	public String entityID();
	public void setUpdateError(String error);
	public String updateError();
	public String feedType();
	public String operationType();
	public boolean isValid();
 	
	default String xmlEscape(String text) { 		
		return Objects.toString(text,"").replace("<", "&lt;")
			.replace(">", "&gt;")
			.replace("&", "&amp;");
	}
	
	default boolean isSKUValid(String sku) {
 		return sku.matches("[\\p{InBasicLatin}\\p{InLatin-1Supplement}]*");
	}
}
