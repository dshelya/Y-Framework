package app.connectors.amazon;

import static java.time.LocalDateTime.of;
import static java.time.ZoneId.systemDefault;

import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;

import com.amazonaws.mws.MarketplaceWebServiceClient;
import com.amazonaws.mws.MarketplaceWebServiceConfig;
import com.amazonservices.mws.orders._2013_09_01.MarketplaceWebServiceOrdersClient;
import com.amazonservices.mws.orders._2013_09_01.MarketplaceWebServiceOrdersConfig;
import com.amazonservices.mws.orders._2013_09_01.MarketplaceWebServiceOrdersException;
import com.amazonservices.mws.orders._2013_09_01.model.Address;
import com.amazonservices.mws.orders._2013_09_01.model.GetOrderRequest;
import com.amazonservices.mws.orders._2013_09_01.model.ListOrderItemsRequest;
import com.amazonservices.mws.orders._2013_09_01.model.ListOrderItemsResult;
import com.amazonservices.mws.orders._2013_09_01.model.ListOrdersByNextTokenRequest;
import com.amazonservices.mws.orders._2013_09_01.model.ListOrdersByNextTokenResponse;
import com.amazonservices.mws.orders._2013_09_01.model.ListOrdersRequest;
import com.amazonservices.mws.orders._2013_09_01.model.ListOrdersResult;
import com.amazonservices.mws.orders._2013_09_01.model.Order;
import com.amazonservices.mws.orders._2013_09_01.model.OrderItem;
import com.amazonservices.mws.products.MarketplaceWebServiceProductsClient;
import com.amazonservices.mws.products.MarketplaceWebServiceProductsConfig;
import com.amazonservices.mws.products.model.ASINListType;
import com.amazonservices.mws.products.model.CompetitivePriceList;
import com.amazonservices.mws.products.model.CompetitivePriceType;
import com.amazonservices.mws.products.model.CompetitivePricingType;
import com.amazonservices.mws.products.model.GetCompetitivePricingForASINRequest;
import com.amazonservices.mws.products.model.GetCompetitivePricingForASINResponse;
import com.amazonservices.mws.products.model.GetCompetitivePricingForSKURequest;
import com.amazonservices.mws.products.model.GetCompetitivePricingForSKUResponse;
import com.amazonservices.mws.products.model.GetLowestOfferListingsForASINRequest;
import com.amazonservices.mws.products.model.GetLowestOfferListingsForASINResponse;
import com.amazonservices.mws.products.model.GetLowestOfferListingsForASINResult;
import com.amazonservices.mws.products.model.GetLowestOfferListingsForSKURequest;
import com.amazonservices.mws.products.model.GetLowestOfferListingsForSKUResponse;
import com.amazonservices.mws.products.model.GetMatchingProductForIdRequest;
import com.amazonservices.mws.products.model.GetMatchingProductForIdResponse;
import com.amazonservices.mws.products.model.GetMyPriceForASINRequest;
import com.amazonservices.mws.products.model.GetMyPriceForASINResponse;
import com.amazonservices.mws.products.model.GetMyPriceForSKURequest;
import com.amazonservices.mws.products.model.GetMyPriceForSKUResponse;
import com.amazonservices.mws.products.model.IdListType;
import com.amazonservices.mws.products.model.LowestOfferListingList;
import com.amazonservices.mws.products.model.LowestOfferListingType;
import com.amazonservices.mws.products.model.OfferType;
import com.amazonservices.mws.products.model.OffersList;
import com.amazonservices.mws.products.model.Product;
import com.amazonservices.mws.products.model.SellerSKUListType;

import app.connectors.amazon.ExternalAmazonOrder.Filter;
import app.context.AppClient;
import app.context.services.ExceptionPolicy;
import app.context.services.interfaces.Context;
import app.context.services.interfaces.LoggerService;
import app.context.services.interfaces.ReturnTask;
import app.context.services.interfaces.SchedulerService;
import app.module.api.ExternalProperty;

public class AmazonConnector {
	public enum LookupType {
		ASIN, GCID, SellerSKU, UPC, EAN, ISBN, JAN
	}
	
	private static final Map<String, String> states;

	private Context context;
	private LoggerService log;
	private SchedulerService scheduler;
	
	private ExternalProperty<String> accessKey;
	private ExternalProperty<String> secretKey;
	private ExternalProperty<String> appName;
	private ExternalProperty<String> appVersion;
	private ExternalProperty<String> serviceUrl;
	private ExternalProperty<String> mwsAuthToken;
	private ExternalProperty<String> sellerId;
	private ExternalProperty<String> marketplaceId;
	
	static {
		states = new HashMap<>();
		
		states.put("AL","ALABAMA");
		states.put("AK","ALASKA");
		states.put("AB","ALBERTA");
		states.put("AS","AMERICAN SAMOA");
		states.put("AZ","ARIZONA");
		states.put("AR","ARKANSAS");
		states.put("AE","ARMED FORCES (AE)");
		states.put("AA","ARMED FORCES AMERICAS");
		states.put("AP","ARMED FORCES PACIFIC");
		states.put("BC","BRITISH COLUMBIA");
		states.put("CA","CALIFORNIA");
		states.put("CO","COLORADO");
		states.put("CT","CONNECTICUT");
		states.put("DE","DELAWARE");
		states.put("DC","DISTRICT OF COLUMBIA");
		states.put("FL","FLORIDA");
		states.put("GA","GEORGIA");
		states.put("GU","GUAM");
		states.put("HI","HAWAII");
		states.put("ID","IDAHO");
		states.put("IL","ILLINOIS");
		states.put("IN","INDIANA");
		states.put("IA","IOWA");
		states.put("KS","KANSAS");
		states.put("KY","KENTUCKY");
		states.put("LA","LOUISIANA");
		states.put("ME","MAINE");
		states.put("MB","MANITOBA");
		states.put("MD","MARYLAND");
		states.put("MA","MASSACHUSETTS");
		states.put("MI","MICHIGAN");
		states.put("MN","MINNESOTA");
		states.put("MS","MISSISSIPPI");
		states.put("MO","MISSOURI");
		states.put("MT","MONTANA");
		states.put("NE","NEBRASKA");
		states.put("NV","NEVADA");
		states.put("NB","NEW BRUNSWICK");
		states.put("NH","NEW HAMPSHIRE");
		states.put("NJ","NEW JERSEY");
		states.put("NM","NEW MEXICO");
		states.put("NY","NEW YORK");
		states.put("NF","NEWFOUNDLAND");
		states.put("NC","NORTH CAROLINA");
		states.put("ND","NORTH DAKOTA");
		states.put("NT","NORTHWEST TERRITORIES");
		states.put("NS","NOVA SCOTIA");
		states.put("NU","NUNAVUT");
		states.put("OH","OHIO");
		states.put("OK","OKLAHOMA");
		states.put("ON","ONTARIO");
		states.put("OR","OREGON");
		states.put("PA","PENNSYLVANIA");
		states.put("PE","PRINCE EDWARD ISLAND");
		states.put("PR","PUERTO RICO");
		states.put("QC","QUEBEC");
		states.put("RI","RHODE ISLAND");
		states.put("SK","SASKATCHEWAN");
		states.put("SC","SOUTH CAROLINA");
		states.put("SD","SOUTH DAKOTA");
		states.put("TN","TENNESSEE");
		states.put("TX","TEXAS");
		states.put("UT","UTAH");
		states.put("VT","VERMONT");
		states.put("VI","VIRGIN ISLANDS");
		states.put("VA","VIRGINIA");
		states.put("WA","WASHINGTON");
		states.put("WV","WEST VIRGINIA");
		states.put("WI","WISCONSIN");
		states.put("WY","WYOMING");
		states.put("YT","YUKON TERRITORY");
	}
 	
	public AmazonConnector(SchedulerService scheduler) {
		context = AppClient.getInstance();
		log = context.getLogger().tag(this.getClass());
		this.scheduler = scheduler;
		
		accessKey = new ExternalProperty<>();
		secretKey = new ExternalProperty<>();
		appName = new ExternalProperty<>();
		appVersion = new ExternalProperty<>();
		serviceUrl = new ExternalProperty<>();
		mwsAuthToken = new ExternalProperty<>();
		sellerId = new ExternalProperty<>();
		marketplaceId = new ExternalProperty<>();
	}
	
	AmazonConnector() {
 		this(AppClient.getInstance().getScheduler());
	}
	
	private MarketplaceWebServiceOrdersClient getOrderClient() {		
		MarketplaceWebServiceOrdersConfig config = new MarketplaceWebServiceOrdersConfig();
		config.setServiceURL(serviceUrl.get());

		return new MarketplaceWebServiceOrdersClient(accessKey.get(), secretKey.get(), appName.get(),appVersion.get(), config);
	}
	
	private MarketplaceWebServiceClient getFeedClient() {
		MarketplaceWebServiceConfig config = new MarketplaceWebServiceConfig();
		config.setServiceURL(serviceUrl.get());	

		return new MarketplaceWebServiceClient(accessKey.get(), secretKey.get(), appName.get(), appVersion.get(), config);
	}
	
	private MarketplaceWebServiceProductsClient getProductsClient() {
		MarketplaceWebServiceProductsConfig config = new MarketplaceWebServiceProductsConfig();
		config.setServiceURL(serviceUrl.get());	
		config.setMaxAsyncThreads(3);
 
		return new MarketplaceWebServiceProductsClient(accessKey.get(), secretKey.get(), appName.get(), appVersion.get(), config);
	}
	
	public <T extends FeedMessage> UpdateFeed<T> prepareUpdate(Class<T> model) {
		return new UpdateFeed<>(sellerId.get(), marketplaceId.get(), getFeedClient(), scheduler);
	}
	
	/** 
	 *  The rules of throttling: request quota = 6 and restore rate = 1 every minute.
	 *  In order to be in line with the throttling rules the process of
	 *  fetching orders is set up by dividing the set of all orders into 50 pcs. blocks.
	 * @param scheduledForFetch 
	 * 
	 */
	public Map<String, Order> fetchOrdersInfoFor(Set<String> scheduledForFetch) {
		final Map<String, Order> info = new HashMap<>();
		
		List<String> bunch = new ArrayList<>();	
		int left = scheduledForFetch.size();
		
		for (String amazonId : scheduledForFetch) {
			bunch.add(amazonId);
			
			if (bunch.size() == 50 || bunch.size() == left) {				
				while (true) {
					try {
						List<Order> fetched = getInfoForOrderList(bunch);						
						fetched.forEach(O -> info.put(O.getAmazonOrderId(), O));	
						
						left -= bunch.size();
						log.info("One %s-pcs bunch (out of %s) has been processed. Left: %s", bunch.size(), scheduledForFetch.size(), left);
						
						bunch.clear();						
						break;
					} catch (MarketplaceWebServiceOrdersException e) {
						if ("Request is throttled".equals(e.getMessage())) {
							log.info("Request is throttled. Waiting 1 minute to retry");

							scheduler.sleep(60000);
						} else throw e;
					}	
				}
			}
		}
		
		return info;
	}
	
	public List<OrderItem> getOrderItems(String amazonOrderId) {
		ListOrderItemsRequest request = new ListOrderItemsRequest();
		
		request.setSellerId(sellerId.get());
		request.setMWSAuthToken(mwsAuthToken.get());
		request.setAmazonOrderId(amazonOrderId);
		
		ListOrderItemsResult response;
		
		while (true) {
			try {
				response = getOrderClient().listOrderItems(request).getListOrderItemsResult();
				break;
			} catch (MarketplaceWebServiceOrdersException e) {
				if ("Request is throttled".equals(e.getMessage())) {
					log.info("Request is throttled. Waiting 1 minute to retry");

					scheduler.sleep(60000);
				} else throw e;
			}
		}

		return response.getOrderItems();
	}
	
	public List<ExternalAmazonOrder> getOrders(int ordersFetchThresholdDays, String defaultPhone, Filter filter) throws DatatypeConfigurationException {
		ListOrdersRequest request = new ListOrdersRequest();

		request.setSellerId(sellerId.get());
		request.setMWSAuthToken(mwsAuthToken.get());
		
		ZonedDateTime lowBoundDate = of(LocalDate.now().minusDays(ordersFetchThresholdDays), LocalTime.MIDNIGHT).atZone(systemDefault());
		
		//Get local time 5 minutes ago
		Instant now = Instant.now().minusSeconds(300);

		GregorianCalendar calendar = new GregorianCalendar();
		calendar.setTime(Date.from(lowBoundDate.toInstant()));
		
		request.setCreatedAfter(DatatypeFactory.newInstance().newXMLGregorianCalendar(calendar));

		calendar.setTime(Date.from(now));
		request.setCreatedBefore(DatatypeFactory.newInstance().newXMLGregorianCalendar(calendar));

		request.setOrderStatus(Arrays.asList("Unshipped", "PartiallyShipped"));
		request.setMarketplaceId(Arrays.asList(marketplaceId.get()));		
		request.setFulfillmentChannel(new ArrayList<>());		
		request.setPaymentMethod(new ArrayList<>());
		request.setMaxResultsPerPage(100);
		
		List<Order> orders = new ArrayList<>();
		ListOrdersResult response = scheduler.runUntilSuccess(
			ReturnTask.named(getOrderClient().listOrders(request)::getListOrdersResult, "Fetch amazon orders task"),
			5,
			E -> {
				if ("Request is throttled".equals(E.getMessage())) {
					log.info("Request is throttled. Waiting 1 minute to retry");
 
					scheduler.sleep(60000);
					return ExceptionPolicy.IGNORE;
				}
				
				return ExceptionPolicy.THROW.as(E);
			}
		);
		
		String token = response.getNextToken();		
		orders.addAll(response.getOrders());
				
		while (token != null) {	
			while (true) {
				try {
					ListOrdersByNextTokenResponse next = getOrderClient().listOrdersByNextToken (
						new ListOrdersByNextTokenRequest(sellerId.get(), mwsAuthToken.get(), response.getNextToken())
					);
					
					orders.addAll(next.getListOrdersByNextTokenResult().getOrders());
					token = next.getListOrdersByNextTokenResult().getNextToken();
					break;
				} catch (MarketplaceWebServiceOrdersException e) {
					if ("Request is throttled".equals(e.getMessage())) {
						log.info("Request is throttled. Waiting 1 minute to retry");

						scheduler.sleep(60000);
					} else throw e;
				}
			}
		}
		
		List<ExternalAmazonOrder> cartOrders = new ArrayList<>();			
 
		orders.stream().filter(filter.asFilter()).forEach(O -> {
			Address address = O.getShippingAddress();
			ExternalAmazonOrder cartOrder = new ExternalAmazonOrder();

			if (address != null && address.getAddressLine1() == null) {
				log.debug("Empty AddressLine1 detected. Order: %s", O.getAmazonOrderId());
			}

			cartOrder.setName(address.getName());
			cartOrder.setAddressLine1(Objects.toString(address.getAddressLine1(),""));
			cartOrder.setAddressLine2(Objects.toString(address.getAddressLine2(),""));
			cartOrder.setCity(address.getCity());
			cartOrder.setUnifiedState(states.getOrDefault(address.getStateOrRegion().toUpperCase(), address.getStateOrRegion()));
			cartOrder.setCountryCode(address.getCountryCode());
			cartOrder.setZip(address.getPostalCode());
			cartOrder.setPhone(address.getPhone(), defaultPhone);
			cartOrder.setAmazonId(O.getAmazonOrderId());
			cartOrder.setOrderItems(getOrderItems(O.getAmazonOrderId()));
			cartOrder.setOrderStatus(O.getOrderStatus());
			cartOrder.setPrice(O.getOrderTotal().getAmount());
			cartOrder.setPurchaseTime(O.getPurchaseDate().toGregorianCalendar().getTime());
			
			cartOrders.add(cartOrder);
		});

		return cartOrders;
	}

	private List<Order> getInfoForOrderList(List<String> amazonOrderIds) {
		GetOrderRequest request = new GetOrderRequest();

		request.setSellerId(sellerId.get());
		request.setMWSAuthToken(mwsAuthToken.get());
		request.setAmazonOrderId(amazonOrderIds);

		return getOrderClient().getOrder(request).getGetOrderResult().getOrders();
	}	
	
	public List<OfferType> getMyPriceForAsin(List<String> asins) {
		GetMyPriceForASINRequest request = new GetMyPriceForASINRequest();
		
		request.setMarketplaceId(marketplaceId.get());
		request.setSellerId(sellerId.get());
		request.setASINList(new ASINListType(asins));
		request.setMWSAuthToken(mwsAuthToken.get());

		 GetMyPriceForASINResponse priceResponse = scheduler.runUntilSuccess(
			ReturnTask.named(() -> getProductsClient().getMyPriceForASIN(request), "Fetch amazon competitive prices"), 
			    5, 
			    E -> {
					if ("Request is throttled".equals(E.getMessage())) {
						scheduler.sleep(30000);
						return ExceptionPolicy.IGNORE;
					}

					log.debug(E.getMessage());
					return ExceptionPolicy.THROW.as(log.getRootCause(E));
				}
	    );
 
		return priceResponse.getGetMyPriceForASINResult()
			.stream()
			.map(R -> Optional.ofNullable(R.getProduct()))
			.map(O -> O.map(Product::getOffers))
			.map(O -> O.map(OffersList::getOffer))
			.filter(Optional::isPresent)
			.flatMap(O -> O.get().stream())
			.collect(Collectors.toList());
	}
	
	public List<OfferType> getMyPriceForSku(List<String> skus) {
		GetMyPriceForSKURequest request = new GetMyPriceForSKURequest();
		
		request.setMarketplaceId(marketplaceId.get());
		request.setSellerId(sellerId.get());
		request.setSellerSKUList(new SellerSKUListType(skus));
 
		 GetMyPriceForSKUResponse priceResponse = scheduler.runUntilSuccess(
			ReturnTask.named(() -> getProductsClient().getMyPriceForSKU(request), "Fetch amazon competitive prices"), 
			    5, 
			    E -> {
					if ("Request is throttled".equals(E.getMessage())) {
 						scheduler.sleep(30000);
						return ExceptionPolicy.IGNORE;
					}

					log.debug(E.getMessage());
					return ExceptionPolicy.THROW.as(log.getRootCause(E));
				}
	    );
		 
		priceResponse.getGetMyPriceForSKUResult().stream()
			.map(R -> Optional.ofNullable(R.getError()))
			.map(O -> O.map(E -> E.getMessage()))
			.filter(Optional::isPresent)
			.map(O -> O.get())
			.forEach(log::info);
 
		return priceResponse.getGetMyPriceForSKUResult()
			.stream()
			.map(R -> Optional.ofNullable(R.getProduct()))
			.map(O -> O.map(Product::getOffers))
			.map(O -> O.map(OffersList::getOffer))
			.filter(Optional::isPresent)
			.flatMap(O -> O.get().stream())
			.collect(Collectors.toList());
	}
	
	public Map<String, List<Product>> lookupProductInfoByIds(List<String> ids, LookupType type) {
		GetMatchingProductForIdRequest request = new GetMatchingProductForIdRequest();
 		
		request.setSellerId(sellerId.get());
		request.setMarketplaceId(marketplaceId.get());
 		request.setIdList(new IdListType(ids));
		request.setIdType(type.name());
 
		GetMatchingProductForIdResponse result = scheduler.runUntilSuccess(
			ReturnTask.named(() -> getProductsClient().getMatchingProductForId(request), "Fetch amazon matching products by id"), 
			    5, 
			    E -> {
					if ("Request is throttled".equals(E.getMessage())) {
 						scheduler.sleep(30000);
						return ExceptionPolicy.IGNORE;
					}

					log.debug(E.getMessage());
					return ExceptionPolicy.THROW.as(log.getRootCause(E));
				}
	    );
		
		result.getGetMatchingProductForIdResult().stream()
			.map(R -> Optional.ofNullable(R.getError()))
			.map(O -> O.map(E -> E.getMessage()))
			.filter(Optional::isPresent)
			.map(O -> O.get())
			.forEach(log::info);		
  
		return result.getGetMatchingProductForIdResult().stream().collect(
			Collectors.toMap(K -> K.getId(), V -> V.getProducts() == null ? Arrays.asList() : V.getProducts().getProduct())
		);
	}
	
	public List<Product> getCompetitivePricingsBySku(List<String> skus) {
		GetCompetitivePricingForSKURequest request = new GetCompetitivePricingForSKURequest();
		
		request.setMarketplaceId(marketplaceId.get());
		request.setSellerId(sellerId.get());
		request.setSellerSKUList(new SellerSKUListType(skus));
		request.setMWSAuthToken(mwsAuthToken.get());

		GetCompetitivePricingForSKUResponse pricesResponse = scheduler.runUntilSuccess(
			ReturnTask.named(() -> getProductsClient().getCompetitivePricingForSKU(request), "Fetch amazon competitive prices"), 
			    5, 
			    E -> {
					if ("Request is throttled".equals(E.getMessage())) {
 						scheduler.sleep(30000);
						return ExceptionPolicy.IGNORE;
					}

					log.debug(E.getMessage());
					return ExceptionPolicy.THROW.as(log.getRootCause(E));
				}
	    );
		
		return pricesResponse.getGetCompetitivePricingForSKUResult()
			.stream()
			.map(R -> Optional.ofNullable(R.getProduct()))			
			.filter(Optional::isPresent)
			.map(Optional::get)

			.collect(Collectors.toList());
	}
	
	public List<CompetitivePriceType> getCompetitivePricingsByAsin(List<String> asins) {
		GetCompetitivePricingForASINRequest request = new GetCompetitivePricingForASINRequest();
		
		request.setMarketplaceId(marketplaceId.get());
		request.setSellerId(sellerId.get());
		request.setASINList(new ASINListType(asins));
		
		GetCompetitivePricingForASINResponse pricesResponse = scheduler.runUntilSuccess(
			ReturnTask.named(() -> getProductsClient().getCompetitivePricingForASIN(request), "Fetch amazon competitive prices"), 
			    5, 
			    E -> {
					if ("Request is throttled".equals(E.getMessage())) {
 						scheduler.sleep(30000);
						return ExceptionPolicy.IGNORE;
					}

					return ExceptionPolicy.THROW.as(E);
				}
	    );
		
		return pricesResponse.getGetCompetitivePricingForASINResult()
			.stream()
			.map(R -> Optional.ofNullable(R.getProduct()))
			.map(O -> O.map(Product::getCompetitivePricing))
			.map(O -> O.map(CompetitivePricingType::getCompetitivePrices))
			.map(O -> O.map(CompetitivePriceList::getCompetitivePrice))				
			.filter(Optional::isPresent)
			.flatMap(O -> O.get().stream())
			.collect(Collectors.toList());
	}
	
	public List<GetLowestOfferListingsForASINResult> getProductListingsByAsins(List<String> asins) {
		GetLowestOfferListingsForASINRequest request = new GetLowestOfferListingsForASINRequest()		
			.withSellerId(sellerId.get())
			.withMWSAuthToken(mwsAuthToken.get())
			.withMarketplaceId(marketplaceId.get())
			.withASINList(new ASINListType(asins))
			.withExcludeMe(true);
		
		GetLowestOfferListingsForASINResponse response = scheduler.runUntilSuccess(
			ReturnTask.named(() -> getProductsClient().getLowestOfferListingsForASIN(request), "Fetch amazon ASIN info"), 
			    5, 
			    E -> {
					if ("Request is throttled".equals(E.getMessage())) {
 						scheduler.sleep(30000);
						return ExceptionPolicy.IGNORE;
					}

					return ExceptionPolicy.THROW.as(E);
				}
	    );
 		
		return response.getGetLowestOfferListingsForASINResult();
	}
	
	public List<LowestOfferListingType> getLowestOfferListingsByAsin(List<String> asins) {
		return getProductListingsByAsins(asins).stream()
			.map(GetLowestOfferListingsForASINResult::getProduct)
	 		.filter(Objects::nonNull)
 			.map(Product::getLowestOfferListings)
			.map(LowestOfferListingList::getLowestOfferListing)
			.flatMap(List::stream)
			.collect(Collectors.toList());
	}
	
	public List<Product> getLowestOfferListingsBySku(List<String> skus, boolean excludeMe) {
 		GetLowestOfferListingsForSKURequest request = new GetLowestOfferListingsForSKURequest()		
			.withSellerId(sellerId.get())
			.withMWSAuthToken(mwsAuthToken.get())
			.withMarketplaceId(marketplaceId.get())
			.withSellerSKUList(new SellerSKUListType(skus))
			.withExcludeMe(excludeMe);
		
		GetLowestOfferListingsForSKUResponse response = scheduler.runUntilSuccess(
			ReturnTask.named(() -> getProductsClient().getLowestOfferListingsForSKU(request), "Fetch amazon ASIN info"), 
			    5, 
			    E -> {
					if ("Request is throttled".equals(E.getMessage())) {
						//log.info("Request is throttled, waiting 30 seconds to retry");
						scheduler.sleep(30000);
						return ExceptionPolicy.IGNORE;
					}

					return ExceptionPolicy.THROW.as(E);
				}
	    );		
		
		return response.getGetLowestOfferListingsForSKUResult()
			.stream()
			.map(R -> Optional.ofNullable(R.getProduct()))
			.filter(Optional::isPresent)
			.map(Optional::get)
			.collect(Collectors.toList());
	}

	
	public void bindAccessKey(ExternalProperty<String> mwsAccessKey) {
		this.accessKey.bind(mwsAccessKey);
	}

	public void bindSecretKey(ExternalProperty<String> mwsSecretKey) {
		this.secretKey.bind(mwsSecretKey);		
	}

	public void bindAppName(ExternalProperty<String> mwsAppName) {
		this.appName.bind(mwsAppName);
	}

	public void bindAppVersion(ExternalProperty<String> mwsAppVersion) {
		this.appVersion.bind(mwsAppVersion);
	}

	public void bindSellerId(ExternalProperty<String> mwsSellerId) {
		this.sellerId.bind(mwsSellerId);
	}

	public void bindAuthToken(ExternalProperty<String> mwsAuthToken) {
		this.mwsAuthToken.bind(mwsAuthToken);
	}

	public void bindMarketplaceId(ExternalProperty<String> mwsMarketplaceId) {
		this.marketplaceId.bind(mwsMarketplaceId);
	}

	public void bindServiceUrl(ExternalProperty<String> mwsServiceUrl) {
		this.serviceUrl.bind(mwsServiceUrl);
	}

	public void logCredentials() {
		log.info("accessKey: " + accessKey.get());
		log.info("sellerId: " + sellerId.get());
		log.info("marketplaceId: " + marketplaceId.get());
		log.info("serviceUrl: " + serviceUrl.get());
	}

	public SchedulerService getScheduler() {
		return scheduler;		
	}
}
