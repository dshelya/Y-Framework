package app.connectors.amazon;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.security.DigestInputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.Base64;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import com.amazonaws.mws.MarketplaceWebServiceClient;
import com.amazonaws.mws.model.FeedSubmissionInfo;
import com.amazonaws.mws.model.GetFeedSubmissionResultRequest;
import com.amazonaws.mws.model.IdList;
import com.amazonaws.mws.model.SubmitFeedRequest;
import com.amazonaws.mws.model.SubmitFeedResponse;

import app.context.AppClient;
import app.context.services.interfaces.ConfigService;
import app.context.services.interfaces.Context;
import app.context.services.interfaces.Context.WorkingMode;
import app.context.services.interfaces.LoggerService;
import app.context.services.interfaces.SchedulerService;
import app.module.api.ModuleException;

public class UpdateFeed<T extends FeedMessage > {
	private static final DateTimeFormatter DATE_TIME = DateTimeFormatter.ofPattern("yyyy-MM-dd'_'HH-mm");

	private static final String ENVELOPE = 
		"<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" + 
		"<AmazonEnvelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"" + 
		"	xsi:noNamespaceSchemaLocation=\"amzn-envelope.xsd\">\n" + 
		"	<Header>\n" + 
		"		<DocumentVersion>1.01</DocumentVersion>\n" + 
		"		<MerchantIdentifier>%s</MerchantIdentifier>\n" + 
		"	</Header>\n" + 
		"	<MessageType>%s</MessageType>\n" + 
		"	%s" + 
		"</AmazonEnvelope>";
	
	private static final String MESSAGE = "<Message>\n" + "  <MessageID>%s</MessageID>\n" + "  %s" + "</Message>\n";

	private Set<T> values = new HashSet<>();

 	private String feed;
	private String merchantId;
	private String marketplaceId;
	
	private MarketplaceWebServiceClient webClient;

	private Context context;
	private LoggerService log;
	private SchedulerService scheduler;
	private ConfigService config;

	private boolean waitForResults;
	
	UpdateFeed(String merchantId, String marketplaceId, MarketplaceWebServiceClient webClient, SchedulerService scheduler) {
 		this.merchantId = merchantId;
 		this.marketplaceId = marketplaceId;
 		
		this.feed = String.format(ENVELOPE, merchantId, "%s", "%s");
		this.webClient = webClient;
		
		this.context = AppClient.getInstance();		
		this.log = context.getLogger().tag(getClass());
		this.scheduler = scheduler;
		this.config = context.getConfig();
		
		this.waitForResults = true;
	}
 
	public void push(T data) {
		values.add(data);
	}
	
	public boolean submit() {
		try {
			log.info("Publishing feed");

			if (this.values.isEmpty()) {
				log.info("No messages found for publishing. Skipping..");
				return false;
			}
				
			SubmitFeedRequest request = new SubmitFeedRequest();
			
			request.setMerchant(merchantId);
			request.setMarketplaceIdList(new IdList(Arrays.asList(marketplaceId)));
			request.setFeedType(values.stream().map(T -> T.feedType()).findFirst().orElse("")); 
			
			File file = dumpFeed();
			
			FileInputStream stream = new FileInputStream(file);
			request.setContentMD5(hash(stream));        
			request.setFeedContent(stream);
			
			if (file.length() <= 0 || context.workingMode() == WorkingMode.SIMULATION)
				return false;
			
			SubmitFeedResponse response = webClient.submitFeed(request);     
			
			if (!response.isSetSubmitFeedResult()) {
				stream.close();
				throw new ModuleException("Error submiting feed. Null response");
			}
			
			FeedSubmissionInfo result = response.getSubmitFeedResult().getFeedSubmissionInfo();
				
			if (!result.isSetFeedProcessingStatus() || !"_SUBMITTED_".equals(result.getFeedProcessingStatus())) {
				stream.close();
				throw new ModuleException("Error submiting feed. Illegal return status");
			}	
			
			log.debug("FEED SUBMISSION ID: %s", result.getFeedSubmissionId());
			stream.close();
			
			processFeedResults(file.getName(), result.getFeedSubmissionId());  
			return true;
		} catch (Exception e) {
			log.info("Error submiting feed data: %s", e.getMessage());
			log.debug(e);
		}
		
		return false;		
	}
	
	public void processFeedResults(String parent, String feedId) throws Exception {
		if (!waitForResults)
			return;
		
		log.debug("Fetching feed [%s] results", feedId);
		File resultFile = config.getPathFor("feeds\\results\\Feed_result_for_"+parent+"("+feedId+").xml");		

		log.debug("Feed results is not yet ready. Waiting 1 minute to retry");
		scheduler.sleep(60000);
		
		while (true) {
			try (OutputStream stream = new FileOutputStream(resultFile)) {
				GetFeedSubmissionResultRequest requestResult = new GetFeedSubmissionResultRequest();
				requestResult.setFeedSubmissionId(feedId);

				requestResult.setFeedSubmissionResultOutputStream(stream);
				requestResult.setMerchant(merchantId);

				webClient.getFeedSubmissionResult(requestResult).getGetFeedSubmissionResultResult();
			} catch (Exception e) {
				if (e.getMessage().contains("Feed Submission Result is not ready")) {
					log.debug("Feed results is not yet ready. Waiting 1 minute to retry");
					scheduler.sleep(60000);
					
					continue;
				}
				
				log.debug("Error fetching feed results: %s", log.getRootCause(e).getMessage());
			}
			
			try {
				Document result = Jsoup.parse(resultFile, "UTF-8");
				String status = result.select("processingreport statuscode").text();
				
				if (!"Complete".equals(status)) {
					log.debug("Feed results is not yet ready. Waiting 1 minute to retry");
					scheduler.sleep(60000);
					continue;
				}	
				
				result.select("ProcessingReport Result").forEach( R -> {
					if ("Error".equals(R.select("ResultCode").text())) {
						String amazonID = R.select("AmazonOrderID").text();
						String errorDesc = R.select("ResultDescription").text();
						
						log.debug("Amazon failed to update order %s. Error: %s", amazonID, errorDesc);						
						values.stream().filter(M -> M.entityID().equals(amazonID) || amazonID.isEmpty()).forEach(M -> M.setUpdateError(errorDesc));
					}					
				});

				log.debug("Feed processing finished");
				break;
			} catch (Exception e) {
				log.debug("Error processing feed [%s] results: %s", feedId, log.getRootCause(e).getMessage());
			} 
		}        
	}
	
	private File dumpFeed() throws IOException {
		String type = values.stream().findFirst().get().getClass().getSimpleName();
		File feedFile = config.getPathFor("feeds\\update_"+type+"_"+DATE_TIME.format(LocalDateTime.now())+".xml");
		
		if (feedFile.exists())
			feedFile.delete();
		
		feedFile.createNewFile();
		
		StringBuilder messages = new StringBuilder();
		AtomicInteger id = new AtomicInteger(1);

		for (T message : values) {
			if (message.isValid())
				messages.append(String.format(MESSAGE, id.getAndIncrement(), message));
			else {
				log.debug("Incorrect feed message found:\n%s", message);
			}
		}
		
		String feed = String.format(this.feed, values.stream().map(T -> T.operationType()).findFirst().orElse(""), messages);		
		
		FileOutputStream stream = new FileOutputStream(feedFile);
		stream.write(feed.getBytes("UTF-8"));
		stream.close();
		
		return feedFile;
	}

	public static String hash(FileInputStream fis) throws IOException, NoSuchAlgorithmException {
		DigestInputStream dis = new DigestInputStream(fis, MessageDigest.getInstance("MD5"));
		byte[] buffer = new byte[30485760];

		while (dis.read(buffer) > 0);

		String hash = new String(Base64.getEncoder().encode(dis.getMessageDigest().digest()));
		fis.getChannel().position(0);

		return hash;
	}

	public Set<T> feedMessages() {
		return values;
	}

	public UpdateFeed<T> waitForResults(boolean waitForResults) {
		this.waitForResults = waitForResults;
 		return this;
	}
}