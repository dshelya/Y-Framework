package app.crawler.api;

import java.util.Optional;

import org.jsoup.nodes.Document;


@SuppressWarnings("serial")
public class WebException extends Exception {	
	private final transient Optional<Document> html;

	public WebException(String message) {
		super(message);
		html = Optional.empty();
	}

	public WebException(Exception exception, Document document) {
		super(exception);
		html = Optional.of(document);
	}

	public WebException(String message, Document document) {
		super(message);
		html = Optional.of(document);
	}

	public Optional<Document> getLastAccessedPage() {
		return html;
	}
}
