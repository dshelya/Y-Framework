package app.crawler.api;

import java.util.Base64;

public class ProxyAuthenticator {
	private static final String HEADER = "Proxy-Authorization";
	public static final ProxyAuthenticator DEFAULT = new ProxyAuthenticator("user", "pass");

	private String authData;
	private char[] password;
	private String user;

	public ProxyAuthenticator(String user, String password) {
		String source = user + ":" + password;		
		this.authData = "Basic " + Base64.getEncoder().encodeToString(source.getBytes());
		
		this.user = user;
		this.password = password.toCharArray();
	}

	public String getAuthHeader() {
		return HEADER;
	}
	
	public String getAuthData() {
		return authData;
	}

	public char[] getPass() {
		return password;
	}

	public String getUser() {
		return user;
	}
}
