package app.crawler.api;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.Authenticator;
import java.net.PasswordAuthentication;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Consumer;
import java.util.function.Function;

import org.jsoup.Connection;
import org.jsoup.Connection.Method;
import org.jsoup.Connection.Response;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import app.context.AppClient;
import app.context.services.ExceptionPolicy;
import app.context.services.interfaces.LoggerService;
import app.context.services.interfaces.SchedulerService;
import app.context.services.interfaces.Task;
import app.context.services.interfaces.WebViewService;
import app.util.captcha.AnticaptchaService;
 
public final class WebCrawler {
	//private static final Map<String, ProxyAuthenticator> PROXIES = new HashMap<>();
	private static final LoggerService log = AppClient.getInstance().getLogger().tag(WebCrawler.class);

	private static Task GLOBAL_AUTH_HANDLER = () -> {};
	private static Consumer<Map<String, String>> GLOBAL_COOKIE_STORAGE = C -> {};		
	private static Function<String, Boolean> GLOBAL_UNAUTHORIZED_DETECTOR = D -> false;;
	
	private final Map<String, String> slidingCookies = new HashMap<String, String>();
	private final Map<String, String> staticCookies = new HashMap<String, String>();
	
	//private final Queue<Function<Document, Document>> processors = new LinkedList<>();

	private boolean dragCookies;
	private boolean throughProxy;
	private boolean retryOnFail;
	private int retries;

	private Connection client;
	private String host;
	private ProxyHolder proxyHolder;

	private Document lastAccessedDocument;

	private Function<Document, Optional<RuntimeException>> validator;
	private final SchedulerService scheduler;

	private Map<String, String> staticHeaders;	
	
	static {
		System.setProperty("jdk.http.auth.tunneling.disabledSchemes", ""); 		
	}
	
	private WebCrawler(SchedulerService scheduler){
		this.scheduler = scheduler;
		this.staticHeaders = new HashMap<>();
		
		dragCookies = true;
		retryOnFail = true;
		retries = 5;
	};

	public static WebCrawler getCrawler(SchedulerService scheduler) {
		return new WebCrawler(scheduler);
	}	

	public WebCrawler crawl(String link) {
		return createClient(link);
	}
	
	public Document lastAccessedDocument() {
		return lastAccessedDocument;
	}
	
	public WebCrawler errorIf(Function<Document, Optional<RuntimeException>> validator) {	
		this.validator = validator;
		return this;
	}
	
	//public WebCrawler loadAndThen(Function<Document, Document> processor) {
	////	processors.add(processor);
	//	return this;
	//}

	public Document loadAndProcess(String formQuery) throws Exception {			
		return loadAndProcess(formQuery, null);
	}
	
	public WebCrawler setHost(String host) {
		this.host = host;
		return this;
	}
	
	public WebCrawler setRetriesNumber(int retries) {
		this.retries = retries;
		return this;
	}
	
	public WebCrawler retryOnFail(boolean retryOnFail) {
		this.retryOnFail = retryOnFail;
		return this;
	}
	
	public Document processForm(Document page, String formQuery) throws Exception {
		return processForm(page, formQuery, null);	
	}
	
	public Document processForm(Document page, String formQuery, Map<String, String> formParams) throws Exception {
		Objects.requireNonNull(host, "Host is not set, the form processor cannot complete action URL");
		
		Elements form = page.select(formQuery);		
		String submitUrl = form.attr("action");
				
		Map<String, String> formData = form.select(
				"textarea[name], " +
				"select[name], " +
				"input[name][type=submit], " +
				"input[name][type=image], " +
				"input[name][type=text], " +
				"input[name][type=hidden], " +
				"input[name][type=password], " +
				"input[name][type=radio][checked], " +
				"input[name][type=checkbox][checked]"
			).stream().collect(HashMap::new, (M, E) -> M.put(E.attr("name"), E.val()), HashMap::putAll);			
		
		if (formParams != null) {
			formParams.forEach((K,V) -> {
				if (formData.containsKey(K)) {
					formData.put(K, V);
				}
			});
		}
		
		//logger.dumpAndOpenFile(page, "page.htm");
		
		return createClient(submitUrl.startsWith("/") ? host + submitUrl : submitUrl).params(formData).post();	
	}
	
	public Document loadAndProcess(String formQuery, Map<String, String> formParams) throws Exception {		
		return processForm(execute(Method.GET, true), formQuery, formParams);		
	}
	
	public Document post() throws Exception {
		return execute(Method.POST, true);
	}
	
	public Document get() throws Exception {		
		return execute(Method.GET, true);
	}
	
	public Document post(boolean detectAuthIssues) throws Exception {
		return execute(Method.POST, detectAuthIssues);
	}
	
	public Document get(boolean detectAuthIssues) throws Exception {		
		return execute(Method.GET, detectAuthIssues);
	}
	
	private Document execute(Method method, boolean detectAuthIssues) throws Exception {	
		if (throughProxy) {
			Authenticator.setDefault(new Authenticator() {
				public PasswordAuthentication getPasswordAuthentication() {	 
					return new PasswordAuthentication(proxyHolder.user(), proxyHolder.password().toCharArray());
				}
			});
		}
		
		Response response = scheduler.runUntilSuccess(() -> client.method(method)
			.cookies(dragCookies ? slidingCookies : staticCookies)
			.execute(), retryOnFail ? retries : 0, E -> {
				
				//if (!retryOnFail)
				//	
				log.debug("Got error: %s", E);
				scheduler.sleep(5000);
				return ExceptionPolicy.THROW.as(E);
			}
		);	
		
		
		if (throughProxy && response.statusCode() == 407) {
			throw new Exception("Proxy connection error. Authentication failed");
		}
		
		lastAccessedDocument = response.parse();
		
		if (validator!=null) {
			try {
				validator.apply(lastAccessedDocument).ifPresent(X -> {throw X;});
			} finally {
				validator = null;
			}
		}
		
		if (detectAuthIssues && GLOBAL_UNAUTHORIZED_DETECTOR.apply(lastAccessedDocument.html())) {
			GLOBAL_AUTH_HANDLER.run();
			
		//	if (detectAuthIssues && detector.apply(document.html())) 
		//		authHandler.run();				
				
			return execute(method, false);
		}	
		
		if (dragCookies) {
			slidingCookies.putAll(response.cookies());	
			GLOBAL_COOKIE_STORAGE.accept(slidingCookies);
		}
		
		return response.parse();
	}
	
	public WebCrawler passCookiesBetweenRequets(boolean drag) {
		this.dragCookies = drag;
		return this;
	}
	
	public void dropDraggedCookies() {
		slidingCookies.clear();
	}
	
	public WebCrawler params(Map<String,String> params) {
		client.data(params);
		return this;
	}
	
	public WebCrawler param(String key, String value) {
		client.data(key, value);
		return this;
	}
	
	public WebCrawler staticHeader(String key, String value) {
		staticHeaders.put(key, value);
		return this;
	}
	
	public WebCrawler clearHeaders() {
		staticHeaders.clear();
		return this;
	}

	private WebCrawler createClient(String url) {
		this.client = Jsoup.connect(url)
			.userAgent(WebViewService.getUserAgent())
			.timeout(30000)
			.ignoreContentType(true)
			.validateTLSCertificates(false)
			.ignoreHttpErrors(true)
			.headers(staticHeaders);
		
		applyProxy();		
		return this;
	}
	
	private void applyProxy() {
		if (throughProxy) {
			if (proxyHolder.isReqiredAuth()) {
				client.header(proxyHolder.getAuthenticator().getAuthHeader(), proxyHolder.getAuthenticator().getAuthData());
 			}
			
			client.proxy(proxyHolder.getProxy());		
		}
	}
	
	public WebCrawler throughProxy(ProxyHolder proxyHolder) {
		this.proxyHolder = proxyHolder;	
		this.throughProxy = true;	
		
		return this;
	}
	
	public WebCrawler staticCookies(Map<String, String> cookies) {	
		staticCookies.putAll(cookies);
		slidingCookies.putAll(cookies);
		
		return this;
	}

	//public void setUnauthorizedHandler(Function<String, Boolean> detector, Task authHandler) {		
	//	this.authHandler = authHandler;
	//	this.detector = detector;
	//}

	public static void setGlobalUnauthorizedHandler(Function<String, Boolean> detector, Task authHandler) {
		GLOBAL_AUTH_HANDLER = authHandler;
		GLOBAL_UNAUTHORIZED_DETECTOR = detector;
	}
	
	public static void setCookiesPersistHandler(Consumer<Map<String, String>> storage) {
		GLOBAL_COOKIE_STORAGE = storage;
	}

	public String solveCaptcha(String link, String anticaptchaKey) {		
		return scheduler.backgroundRun(() -> {
			String decodedCaptcha = null;

			while (decodedCaptcha == null) {
				try (				
					InputStream captchaStream = new URL(link).openStream();
				){
					decodedCaptcha = AnticaptchaService.getInStance().loadProvider("antigate", anticaptchaKey).orElseThrow(() -> {
						return new Exception("Unable to find active anticaptcha profider");
					}).recognize(captchaStream);
				} catch (SocketTimeoutException e) {
						log.debug("Captcha solve timeout. Retry in 30 seconds");
						scheduler.sleep(30000);
				} catch (Exception e) {
					if (e.getMessage().contains("busy")) {
						log.debug("Service is busy. Waiting 30 seconds to retry");
						scheduler.sleep(30000);
					} else
						throw new RuntimeException(e);
				}
			}
			
			return decodedCaptcha;
		});
	}

	public File fetchFile(String downloadLink, String fileNamePrifix) throws IOException {		
		Response file = crawl(downloadLink).client.cookies(dragCookies ? slidingCookies : staticCookies).execute();
			
		String filename = file.header("Content-Disposition").replaceAll(".*filename=(.*)$", "$1");		
		File filePath = AppClient.getInstance().getConfig().getPathFor("buyer reports\\" + fileNamePrifix + "_" + filename);		
		
		FileOutputStream out = new FileOutputStream(filePath);
		out.write(file.bodyAsBytes());		
		out.close();
		
		return filePath;		
	}	
}
