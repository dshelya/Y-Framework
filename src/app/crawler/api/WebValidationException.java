package app.crawler.api;

@SuppressWarnings("serial")
public class WebValidationException extends Exception {

	public WebValidationException(String string) {
		super(string);
	}

}
