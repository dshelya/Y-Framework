package app.crawler.api;

import org.jsoup.nodes.Document;


@SuppressWarnings("serial")
public class WebRuntimeException extends RuntimeException {	
	private Document html;

	public WebRuntimeException(String message) {
		super(message);
	}

	public WebRuntimeException(Exception exception) {
		super(exception);
	}

	public Document getHtml() {
		return html;
	}

	@SuppressWarnings("unchecked")
	public <T extends WebRuntimeException> T bindHtml(Document lastHTML) {
		this.html = lastHTML;
		return (T) this;
	}
}
