package app.crawler.api;

import java.net.InetSocketAddress;
import java.net.Proxy;
import java.util.Objects;

public class ProxyHolder {
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((authenticator == null) ? 0 : authenticator.hashCode());
		result = prime * result + ((proxy == null) ? 0 : proxy.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		
		if (obj == null)
			return false;
		
		if (getClass() != obj.getClass())
			return false;
		
		ProxyHolder other = (ProxyHolder) obj;
		
		if (authenticator == null) {
			if (other.authenticator != null)
				return false;
		} 
		
		if (proxy == null) {
			if (other.proxy != null)
				return false;			
		} else if (!proxy.equals(other.proxy))
			return false;
		return true;
	}

	private Proxy proxy;
	private ProxyAuthenticator authenticator;
	private String proxyKey;
	
	private boolean requiredAuth;
	
	private ProxyHolder() {
		authenticator = new ProxyAuthenticator("", "");
	}

	public static ProxyHolder valueOf(String ip, Integer port, String user, String password, Proxy.Type type) {
		ProxyHolder instance = new ProxyHolder();

		if (port == null)
			throw new IllegalArgumentException("Invalid proxy address port");
		
		instance.proxy = new Proxy(type, new InetSocketAddress(ip, port));
		
		if (user != null && !user.isEmpty()) {
			instance.authenticator = new ProxyAuthenticator(user, password);
			instance.requiredAuth = true;
		}
		
		instance.proxyKey = ip + ":" + port;
		
		return instance;
	}
	
	public static ProxyHolder valueOf(String proxy, Proxy.Type type) {
 		String[] part = Objects.toString(proxy, "").split(":");
 		
 		if (type == null)
 			throw new IllegalArgumentException("Proxy type is not selected");

		if (part.length < 2)
			throw new IllegalArgumentException("Invalid proxy address value");		

		if (part.length == 4) {
			return valueOf(part[0], Integer.valueOf(part[1]), part[2], part[3], type);
		}
		
		return valueOf(part[0], Integer.valueOf(part[1]), null, null, type);
	}

	public Proxy getProxy() {
		return proxy;
	}

	public ProxyAuthenticator getAuthenticator() {
		return authenticator;
	}

	public boolean isReqiredAuth() {
		return requiredAuth;
	}

	public String getProxyKey() {
		return proxyKey;
	}

	public String type() {
 		return proxy.type().toString();
	}

	public String ip() {
 		return ((InetSocketAddress)proxy.address()).getHostString();
	}

	public String port() {
 		return String.valueOf(((InetSocketAddress)proxy.address()).getPort());
	}

	public String user() {
		return authenticator == null ? "-" : authenticator.getUser();
	}

	public String password() {		
		return authenticator == null ? "-" : String.valueOf(authenticator.getPass());
	}
}
