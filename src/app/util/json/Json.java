package app.util.json;

import java.util.Objects;

import javax.script.Invocable;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;

public class Json {
	private static final ScriptEngine engine;
	private static final String JS = "function parse(json) { return convert(JSON.parse(json)); }" + 
			"" + 
			"function convert(object) {" + 
			"    if (isObject(object)) return toHashMap(object);" + 
			"    if (isArray(object)) return toArray(object);" + 
			"    return object;" + 
			"}" + 
			"" + 
			"function toHashMap(object) {" + 
			"    var map = new java.util.HashMap();" + 
			"    for (key in object) map.put(key, convert(object[key]));" + 
			"    return map;" + 
			"}" + 
			"" + 
			"function toArray(object) {" + 
			"    var array = Java.to(object, \"java.lang.Object[]\");" + 
			"    for (var index = 0, len = array.length; index < len; index++) array[index] = convert(array[index]);" + 
			"    return array;" + 
			"}" + 
			"" + 
			"function isObject(object) { return Object.prototype.toString.call(object) === \"[object Object]\"; }" + 
			"function isArray(object) { return Object.prototype.toString.call(object) === \"[object Array]\"; }";
	
	
	static {
		engine = new ScriptEngineManager().getEngineByName("nashorn");
		
		try {			
			engine.eval(JS);
		} catch (Exception x) {
			throw new RuntimeException(x);
		}
	}
	
	public static JsonObject make() {
		return JsonObject.make();
	}

	private Json() {
	}

	public static JsonObject build(String source) {
		try {
			if (Objects.toString(source, "").isEmpty())
				return make();    
			
			//Plain string
			if (source.matches("^\".*?\"$")) {
				source = "{\"value\":" + source + "}";
			}

			return JsonObject.make(((Invocable) engine).invokeFunction("parse", source));
		} catch (Exception x) {
			return make().add("error", x.getMessage());
		}
	}

	public static void main(String[] args) {
		String source = "{\"result\":{\"id\":\"20133a550524492067a38ea7daf5dda4fe90d544681afe6647ed4a8cae60a5d5\",\"username\":\"teriyaky\",\"captcha_link\":\"https:\\/\\/textnow.com\\/recaptcha?token=MTg2YmI4NjVkNDI0MDk2YTE4MjY0MDIwN2VhNmVhYWVDDRWkby04RYZbgTYSECk5lh%2B4qQiif6etuaNp3CLcOQ%3D%3D\\u0026client_type=TN_ANDROID\"},\"error_code\":\"CAPTCHA_REQUIRED\"}";

		System.out.println(build(source).asString("result.captcha_link"));
		//System.out.println(new ScriptEngineManager().getEngineByName("JavaScript"));

	}
}