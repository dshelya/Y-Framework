package app.util.json;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class JsonArray implements JsonElement {
	private final List<Object> HOLDER;
	
	public static JsonArray make() {
		return new JsonArray();
	}
	
	public static JsonArray make(Object source) {		
		if (source instanceof Object[]) {			
			return make().put((Object[]) source);
		} else {
			throw new IllegalArgumentException("Cannot represent value as JSON object");
		}
	}

	private JsonArray() {
		HOLDER = new ArrayList<>();
	}

	@Override
	public String toString() {
		return toJson();
	}
	
	private String mapper(Object data) {
		return data instanceof String ? "\""+data+"\"" : Objects.toString(data, null);
	}

	public String toJson() {
		return "[" + HOLDER.stream().map(this::mapper).collect(Collectors.joining(",")) + "]";
	}
	private JsonArray put(Object[] value) {
		HOLDER.addAll(Arrays.asList(value));
		return this;
	}

	public JsonArray addNull(JsonElement ... value) {
		put(value);
		return this;
	}

	public JsonArray add(JsonElement ... value) {
		put(value);
		return this;
	}

	public JsonArray add(Long ... value) {
		put(value);
		return this;
	}

	public JsonArray add(Double ... value) {
		put(value);
		return this;
	}

	public JsonArray add(String ... value) {
		put(value);
		return this;
	}

	public JsonArray add(Boolean ... value) {
		put(value);
		return this;
	}
	
	public int size() {
		return HOLDER.size();
	}
	
	public String asString(int index) {
		return HOLDER.get(index).toString();
	}
	
	public boolean asBoolean(int index) {
		return (Boolean) HOLDER.get(index);
	}
	
	public Object asLong(int index) {
		return (Long) HOLDER.get(index);
	}
	
	public Double asDouble(int index) {
		return (Double) HOLDER.get(index);
	}
	
	public JsonArray asArray(int index) {
		return (JsonArray) HOLDER.get(index);
	}
	
	public JsonObject asObject(int index) {
		return JsonObject.make(HOLDER.get(index));
	}




}