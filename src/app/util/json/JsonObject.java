package app.util.json;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.stream.Collectors;

public class JsonObject implements JsonElement {
	private final LinkedHashMap<String, Object> holder;

	public static JsonObject make() {
		return new JsonObject();
	}
	
	public static JsonObject make(Object data) {
		return new JsonObject(data);
	}

	private JsonObject() {
		holder = new LinkedHashMap<>();
	}
	
	@SuppressWarnings("unchecked") 
	private JsonObject(Object data) {
		this();
		
		if (data instanceof Map<?, ?>) {			
			Map<String, Object> map =  (Map<String, Object>) data;
			
			for (Entry<String, Object> entry : map.entrySet()) {
				if (entry.getValue() instanceof Object[]) {
					holder.put(entry.getKey(), JsonArray.make(entry.getValue()));
				} else {
					holder.put(entry.getKey(), entry.getValue());
				}
			}
		} else {
			throw new IllegalArgumentException("Cannot represent value as JSON object");
		}
	}

	@Override
	public String toString() {
		return toJson();
	}
	
	private String mapper(Entry<String, Object> data) {
		Object val = data.getValue();		
		return "\""+data.getKey() + "\":" + (val instanceof String ? "\""+val+"\"" : Objects.toString(val, null));
	}

	public String toJson() {
		return "{" + holder.entrySet().stream().map(this::mapper).collect(Collectors.joining(","))	+ "}";
	}

	private JsonObject put(String key, Object value) {
		holder.put(key, value);
		return this;
	}

	public JsonObject addNull(String key, JsonElement value) {
		put(key, value);
		return this;
	}

	public JsonObject add(String key, JsonElement value) {
		put(key, value);
		return this;
	}

	public JsonObject add(String key, Long value) {
		put(key, value);
		return this;
	}
	
	public JsonObject add(String key, Integer value) {
		put(key, value);
		return this;
	}

	public JsonObject add(String key, Double value) {
		put(key, value);
		return this;
	}

	public JsonObject add(String key, String value) {
		put(key, value);
		return this;
	}

	public JsonObject add(String key, Boolean value) {
		put(key, value);
		return this;
	}
	
	private Object get(String key) {
		if (holder.containsKey(key)) {
			return holder.get(key);
		}
				
		Object result = null;
		
		for (String item : key.split("\\.")) {
			if (result instanceof JsonObject)
				result = ((JsonObject) result).get(item);
			else if (result instanceof Map<?, ?>)
				result = JsonObject.make(result).get(item);
			else
				result = holder.get(item);
		}
		
		return result;
	}

	public String asString(String key) {
		return Objects.toString(get(key), "");
	}
	
	public boolean asBoolean(String key) {
		return (Boolean) get(key);
	}
	
	public Long asLong(String key) {
		Object value = get(key);
		return value == null ? null : Long.valueOf(value.toString());
	}
	
	public Double asDouble(String key) {
		return (Double) get(key);
	}
	
	public JsonArray asArray(String key) {
		return (JsonArray) get(key);
	}
	
	public JsonObject asObject(String key) {
		return make(holder.get(key));
	}

	public boolean isEmpty() {
		return holder.isEmpty();
	}
}
