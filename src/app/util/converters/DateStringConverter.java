package app.util.converters;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import javafx.util.StringConverter;

public final class DateStringConverter extends StringConverter<LocalDate> {
	private static final DateStringConverter INSTANCE;

    static {
    	INSTANCE = new DateStringConverter().withFormat("yyyy-MM-dd");
    }

	private DateTimeFormatter formatter;
    
    @Override 
    public String toString(LocalDate date) {
        if (date != null) {
            return formatter.format(date);
        } else {
            return "";
        }
    }
    
    @Override 
    public LocalDate fromString(String string) {
        if (string != null && !string.isEmpty()) {
            return LocalDate.parse(string, formatter);
        } else {
            return null;
        }
    }
    
    private DateStringConverter() {    	
    }
    
    public static DateStringConverter get(String format) {
    	return get().withFormat(format);
    }
    
    public static DateStringConverter get() {
    	return INSTANCE;
    }


	private DateStringConverter withFormat(String format) {
		this.formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
		return this;
	}
}