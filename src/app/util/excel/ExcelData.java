package app.util.excel;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Objects;
import java.util.Set;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Row.MissingCellPolicy;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ExcelData {
	public class ExcelRecord {
		private XSSFSheet sheet;

		private ExcelRecord(int sheetNumber) {		
			if (sheetNumber >= workbook.getNumberOfSheets()) {
				throw new IllegalArgumentException("Cannot access sheet. The index is out of range");
			}
			
			sheet = workbook.getSheetAt(sheetNumber);						
		}
		
		private ExcelRecord(String sheetName) {			
			sheet = workbook.getSheet(sheetName);
			
			if (sheet == null) {
				sheet = workbook.createSheet(sheetName);		
			}
		}

		public ExcelRecord(String sheetName, String... headers) {
			this(sheetName);
			generateHeader(headers);
		}
		
		public ExcelRecord(int sheetNumber, String... headers) {
			this(sheetNumber);
			generateHeader(headers);
		}		

		public ExcelRecord withValue(int row, int column, String value) {
			XSSFRow excelRow = sheet.getRow(row);

			if (excelRow == null) {
				excelRow = sheet.createRow(row);
			}

			Cell cell = excelRow.getCell(column, MissingCellPolicy.CREATE_NULL_AS_BLANK);

			cell.setCellType(CellType.STRING);
			cell.setCellValue(value);

			return this;
		}

		public void flush() {
			numberOfSheetsToProcess++;
		}		

		private void generateHeader(String... headers) {
			for (int i = 0; i < headers.length; i++) {
				withValue(0, i, headers[i]);
			}
		}
	}

	private XSSFWorkbook workbook;
	private File excelFile;

	private int numberOfRowsToSkip;
	private int numberOfSheetsToProcess;

	public ExcelData(File excelFile, int numberOfRowsToSkip, int numberOfSheetsToProcess) throws IOException {
		this.excelFile = excelFile;
		this.numberOfRowsToSkip = numberOfRowsToSkip;
		this.numberOfSheetsToProcess = numberOfSheetsToProcess;

		init();
	}

	public ExcelData(File excelFile) throws IOException {
		this(excelFile, 1, 1);
	}

	public ExcelData(File excelFile, int numberOfSheetsToProcess) throws IOException {
		this(excelFile, 1, numberOfSheetsToProcess);
	}

	private void init() throws IOException {
		if (excelFile == null)
			throw new IllegalArgumentException("Please select excel file first");

		FileInputStream inputStream = new FileInputStream(excelFile);
		this.workbook = new XSSFWorkbook(inputStream);
		inputStream.close();

		if (numberOfSheetsToProcess > workbook.getNumberOfSheets())
			throw new IllegalArgumentException("Excel file has less than [" + numberOfSheetsToProcess + "] sheets");

		for (int sheetNumber = 0; sheetNumber < numberOfSheetsToProcess; sheetNumber++) {
 			removeDublicates(workbook.getSheetAt(sheetNumber));
		}
	}

	private void skipTopRows(Iterator<Row> excelData) {
		for (int i = 0; i < numberOfRowsToSkip; i++) {
			if (excelData.hasNext())
				excelData.next();
		}
	}

	private void removeDublicates(Sheet sheet) {
		Set<String> uniqueRows = new HashSet<>();
		Iterator<Row> rows = sheet.iterator();

		while (rows.hasNext()) {
			Row currentRow = rows.next();

			StringBuilder celldata = new StringBuilder("#");

			for (int i = 0; i <= currentRow.getLastCellNum(); i++) {
				Cell cell = currentRow.getCell(i);
				celldata.append(cell == null ? "#" : cell.toString());
			}

			// Remove row if its already present or empty
			if (!uniqueRows.add(celldata.toString()) || celldata.toString().matches("#+")) {
				rows.remove();
			}
		}
	}

	public int rowsCount() {
		return workbook.getSheetAt(0).getPhysicalNumberOfRows();
	}

	public int getLastRowx() {
		throw new RuntimeException("Patch required. Please contact developer");
	}

	public int getLastRow(Sheet sheet) {
		int currentRowNumber = 0;
		int wrongRows = 0;

		Iterator<Row> rows = sheet.iterator();

		while (rows.hasNext()) {
			Row currentRow = rows.next();
			currentRowNumber = currentRow.getRowNum();

			boolean allNulls = true;

			for (int i = 0; i <= currentRow.getLastCellNum(); i++) {
				Cell cell = currentRow.getCell(i);

				if (cell != null && !Objects.toString(cell, "").isEmpty()) {
					allNulls = false;
					break;
				}
			}

			if (!allNulls)
				continue;

			if (++wrongRows > 1)
				return currentRowNumber - 2;
		}

		return currentRowNumber - wrongRows - 1;
	}

	public void setString(Row row, int column, String value) {
		if (row == null) {
			System.out.println("row is null");
			return;
		}

		Cell cell = row.getCell(column);

		if (cell == null) {
			cell = row.createCell(column);
		}

		cell.setCellType(CellType.STRING);
		cell.setCellValue(value);
	}

	public ExcelRecord createRecord(int sheetNumber) {
		return new ExcelRecord(sheetNumber - 1);
	}
	
	public ExcelRecord createRecord(int sheetNumber, String ... headers) {
		return new ExcelRecord(sheetNumber - 1, headers);
	}
	
	public ExcelRecord createRecord(String sheetName) {
		return new ExcelRecord(sheetName);
	}
	
	public ExcelRecord createRecord(String sheetName, String ... headers) {
		return new ExcelRecord(sheetName, headers);
	}

	public Integer checkAndGetIntCell(Row row, int cell, Integer defaultValue) {
		String value = checkAndGetCell(row, cell, null, true);
		return value == null ? defaultValue : Integer.valueOf(value.replaceAll("[., ].*", ""));
	}

	public Integer checkAndGetIntCell(Row row, int cell, String columnName) {
		return Integer.valueOf(checkAndGetCell(row, cell, columnName, false).replaceAll("[., ].*", ""));
	}

	public Double checkAndGetDoubleCell(Row row, int cell, String columnName) {
		return Double.valueOf(checkAndGetCell(row, cell, columnName, false).replace(",", "."));
	}

	public String checkAndGetCell(Row row, int cell) {
		return checkAndGetCell(row, cell, null, true);
	}

	public String checkAndGetCell(Row row, int cell, String columnName) {
		return checkAndGetCell(row, cell, columnName, false);
	}

	public String checkAndGetCell(Row row, int cell, String columnName, boolean canBeNull) {
		if (row.getCell(cell) == null) {
			if (canBeNull)
				return null;

			throw new IllegalArgumentException(
					"Unable to find data in row: " + (row.getRowNum() + 1) + " [" + columnName + "]");
		}

		Cell excelCell = row.getCell(cell);

		excelCell.setCellType(CellType.STRING);
		String cellValue = excelCell.toString().trim();

		if (cellValue.isEmpty()) {
			if (canBeNull)
				return null;

			throw new IllegalArgumentException(
					"Unable to find data in row: " + (row.getRowNum() + 1) + " [" + columnName + "]");
		}

		return cellValue;
	}

	public Iterator<Row> getData() {
		return getData(1);
	}

	public Iterator<Row> getData(int sheetNumber) {
		Iterator<Row> rows = workbook.getSheetAt(sheetNumber - 1).rowIterator();
		skipTopRows(rows);
		
		return rows;
	}

	public void writeChanges() throws IOException {
		try (FileOutputStream outFile = new FileOutputStream(excelFile.getAbsolutePath())) {
			workbook.write(outFile);
			init();
		}
	}

	public String getName() {
		return excelFile.getName();
	}

	public int getNumberOfSheets() {
		return workbook.getNumberOfSheets();
	}

	public void addSheet(String sheetName) {
		workbook.createSheet(sheetName);
	}
	
	public void dropSheet(int index) {
		workbook.removeSheetAt(index - 1);
	}
		
	public FileCopyLock createFileLock() throws IOException {
		return new FileCopyLock(excelFile, F -> excelFile = F);
	}

	public void reload() throws IOException {
		init();
	} 
}
