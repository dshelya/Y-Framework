package app.util.excel;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.util.function.Consumer;

import org.eclipse.jetty.io.RuntimeIOException;

public class FileCopyLock implements AutoCloseable {
	private final Consumer<File> onLockHandler;

	private RandomAccessFile fileAccessor;  
	private File sourceFile;
	private File tempFile;

	public FileCopyLock(File sourceFile, Consumer<File> onLockHandler) throws IOException {
		this.sourceFile = sourceFile;
		this.onLockHandler = onLockHandler;
	}

	@Override
	public void close() {
		try {
			releaseLock();
		} catch (IOException e) {
			throw new RuntimeIOException(e);
		}
	}

	public boolean release() {
		try {
			releaseLock();
			
			File initialFile = new File(tempFile.getAbsolutePath());			

			if (tempFile.delete() && sourceFile.renameTo(initialFile)) {	 			
				onLockHandler.accept(tempFile);
				return true;
			}
		} catch (IOException e) {
			return false;
		}
		
		return false;
	}

	public File aquire() {
		tempFile = new File(sourceFile.getAbsolutePath());

		try {
			// rename initial file
 			sourceFile = Files.move(sourceFile.toPath(), File.createTempFile(".excel","tmp").toPath(), StandardCopyOption.REPLACE_EXISTING).toFile();
 			
			// Copy source file content to TMP-file
			Files.copy(sourceFile.toPath(), tempFile.toPath(), StandardCopyOption.REPLACE_EXISTING);		

 			fileAccessor = new RandomAccessFile(tempFile, "rwd");
			fileAccessor.getChannel().lock();
			
			onLockHandler.accept(sourceFile);
 		} catch (IOException e) {
			try {
 				if (fileAccessor != null) {
 					fileAccessor.close();
 				}
			} catch (IOException e1) {
				return sourceFile;
			}
		} 
		
		return sourceFile;
	}

	private void releaseLock() throws IOException {
		fileAccessor.close();
	}
}