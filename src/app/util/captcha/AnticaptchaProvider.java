package app.util.captcha;

import java.io.IOException;
import java.io.InputStream;

public interface AnticaptchaProvider {
	String getBalance() throws IOException;
	String recognize(InputStream imageStream) throws Exception;
	AnticaptchaProvider setApiKey(String apiKey);
	String getName();
	void abortRecognition();
	boolean isAborted();
}	