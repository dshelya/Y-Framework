package app.util.captcha;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

public final class AnticaptchaService {
	private static final AnticaptchaService INSTANCE = new AnticaptchaService();
	private static final Map<String, AnticaptchaProvider> PROVIDERS = new HashMap<>();

	public static AnticaptchaService getInStance() {
		return INSTANCE;
	}

	public Optional<AnticaptchaProvider> loadProvider(String provider, String apiKey) {
		try {
			Class.forName("app.util.captcha." +  Character.toUpperCase(provider.charAt(0)) + provider.substring(1));
			return Optional.of(PROVIDERS.get(provider).setApiKey(apiKey));
		} catch (ClassNotFoundException e) {}

		return Optional.empty();
	}

	public void register(String name, AnticaptchaProvider instance) {
		PROVIDERS.put(name, instance);
	}
}