package app.util.captcha;

import java.io.IOException;
import java.io.InputStream;

import j.antigate.CapchaBypass;
import j.antigate.CapchaUtils;

abstract class Antigate implements AnticaptchaProvider {
	static {
		AnticaptchaService.getInStance().register("antigate", new Antigate() {
			private String apiKey;
			private boolean aborted;

			public String getBalance() throws IOException {
				return CapchaUtils.getBalance(apiKey);
			}

			public String recognize(InputStream imageStream) throws Exception {
				this.aborted = false;
				
				if (apiKey == null || apiKey.isEmpty())
					throw new IllegalArgumentException("Cannot recognize captcha. API key is not set");
				
				if (imageStream == null)
					throw new IllegalArgumentException("Cannot recognize empty captcha");
				
				String decoded = CapchaBypass.CapchaAnswer(imageStream, apiKey, "NO", "NO", "NO");
				
				if ("ERROR_KEY_DOES_NOT_EXIST".equals(decoded))
					throw new RuntimeException("Invalid anticaptcha API-KEY");
				
				if ("ERROR_NO_SLOT_AVAILABLE".equals(decoded))
					throw new RuntimeException("Anticaptcha service is too busy. Waiting 1 minute");				
				
				return decoded;
			}

			public Antigate setApiKey(String apiKey) {
				this.apiKey = apiKey;
				return this;
			}
			
			public String getName() {
				return "antigate";
			}

			@Override
			public void abortRecognition() {
				this.aborted = true;
				System.out.println("Aborting recognition..");
			}

			@Override
			public boolean isAborted() {
				return aborted;
			}
		});
	}
}