package app.util;

import java.util.ArrayDeque;

public class RingBuffer<E> {
	private ArrayDeque<Node<E>> buffer = new ArrayDeque<>();
	private Node<E> lastRequested;
	
	private class Node<I> {
		I item;
		Node<I> next;

		Node(I item) {
			this.item = item;
			this.next = this;
		}

		Node<I> bindNext(I item) {
			return next = new Node<>(item);
		}
	}

	public RingBuffer() {
		reset();
	}

	public void add(E item) {
		// bind current last item to the new item
		Node<E> last = buffer.peekLast();
		
		if (last.item == null) 
			last.item = item;
		else {
			buffer.offer(last.bindNext(item));
		}
		// Set next reference of the last as the first node
		buffer.peekLast().next = buffer.peekFirst();

	}

	public E next() {
		E item = lastRequested.item;
		lastRequested = lastRequested.next;
		
		return item;
	}

	public void reset() {
		buffer.clear();
		
		lastRequested = new Node<>(null);
		buffer.offer(lastRequested);
	}
}
