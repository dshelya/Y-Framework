package app.connectors.ebay;

import java.sql.SQLException;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import app.context.services.impl.LocalContext;
import app.context.services.interfaces.Context;


@RunWith(MockitoJUnitRunner.class)
public class EbayConnectorTest {
	@Spy
	static EbayConnector subject;
	
	@BeforeClass
	public static void setup() throws SQLException {
		Context context = LocalContext.getInstance("YFrameworkTest", true);
		context.getDatabase().bindDBFileName("ebaytracker");
		context.getLogger().tag(EbayConnectorTest.class).setDebugMode(true);
		
		subject = new EbayConnector("https://api.ebay.com/wsapi", "AgAAAA**AQAAAA**aAAAAA**+y9RWQ**nY+sHZ2PrBmdj6wVnY+sEZ2PrA2dj6ACk4GjCJCCpwSdj6x9nY+seQ**fpgDAA**AAMAAA**kdRINosbQscqgTeEG5GLB7DhnBqB2OEdB01cuicedrxL6g9gdHB1gMl08GH61Xf08FeGOfNSSDkp9EZshEqt0QckqR79ku/3o3ji3mjcqAHKfO2PLAaVvrle+x6eJWgEnXXMPdk92bbF+6vZf0lzjmtca8E702ohqBpBAyKn9AxZ2XWcI/9pFNZmbwxW5g7qJ1BhFYfMc6aRRWgWcs5qqkYE4q4J+cfWWu9hUPP2hEFAyKy1o9XchKsi1nVqMid/ttCzbO0R5jxIEWIZKxgepHrjTbutt5Kw8YhpWF65qPp9FTiXFzfHU5q8eDUEGEYSqlGH0TZgmdZJceNyoH9YfGbe+0PIC+UDCKeHp8bvwg40z8CuX7JCUm4b89Pi8HXJYslpD96MxTXo5fFZhfhWxv7ZmRBb668RUfwcZD/ofhQCdaIKYNmnK5xBqGA/O7Dz0v1jmWRtAZMhSnxpXBomVXeEMJEEzxZ4w+zc3CCM5ORzgP402CQOKVy1sabuB3gEwZubAg+Fk9+3JbMIey2YSl525tav7n6E+T3ATvvK7AUkkWrAzQVX9hsTA5t1S0nvQny9phCCRVwYR7O4q/xx5cfJNEKwrJU3tsQH2v/Z8QI1COv6DYIvO3CdtN4zYQeTgVOY447hZAgP0kmn8zIgn71DcuTaWzZbYnaoP386l50pvgz2VaGHeG7puCis4urVtfDHBMq+rn+m0hvAJ0sk1nCMQjU99XlL6999aCzwpC/YC5ujYSZrsMLmLFFn8BfZ");
	}
	
	@Test
	public void getLowestOfferListingsByAsin() throws Exception {
		//subject.reviseItem("272736284149", null, 12.34, 0);
		subject.reviseItem("272731771732", "571867262196", 12.34, 0);

		System.out.println();
 	}
}
