package app.connectors.amazon;

import java.sql.SQLException;
import java.util.Arrays;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import app.context.services.impl.LocalContext;
import app.context.services.interfaces.Context;
import app.module.api.ExternalProperty;


@RunWith(MockitoJUnitRunner.class)
public class AmazonConnectorTest {
	@Spy
	static AmazonConnector subject;
	
	@BeforeClass
	public static void setup() throws SQLException {
		Context context = LocalContext.getInstance("YFrameworkTest", true);
		context.getDatabase().bindDBFileName("ebaytracker");
		context.getLogger().tag(AmazonConnectorTest.class).setDebugMode(true);
		
		subject = new AmazonConnector();
		
		subject.bindAccessKey(new ExternalProperty<>("AKIAJMRJF434R24FXHEQ"));
		subject.bindSecretKey(new ExternalProperty<>("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"));
		subject.bindAppName(new ExternalProperty<>("AmazonTracker"));
		subject.bindAppVersion(new ExternalProperty<>("1"));
		subject.bindSellerId(new ExternalProperty<>("A2SZVCEUZ1L9VS"));
		subject.bindAuthToken(new ExternalProperty<>(""));
		subject.bindMarketplaceId(new ExternalProperty<>("ATVPDKIKX0DER"));
		subject.bindServiceUrl(new ExternalProperty<>("https://mws.amazonservices.com")); 	
	}
	
	@Test
	public void getLowestOfferListingsByAsin() {
		subject.getLowestOfferListingsByAsin(Arrays.asList("B01IR0J3MK"))
			.stream()
			.map(O -> O.toJSON())
			.forEach(System.out::println);
 	}
}
