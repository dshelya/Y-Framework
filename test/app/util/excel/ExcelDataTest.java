package app.util.excel;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.IOException;
import java.util.Iterator;

import org.apache.poi.ss.usermodel.Row;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import app.util.excel.ExcelData.ExcelRecord;

@RunWith(MockitoJUnitRunner.class)
public class ExcelDataTest {

	private File excelFile;

	@Before
	public void setupExcel() { 
		excelFile = new File("resource/example.xlsx");
	}
	
	@Test
	public void dropSheetShouldRemoveSheetAtGivenIndex() throws IOException {
		ExcelData excel = new ExcelData(excelFile);				
		
		excel.addSheet("Brand new sheet");		
		assertThat(excel.getData(2)).isNotNull();	
		
		excel.dropSheet(2);
		assertThatThrownBy(() -> excel.getData(2)).isInstanceOf(IllegalArgumentException.class);
 	}

	@Test
	public void addSheetShouldCreateANewSheet() throws IOException {
		ExcelData excel = new ExcelData(excelFile);
				
		assertThatThrownBy(() -> excel.getData(2)).isInstanceOf(IllegalArgumentException.class);
		
		excel.addSheet("Brand new sheet");		
		assertThat(excel.getData(2)).isNotNull();		
 	}

	@Test
	public void createRecordOnNewSheetShouldCreateANewSheetWithValidValues() throws IOException {
		ExcelData excel = new ExcelData(excelFile);
		
		assertThatThrownBy(() -> excel.getData(2)).isInstanceOf(IllegalArgumentException.class);
		
		createSheet(excel, "New Sheet", "HEAD1", "HEAD2");
		fillSheetWithValues(excel, 2);
		
		Iterator<Row> data = excel.getData(2);

		assertThat(data).isNotNull();
		assertTrue(data.hasNext());

		Row row = excel.getData(2).next();
		
		assertThat(row).isNotNull();
		assertThat(row.getCell(2)).isNull();

		assertThat(excel.checkAndGetCell(row, 0)).isEqualTo("value1");
		assertThat(excel.checkAndGetCell(row, 1)).isEqualTo("value2");
	}	

	@Test
	public void createRecordForExistingSheetShouldReuseExistingSheetAndSetValidValues() throws IOException {
		ExcelData excel = new ExcelData(excelFile);
				
		fillSheetWithValues(excel, 1);
		
		Iterator<Row> data = excel.getData();
		
		assertThat(data).isNotNull();
		assertTrue(data.hasNext());

		Row row = excel.getData(1).next();
		
		assertThat(row).isNotNull();
		assertThat(excel.checkAndGetCell(row, 0)).isEqualTo("value1");
		assertThat(excel.checkAndGetCell(row, 1)).isEqualTo("value2");
	}
	
	@Test
	public void getDataShouldReturnFreshRowIteratorForEachCall() throws IOException {
		ExcelData excel = new ExcelData(excelFile);		 	 
		
		fillSheetWithValues(excel, 1);
		
		for (int i = 0; i<5; i++) {
			Iterator<Row> data = excel.getData();
			
			assertThat(data).isNotNull();
			assertTrue(data.hasNext());

			Row row = excel.getData(1).next();
			
			assertThat(row).isNotNull();
			assertThat(excel.checkAndGetCell(row, 0)).isEqualTo("value1");
			assertThat(excel.checkAndGetCell(row, 1)).isEqualTo("value2");
		}
	}
		
	private void createSheet(ExcelData excel, String sheetName, String ... headers) {
		excel.createRecord(sheetName, headers).flush();
	}
	
	private void fillSheetWithValues(ExcelData excel, int sheetNumber) {
		ExcelRecord record = excel.createRecord(sheetNumber);			
		record.withValue(1, 0, "value1").withValue(1, 1, "value2").flush();
	}	
}
